

#include "testmatrix.cuh"

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
using namespace CudaFunctions;




cudaEvent_t start_a, stop_a;
float gpuTime_a;
void initCudaCalcTime()
{
	HANDLE_ERROR(cudaEventCreate(&start_a));
	HANDLE_ERROR(cudaEventCreate(&stop_a));
	HANDLE_ERROR(cudaEventRecord(start_a, 0));
}
void startCudaCalcTime()
{
	HANDLE_ERROR(cudaEventRecord(start_a, 0));
}
float elapsedCudaCalcTime()
{
	HANDLE_ERROR(cudaEventRecord(stop_a, 0));
	HANDLE_ERROR(cudaEventSynchronize(stop_a));
	HANDLE_ERROR(cudaEventElapsedTime(&gpuTime_a, start_a, stop_a));
	return gpuTime_a;
}

__global__ void mulMatrix(DeviceMatrix<int> a, DeviceMatrix<int> b, DeviceMatrix<int> c)
{
	auto i = blockIdx.x;
	auto j = blockIdx.y;
	auto k = threadIdx.x;

	if (i >= c.n || j >= c.m || k >= a.m)
		return;
	atomicAdd(&c.getFromGpu(i, j), a.getFromGpu(i, k)*b.getFromGpu(k, j));
}


namespace CudaFunctions
{
	Matrix<int> gpuMatrixMul(Matrix<int> &a, Matrix<int> &b)
	{


		Matrix<int> c(a.n, b.m);

		if (a.m != b.n || c.n > 1024)
			return c;

		a.copyToDevice();
		b.copyToDevice();
		c.copyToDevice();

		mulMatrix <<< dim3(c.m, c.n), c.n>>> (a.toDeviceMatrix(), b.toDeviceMatrix(), c.toDeviceMatrix());

		c.copyToHost();

		a.deleteDeviceData();
		b.deleteDeviceData();
		c.deleteDeviceData();
		return c;
	}
	Matrix<int> cpuMatrixMul(Matrix<int> &a, Matrix<int> &b)
	{
		Matrix<int> c(a.n,b.m);

		if (a.m != b.n)
			return c;

		for (int i = 0; i < c.n; i++)
		{
			for (int j = 0; j < c.m; j++)
			{
				for (int k = 0; k < a.m; k++)
				{
					c.getFromCpu(i, j) += a.getFromCpu(i, k)*b.getFromCpu(k, j);
				}
			}
		}

		return c;
	}
}