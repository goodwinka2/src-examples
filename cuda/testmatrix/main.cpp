﻿#include <iostream>

#include "testmatrix.cuh"
#include <ctime>



int main()
{
    std::cout << "Test!\n";

	auto size = 1000;
	CudaFunctions::Matrix<int> a(size, size+1);
	CudaFunctions::Matrix<int> b(size+1, size);

	a.toEye(2);
	b.toEye(3);
	auto start = std::clock();
	auto c_cpu = CudaFunctions::cpuMatrixMul(a, b);
	std::cout << "cpu\t" << double(clock()-start)/CLOCKS_PER_SEC << std::endl;

	start = std::clock();
	auto c_gpu = CudaFunctions::gpuMatrixMul(a, b);
	std::cout << "gpu\t" << double(clock() - start) / CLOCKS_PER_SEC << std::endl;
	//return 0;
}


