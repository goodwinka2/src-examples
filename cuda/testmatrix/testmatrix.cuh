#pragma once

#pragma comment(lib, "cudart") 
#include <vector>
#include <string>
#include "cuda_runtime.h"
#include <algorithm>

static bool HandleError(cudaError_t err, const char *file, int line)
{

	if (err != cudaSuccess && err != cudaErrorCudartUnloading)
	{
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
		
		exit(err);
		return false;
	}
	return true;
}
#define HANDLE_ERROR(err) (HandleError(err,__FILE__,__LINE__))

namespace CudaFunctions
{
	template<class T>
	struct Matrix;
	template<class T>
	struct DeviceMatrix;

	Matrix<int> gpuMatrixMul(Matrix<int>&, Matrix<int>&);
	Matrix<int> cpuMatrixMul(Matrix<int>&, Matrix<int>&);

	template<class T>
	struct DeviceMatrix
	{
		int n;
		int m;
		T* deviceData;

		__device__ inline T& getFromGpu(int _n, int _m)
		{
			return deviceData[_n*m + _m];
		}
	};

	template<class T>
	struct Matrix: public DeviceMatrix<T>
	{
		//int n;
		//int m;
		std::vector<T> hostData;
		//T* deviceData;
		__host__ Matrix(int _n, int _m)
		{			
			
			this->n = _n;
			this->m = _m;
			hostData.resize(_n*_m);
			for (auto &var : hostData)
				var = 0;
			this->deviceData = nullptr;
		}
		__host__ __device__ Matrix()
		{
			this->n = 0;
			this->m = 0;
			this->deviceData = nullptr;
		}

		DeviceMatrix<T> toDeviceMatrix()
		{
			DeviceMatrix<T> a;
			a.n = this->n;
			a.m = this->m;
			a.deviceData = this->deviceData;
			return a;
		}


		__host__ void toEye(T k)
		{
			for (int i = 0; i <std::min(this->n, this->m); i++)
			{
				
				getFromCpu(i, i) = k*(i+1);
				
			}

		}

		__host__ inline T& getFromCpu(int _n, int _m)
		{
			return hostData[_n*this->m + _m];
		}


		__host__ void deleteDeviceData()
		{
			if (this->deviceData != nullptr)
			{
				HANDLE_ERROR(cudaFree(this->deviceData));
				this->deviceData = nullptr;
			}

		}

		__host__ void createDeviceData()
		{
			deleteDeviceData();
			HANDLE_ERROR(cudaMalloc((void**)&this->deviceData, sizeof(T)* (hostData.size())));
			HANDLE_ERROR(cudaMemset(this->deviceData, 0, sizeof(T)* (hostData.size())));
		}

		void copyToHost()
		{
			if (this->deviceData != nullptr && hostData.size() > 0)
				HANDLE_ERROR(cudaMemcpy(hostData.data(), this->deviceData, hostData.size() * sizeof(T), cudaMemcpyDeviceToHost));
		}

		void copyToDevice()
		{
			if (this->deviceData == nullptr && hostData.size() > 0)
				createDeviceData();
			if (this->deviceData != nullptr && hostData.size() > 0)
				HANDLE_ERROR(cudaMemcpy(this->deviceData, hostData.data(), hostData.size() * sizeof(T), cudaMemcpyHostToDevice));
		}
	};
}