#pragma once

#pragma comment(lib, "cudart") 

#include <QString>
#include <QVector>
#include "pesokresultstructs.h"

#include <QtDataVisualization>

#include <QPair>

#include "cuda_runtime.h"

#include "cudasurfwidget.h"

namespace CudaFunctions
{
	struct CudaConfig
	{
		int cubeMaxSize_X = (32);
		int cubeMaxSize_Y = (32);
		int cubeMaxSize_Z = (32);
		float discretization_X = 1.5;
		float discretization_Y = 2.0;
		float discretization_Z = 1.5;
		float dnaSize = (4.0);
		float noizePower = 1.5;
		int firstDiscrete = 20;
		int useDnaKoeff = 0;
		int lastDiscrete = 140;// STROB_DATA_SIZE - 1;
		CudaSurfWidget *cudaSurfWidget;
	};

	struct SceneDataInfo
	{
		int4 minCoord;
		int4 maxCoord;
		int4 cubesCount;
		unsigned max;
		unsigned min;
	};

	bool initialize();
	int getDeviceCount();
	QString loadData(QList <CollectedSourceData> &collectedSourceData);
	void preLoadData(QList <CollectedSourceData> &collectedSourceData);

	QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>> getSurf(float addMin = 0);
	QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>> getImageXZ(float level = 0);
	QVector3D getCubesCount();
	QString testCuda();
	void setConfig(CudaConfig);
	CudaConfig getConfig();

	SceneDataInfo getSceneDataInfo();
}