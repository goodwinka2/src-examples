
#include "cudasurf.cuh"


#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#include <qmath.h>

#include <QString>
#include <QMessageBox>
#include <QApplication>
#include <QThread>

using namespace CudaFunctions;

#define THREAD_SIZE_calculateSourceDataInfo (512)

#define normKoeff 1000000

#if 0
#define cubeMaxSize_X (32)
#define cubeMaxSize_Y (32)
#define cubeMaxSize_Z (32)
#define discretization_X 0.5
#define discretization_Y 4.0
#define discretization_Z 0.5
#define dnaSize (4.0)
#else


#endif

#define cu_deg2rad(angle) (angle * M_PI / 180.0)
#define cu_rad2deg(angle) (angle * 180.0  / M_PI)

struct CubeData;
class FullCubeData;
//struct SceneDataInfo;
struct SourceDataInfo;
struct MultiVideocardPointers;
__global__ void calculateSourceDataInfo(CollectedSourceData* data, SourceDataInfo* sourceDataInfo_device, SceneDataInfo* info, int size, float dnaHalfSize, int lastDiscrete);
//__global__ void calculateSceneDataInfo(CollectedSourceData* data, SceneDataInfo* info, int size);
__global__ void fillCubeDataFromSource(FullCubeData *cube, int cubesCount, CollectedSourceData* data, SourceDataInfo* sourceDataInfo_device, int size, int firstDiscrete, int lastDiscrete, float dna, int useDnaKoeff);
__global__ void fillSumImage(FullCubeData *cube, int cubesCount, SceneDataInfo* info, float noizePower);
__global__ void fillSumImageXZ(FullCubeData *cube, int cubesCount, SceneDataInfo* info, float level);


static bool HandleError(cudaError_t err, const char *file, int line)
{
	
	if (err != cudaSuccess && err != cudaErrorCudartUnloading)
	{
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);

		auto error = QString("\nerror \n\"%1\" \nin %2 at line %3").arg(cudaGetErrorString(err)).arg(file).arg(line);
		//QThread::currentThread()->moveToThread(qApp->thread());
		int ret = QMessageBox::warning(0, QString("CUDA ERROR %1").arg(err), error);
		//exit(EXIT_FAILURE);
		//QThread::currentThread()->quit();
		return false;
	}
	return true;
}
#define HANDLE_ERROR(err) (HandleError(err,__FILE__,__LINE__))


typedef unsigned int elem_type;
__device__ elem_type torben(elem_type m[], int n)
{
	register int         i, less, greater, equal;
	elem_type  min, max, guess, maxltguess, mingtguess;

	min = max = m[0];
	for (i = 1; i < n; i++) {
		if (m[i] < min) min = m[i];
		if (m[i] > max) max = m[i];
	}

	while (1) {
		guess = (min + max) / 2;
		less = 0; greater = 0; equal = 0;
		maxltguess = min;
		mingtguess = max;
		for (i = 0; i < n; i++) {
			if (m[i] < guess) {
				less++;
				if (m[i] > maxltguess) maxltguess = m[i];
			}
			else if (m[i] > guess) {
				greater++;
				if (m[i] < mingtguess) mingtguess = m[i];
			}
			else equal++;
		}
		if (less <= (n + 1) / 2 && greater <= (n + 1) / 2) break;
		else if (less > greater) max = maxltguess;
		else min = mingtguess;
	}
	if (less >= (n + 1) / 2) return maxltguess;
	else if (less + equal >= (n + 1) / 2) return guess;
	else return mingtguess;
}



struct CubeData
{
	float4 coord;
	int4 size;
	float4 metrsPerIndex;
	unsigned int *data_device;
	unsigned int *dataWeight_device;
	int elementsCount;
	

	__host__ __device__ CubeData()
	{
		coord = float4({ 0, 0, 0, 0 });
		metrsPerIndex = float4({ 0, 0, 0, 0 });
		size = int4({ 0, 0, 0, 0 });
		data_device	= nullptr;
		dataWeight_device = nullptr;
		elementsCount = 0;
	};

	__host__ void createCubeData(float4 incoord, int4 insize, float4 inmetrsPerIndex)
	{
		deleteCubeData();
		coord = incoord;
		size = insize;
		metrsPerIndex = inmetrsPerIndex;

		size.w = size.x*size.z;
		elementsCount = size.x*size.y*size.z;
		HANDLE_ERROR(cudaMalloc((void**)&data_device, sizeof(unsigned int)* (elementsCount)));
		HANDLE_ERROR(cudaMemset(data_device, 0, sizeof(unsigned int)* (elementsCount)));

		HANDLE_ERROR(cudaMalloc((void**)&dataWeight_device, sizeof(unsigned int)* (elementsCount)));
		HANDLE_ERROR(cudaMemset(dataWeight_device, 0, sizeof(unsigned int)* (elementsCount)));
	}
	__host__ void deleteCubeData()
	{
		if (data_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(data_device));
			data_device = nullptr;
		}
		if (dataWeight_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(dataWeight_device));
			dataWeight_device = nullptr;
		}

	}
	__host__ __device__ inline int getCubeDataIndex(int x, int y, int z)
	{
		return (x + z * size.x + y * size.w);
	}
	__host__ __device__ inline int getCubeDataIndex(uint3 c)
	{
		return (c.x + c.z * size.x + c.y * size.w);
	}
	__host__ __device__ inline uint3 getCubeDataIndexes(int index)
	{
		uint3 c;
		c.y = index / size.w;
		c.x = index - c.y * size.w;
		c.z = (c.x) / size.x;
		c.x -=   c.z * size.x ;
		return c;
	}

	__host__ __device__ inline float3 getCubeDataCoord(int index)
	{
		float3 c;
		uint3 i = getCubeDataIndexes(index);
		c.x = coord.x + metrsPerIndex.x * i.x;
		c.y = coord.y + metrsPerIndex.y * i.y;
		c.z = coord.z + metrsPerIndex.z * i.z;
		return c;
	}

	__device__ inline float3 __getCubeDataCoord(int index)
	{
		float3 c;
		uint3 i = getCubeDataIndexes(index);
		c.x = fmaf(metrsPerIndex.x, i.x, coord.x);
		c.y = fmaf(metrsPerIndex.y, i.y, coord.y);
		c.z = fmaf(metrsPerIndex.z, i.z, coord.z);
		return c;
	}

	__device__ inline unsigned int& getCubeData(uint3 c)
	{
		return data_device[getCubeDataIndex(c)];
	}
};

//(arr[x][y][z]) w = x*z
#define _getCubeDataIndex(cubedata, x, y, z)\
(x + z*cubedata.size.x + y*cubedata.size.w)

#define _getCubeDataIndexes(cubedata, x, y, z, index)\
{\
y = index/cubedata.size.w;\
z = (index - y*cubedata.size.w)/cubedata.size.x; \
x = index - z*cubedata.size.x - y*cubedata.size.w;\
}

#define _getCubeData(cubedata,x,y,z)\
(cubedata.data_device[ getCubeDataIndex(cubedata, x, y, z)])



cudaEvent_t start_a, stop_a;
float gpuTime_a;
void initCudaCalcTime()
{
	HANDLE_ERROR(cudaEventCreate(&start_a));
	HANDLE_ERROR(cudaEventCreate(&stop_a));
	HANDLE_ERROR(cudaEventRecord(start_a, 0));
}
void startCudaCalcTime()
{
	HANDLE_ERROR(cudaEventRecord(start_a, 0));
}
float elapsedCudaCalcTime()
{
	HANDLE_ERROR(cudaEventRecord(stop_a, 0));
	HANDLE_ERROR(cudaEventSynchronize(stop_a));
	HANDLE_ERROR(cudaEventElapsedTime(&gpuTime_a, start_a, stop_a));
	return gpuTime_a;
}

template<class T>
struct ImagePointers
{
	T* image_device;
	T* image;

	int elementsCount;
	ImagePointers()
	{
		image_device = nullptr;
		image = nullptr;
		elementsCount = 0;
	}

	void create(int count)
	{
		if (count < 0)
			return;
		deleteData();

		elementsCount = count;		
		HANDLE_ERROR(cudaMalloc((void**)&image_device, sizeof(T)* (elementsCount)));
		image = new T[elementsCount];
		clear();
	}

	void deleteData()
	{
		if (image_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(image_device));
			image_device = nullptr;
		}
		if (image != nullptr)
		{
			delete image;
			image = nullptr;
		}
		elementsCount = 0;
	}

	void clear()
	{
		if (image_device != nullptr && image != nullptr) 
		{
			HANDLE_ERROR(cudaMemset(image_device, 0, sizeof(*image_device)* (elementsCount)));
			memset(image, 0, sizeof(*image)*elementsCount);
		}
	}

	void copyToHost()
	{
		if (image_device != nullptr && image != nullptr)
			HANDLE_ERROR(cudaMemcpy(image, image_device, elementsCount * sizeof(*image), cudaMemcpyDeviceToHost));
	}

	void copyToDevice()
	{
		if (image_device != nullptr && image != nullptr)
			HANDLE_ERROR(cudaMemcpy( image_device, image, elementsCount * sizeof(*image), cudaMemcpyHostToDevice));
	}
};


class FullCubeData
{
public:
	CubeData cube;

	ImagePointers<float> coordImage;
	ImagePointers<unsigned> summPowerImage;
	ImagePointers<unsigned> summPowerImageXZ;
	FullCubeData()
	{
	}

	void createFullCubeData(float4 incoord, int4 insize, float4 inmetrsPerIndex)
	{
		cube.createCubeData(incoord, insize, inmetrsPerIndex);
		createImageData();
	}

	void deleteFullCubeData()
	{
		cube.deleteCubeData();
		deleteImageData();
	}

	void createImageData()
	{
		deleteImageData();
		if (cube.data_device != nullptr)
		{
			int elementsCount = cube.size.x *cube.size.z;
			coordImage.create(elementsCount);
			summPowerImage.create(elementsCount);
			summPowerImageXZ.create(elementsCount);
			clearImages();
			clearDataImage();
		}

	}

	void clearImages()
	{
		if (cube.data_device != nullptr)
		{
			summPowerImage.clear();
			summPowerImageXZ.clear();
			
		}
	}
	void clearDataImage()
	{
		if (cube.data_device != nullptr)
		{
			coordImage.clear();
		}
	}

	void deleteImageData()
	{
		summPowerImage.deleteData();
		summPowerImageXZ.deleteData();
		coordImage.deleteData();
	}
	void copyImageData()
	{
		summPowerImage.copyToHost();
		summPowerImageXZ.copyToHost();
		coordImage.copyToHost();
	}


};

//struct SceneDataInfo
//{
//	int4 minCoord;
//	int4 maxCoord;
//	int4 cubesCount;
//	unsigned max;
//	unsigned min;
//};

struct SourceDataInfo
{
	float4 minCoord; 
	float4 maxCoord;	
	//unsigned max;
	//unsigned min;
	
	float4 vector;
	uint4 minMidMaxThreshold;


	__device__ void calcVector(float4 &angle, float &beta, float &eps)
	{
		beta = cu_deg2rad(beta);
		eps = cu_deg2rad(eps);
		angle.x = cu_deg2rad(angle.x);
		angle.y = cu_deg2rad(angle.y);
		angle.z = cu_deg2rad(angle.z);
#if 0
		float3 coord;

		float2 sc_beta;
		__sincosf(beta, &sc_beta.x, &sc_beta.y);

		float2 sc_eps;
		__sincosf(eps, &sc_eps.x, &sc_eps.y);

		float2 sc_psi;
		__sincosf(angle.x, &sc_psi.x, &sc_psi.y);

		float2 sc_theta;
		__sincosf(angle.y, &sc_theta.x, &sc_theta.y);

		float2 sc_gamma;
		__sincosf(angle.z, &sc_gamma.x, &sc_gamma.y);
#else
		float3 coord;

		float2 sc_beta;
		sincosf(beta, &sc_beta.x, &sc_beta.y);

		float2 sc_eps;
		sincosf(eps, &sc_eps.x, &sc_eps.y);

		float2 sc_psi;
		sincosf(angle.x, &sc_psi.x, &sc_psi.y);

		float2 sc_theta;
		sincosf(angle.y, &sc_theta.x, &sc_theta.y);

		float2 sc_gamma;
		sincosf(angle.z, &sc_gamma.x, &sc_gamma.y);
#endif

		coord.x =  sc_eps.x;
		coord.y = -sc_eps.y*sc_beta.y;
		coord.z = -sc_eps.y*sc_beta.x;

		vector.x = coord.x*sc_psi.y*sc_theta.y + coord.y*(sc_psi.x*sc_gamma.x - sc_psi.y*sc_theta.x*sc_gamma.y) + coord.z*(sc_psi.x*sc_gamma.y + sc_psi.y*sc_theta.x*sc_gamma.x);
		vector.y = coord.x*sc_theta.x + coord.y*sc_theta.y*sc_gamma.y - coord.z*sc_theta.y*sc_gamma.x;
		vector.z = -coord.x*sc_psi.x*sc_theta.y + coord.y*(sc_psi.y*sc_gamma.x + sc_psi.x*sc_theta.x*sc_gamma.y) + coord.z*(sc_psi.y*sc_gamma.y - sc_psi.x*sc_theta.x*sc_gamma.x);
	};
};

struct MultiVideocardPointers
{
	QVector<FullCubeData> cubes;
	FullCubeData* cubes_device;
	CollectedSourceData* sourceData_device;
	SourceDataInfo* sourceDataInfo_device;
	QVector<SourceDataInfo> sourceDataInfo;
	SceneDataInfo* info_device;
	SceneDataInfo info;
	CudaConfig config;
	int getFullCubeDataIndex(int3 i)
	{
		return i.y + info.cubesCount.y*i.z + info.cubesCount.y*info.cubesCount.z*i.x;
	}

	MultiVideocardPointers()
	{
		sourceData_device = nullptr;
		sourceDataInfo_device = nullptr;
		info_device = nullptr;
		cubes_device = nullptr;
		clear();
	}

	void clear()
	{
		for (auto&var : cubes)
		{
			var.deleteFullCubeData();
		}
		if (sourceData_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(sourceData_device));
			sourceData_device = nullptr;
		}
		if (sourceDataInfo_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(sourceDataInfo_device));
			sourceDataInfo_device = nullptr;
		}
		if (info_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(info_device));
			info_device = nullptr;
		}
		if (cubes_device != nullptr)
		{
			HANDLE_ERROR(cudaFree(cubes_device));
			cubes_device = nullptr;
		}
		sourceDataInfo.clear();
		cubes.clear();
	}

	void load(QList <CollectedSourceData> &collectedSourceData)
	{
		clear();
		
		HANDLE_ERROR(cudaMalloc((void**)&sourceData_device, sizeof(CollectedSourceData)* (collectedSourceData.size())));
		//HANDLE_ERROR(cudaMemset(sourceData_device, 0, sizeof(*sourceData_device)* (collectedSourceData.size())));

		auto src = QVector<CollectedSourceData>::fromList(collectedSourceData);
		HANDLE_ERROR(cudaMemcpy(sourceData_device, src.data(), sizeof(CollectedSourceData)* (collectedSourceData.size()), cudaMemcpyHostToDevice));

		HANDLE_ERROR(cudaMalloc((void**)&info_device, sizeof(*info_device)));

		int intInf = 0x7FFFFFFF;
		info.maxCoord = int4{ -intInf, -intInf, -intInf, -intInf };
		info.minCoord = int4{ intInf, intInf, intInf, intInf};
		HANDLE_ERROR(cudaMemcpy(info_device, &info, sizeof(*info_device), cudaMemcpyHostToDevice));

		HANDLE_ERROR(cudaMalloc((void**)&sourceDataInfo_device, sizeof(SourceDataInfo)* (collectedSourceData.size())));
		//cuda <<<>>>
		
		calculateSourceDataInfo << <collectedSourceData.size() / THREAD_SIZE_calculateSourceDataInfo + 1, THREAD_SIZE_calculateSourceDataInfo >> >
			(
				sourceData_device,
				sourceDataInfo_device,
				info_device, 
				collectedSourceData.size(),
				cu_deg2rad(config.dnaSize / 2.0),
				config.lastDiscrete
				);
		HANDLE_ERROR(cudaGetLastError());
		
		//HANDLE_ERROR(cudaThreadSynchronize());
		sourceDataInfo.resize(collectedSourceData.size());
		HANDLE_ERROR(cudaMemcpy(sourceDataInfo.data(), sourceDataInfo_device, sizeof(*sourceDataInfo_device)* (collectedSourceData.size()), cudaMemcpyDeviceToHost));
		HANDLE_ERROR(cudaMemcpy(&info, info_device, sizeof(*info_device), cudaMemcpyDeviceToHost));

		info.maxCoord.x /= 100.0;
		info.maxCoord.y /= 100.0;
		info.maxCoord.z /= 100.0;

		info.minCoord.x /= 100.0;
		info.minCoord.y /= 100.0;
		info.minCoord.z /= 100.0;

		int discretsCountX = (info.maxCoord.x - info.minCoord.x) / config.discretization_X;
		int discretsCountY = (info.maxCoord.y - info.minCoord.y) / config.discretization_Y;
		int discretsCountZ = (info.maxCoord.z - info.minCoord.z) / config.discretization_Z;

		int cubesCountX = discretsCountX / config.cubeMaxSize_X + 1;
		int cubesCountY = discretsCountY / config.cubeMaxSize_Y + 1;
		int cubesCountZ = discretsCountZ / config.cubeMaxSize_Z + 1;

		
		for (size_t x = 0; x < cubesCountX; x++)
		{
			for (size_t z = 0; z < cubesCountZ; z++)
			{
				for (size_t y = 0; y < cubesCountY; y++)
				{
					FullCubeData cubeData;

					float4 coord;
					int4 size;
					float4 metrPerIndex;

					coord.x = info.minCoord.x + x * config.discretization_X*config.cubeMaxSize_X;
					coord.y = info.minCoord.y + y * config.discretization_Y*config.cubeMaxSize_Y;
					coord.z = info.minCoord.z + z * config.discretization_Z*config.cubeMaxSize_Z;

					metrPerIndex.x = config.discretization_X;
					metrPerIndex.y = config.discretization_Y;
					metrPerIndex.z = config.discretization_Z;
					metrPerIndex.w = sqrt((metrPerIndex.x*metrPerIndex.x) + (metrPerIndex.y*metrPerIndex.y) + (metrPerIndex.z*metrPerIndex.z));
					size.x = config.cubeMaxSize_X;
					size.y = config.cubeMaxSize_Y;
					size.z = config.cubeMaxSize_Z;

					cubeData.createFullCubeData(coord,size,metrPerIndex);

					cubes << cubeData;
				}
			}
		}
		
		HANDLE_ERROR(cudaMalloc((void**)&cubes_device, sizeof(FullCubeData)* (cubes.size())));
		HANDLE_ERROR(cudaMemcpy(cubes_device, cubes.data(), sizeof(FullCubeData)* (cubes.size()), cudaMemcpyHostToDevice));

		info.cubesCount = int4{ cubesCountX , cubesCountY, cubesCountZ, 0};
		HANDLE_ERROR(cudaMemcpy(info_device, &info, sizeof(*info_device), cudaMemcpyHostToDevice));
	}

	int fillCubes()
	{
		int counter = 0;
		QVector<cudaStream_t> streams;

		int totalSize =  cubes.size() * (sourceDataInfo.size() / THREAD_SIZE_calculateSourceDataInfo + 1);
		streams.resize(totalSize);

		for (size_t i = 0; i < totalSize; i++)
		{
			cudaStreamCreate(&streams[i]);
		}

		emit config.cudaSurfWidget->readyPrecent(0);
		for (int i = 0; i < cubes.size(); i++)
		{
			
			for (int j = 0; j < sourceDataInfo.size() / THREAD_SIZE_calculateSourceDataInfo + 1; j++)
			{
				//auto &cube = cubes[i];
				
				int size;
				size = std::min(THREAD_SIZE_calculateSourceDataInfo, sourceDataInfo.size() - j * THREAD_SIZE_calculateSourceDataInfo);
				fillCubeDataFromSource<<<dim3(config.cubeMaxSize_X * config.cubeMaxSize_Y * config.cubeMaxSize_Z ), dim3(THREAD_SIZE_calculateSourceDataInfo) /*, 0, streams[counter]*/ >>>
					(
						&cubes_device[i],
						cubes.size(),
						&sourceData_device[j * THREAD_SIZE_calculateSourceDataInfo],
						&sourceDataInfo_device[j * THREAD_SIZE_calculateSourceDataInfo],
						size,
						config.firstDiscrete,
						config.lastDiscrete,
						cu_deg2rad(config.dnaSize),
						config.useDnaKoeff
					);
				
				
				counter++;
			}
		}

		for (size_t i = 0; i < totalSize; i++)
		{
			cudaStreamSynchronize(streams[i]);
			emit config.cudaSurfWidget->readyPrecent(i * 100 / totalSize);
		}
		HANDLE_ERROR(cudaGetLastError());
		HANDLE_ERROR(cudaThreadSynchronize());
		for (size_t i = 0; i < totalSize; i++)
		{
			cudaStreamDestroy(streams[i]);
		}
		emit config.cudaSurfWidget->readyPrecent(100);
		return counter;
	}

	~MultiVideocardPointers()
	{
		//clear();
	}

	
	void createSumImage()
	{
		//uint3 coord = { blockIdx.x, threadIdx.y ,blockIdx.z };
		//int cubeY = threadIdx.x;
		//__global__ void fillSumImage(FullCubeData *cube, int cubesCount, SceneDataInfo* info)
		for (auto&var : cubes)
		{
			var.clearImages();
			var.clearDataImage();
			//var.
		}
		for (size_t x = 0; x < info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < info.cubesCount.z; z++)
			{

				fillSumImage << <dim3(config.cubeMaxSize_X, config.cubeMaxSize_Z), dim3(info.cubesCount.y, config.cubeMaxSize_Y) >> >
					(
						&cubes_device[z*info.cubesCount.y + x * info.cubesCount.y* info.cubesCount.z],
						cubes.size(),
						info_device,
						config.noizePower);
				HANDLE_ERROR(cudaGetLastError());
				//HANDLE_ERROR(cudaThreadSynchronize());
			}
		}

		for (auto&var : cubes)
		{
			var.copyImageData();
		}
	}

	void createSumImageXZ(float level)
	{
		//uint3 coord = { blockIdx.x, threadIdx.y ,blockIdx.z };
		//int cubeY = threadIdx.x;
		//__global__ void fillSumImage(FullCubeData *cube, int cubesCount, SceneDataInfo* info)
		for (auto&var : cubes)
		{
			var.clearImages();
			//var.
		}
		for (size_t x = 0; x < info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < info.cubesCount.z; z++)
			{

				fillSumImageXZ << <dim3(config.cubeMaxSize_X, config.cubeMaxSize_Z), dim3(info.cubesCount.y, config.cubeMaxSize_Y) >> >
					(
						&cubes_device[z*info.cubesCount.y + x * info.cubesCount.y* info.cubesCount.z],
						cubes.size(),
						info_device,
						level);
				HANDLE_ERROR(cudaGetLastError());
				//HANDLE_ERROR(cudaThreadSynchronize());
			}
		}

		for (auto&var : cubes)
		{
			var.copyImageData();
		}
	}
};

QList<MultiVideocardPointers> multiVideocardPointers;

MultiVideocardPointers& getCurrentCubes() 
{
	int device;
	HANDLE_ERROR(cudaGetDevice(&device));
	return multiVideocardPointers[device];
}

__global__ void calculateTestData(CubeData cubeData, int *result)
{
	*result = cubeData.getCubeDataCoord(3).x;// cubeData.size.x;
	//CubeData cubeData2 = cubeData;
	
}

__global__ void calculateSourceDataInfo(CollectedSourceData* data , SourceDataInfo* sourceDataInfo_device, SceneDataInfo* info, int size, float dnaHalfSize, int lastDiscrete)
{

	int index = blockIdx.x*THREAD_SIZE_calculateSourceDataInfo + threadIdx.x;

	if (index >= size)
		return;

	//CollectedSourceData* data = &_data[index];
	//SourceDataInfo* sourceDataInfo_device = &_sourceDataInfo_device[index];

	data = &data[index];
	sourceDataInfo_device = &sourceDataInfo_device[index];
	 
	float4 coord;
	coord.x = data->projectionX;
	coord.y = data->projectionY;
	coord.z = data->projectionZ;

	
	float4 angle;
	angle.x = data->psi;// = cu_deg2rad(data->psi);
	angle.y = data->theta;// = cu_deg2rad(data->theta);
	angle.z = data->gamma;// = cu_deg2rad(data->gamma);
	float beta = data->beta;// = cu_deg2rad(data->beta);
	float eps = data->eps;// = cu_deg2rad(data->eps);

	sourceDataInfo_device->calcVector(angle, beta, eps);

	float4 min;
	float4 max;
	float4 vector = sourceDataInfo_device->vector;

	float d = lastDiscrete * data->discreteSize + data->distanceToFirstDiscrete;
	float r = d * __tanf(dnaHalfSize * 1);
	
#if 1
	min.x = fminf(coord.x, fmaf(vector.x, d, coord.x -r));
	min.y = fminf(coord.y, fmaf(vector.y, d, coord.y -r));
	min.z = fminf(coord.z, fmaf(vector.z, d, coord.z -r));

	max.x = fmaxf(coord.x, fmaf(vector.x, d, coord.x + r));
	max.y = fmaxf(coord.y, fmaf(vector.y, d, coord.y + r));
	max.z = fmaxf(coord.z, fmaf(vector.z, d, coord.z + r));
#else
	min.x = fminf(coord.x, (vector.x* d + coord.x ));
	min.y = fminf(coord.y, (vector.y* d + coord.y ));
	min.z = fminf(coord.z, (vector.z* d + coord.z ));

	max.x = fmaxf(coord.x, (vector.x* d + coord.x));
	max.y = fmaxf(coord.y, (vector.y* d + coord.y ));
	max.z = fmaxf(coord.z, (vector.z* d + coord.z ));
#endif


	sourceDataInfo_device->maxCoord = max;
	sourceDataInfo_device->minCoord = min;
	
	atomicMin(&info->minCoord.x, min.x * 100);
	atomicMin(&info->minCoord.y, min.y * 100);
	atomicMin(&info->minCoord.z, min.z * 100);

	atomicMax(&info->maxCoord.x, max.x * 100);
	atomicMax(&info->maxCoord.y, max.y * 100);
	atomicMax(&info->maxCoord.z, max.z * 100); 


	sourceDataInfo_device->minMidMaxThreshold.y = torben(&data->rlData[50], STROB_DATA_SIZE-50);
	if (sourceDataInfo_device->minMidMaxThreshold.y == 0)sourceDataInfo_device->minMidMaxThreshold.y = 1;
	 
	//min.w = coord.y;// rnorm3df(vector.x, vector.y, vector.z);
	//max.w = coord.y;// rnorm3df(vector.x, vector.y, vector.z);
	//atomicMin(&info->minCoord.w, min.w * 100);
	//atomicMax(&info->maxCoord.w, max.w * 100);
}

__global__ void fillCubeDataFromSource(FullCubeData *cube, int cubesCount, CollectedSourceData* data, SourceDataInfo* sourceDataInfo_device, int size, int firstDiscrete,int lastDiscrete,float dna, int useDnaKoeff)
{
	
	__shared__ int cellIndex;
	cellIndex = blockIdx.x;
	int sourceIndex = threadIdx.x;
	
	if (sourceIndex >= size)
		return;  


	__shared__ CubeData cubeData;
	cubeData = cube->cube;

	if (cellIndex >= cubeData.elementsCount)
		return;

	auto sourceDataInfo = sourceDataInfo_device[sourceIndex];
	data = &data[sourceIndex];
	

	__shared__ float3 cellCoord;
	cellCoord = cubeData.__getCubeDataCoord(cellIndex);



	if (
		cellCoord.x > sourceDataInfo.maxCoord.x 
		|| 
		cellCoord.x < sourceDataInfo.minCoord.x
		||
		cellCoord.y > sourceDataInfo.maxCoord.y 
		|| 
		cellCoord.y < sourceDataInfo.minCoord.y
		||
		cellCoord.z > sourceDataInfo.maxCoord.z 
		|| 
		cellCoord.z < sourceDataInfo.minCoord.z
		)
		return;
	
	float4 vector2;
	vector2.x = cellCoord.x - data->projectionX;
	vector2.y = cellCoord.y - data->projectionY;
	vector2.z = cellCoord.z - data->projectionZ;
	vector2.w = norm3df(vector2.x, vector2.y, vector2.z);


	float d = fmaf(lastDiscrete+1, data->discreteSize , data->distanceToFirstDiscrete);

	if (vector2.w > d || vector2.w < data->distanceToFirstDiscrete || vector2.w < 1)
		return;
	
	float value = 0;
	value = __fdividef(fmaf(sourceDataInfo.vector.x,vector2.x , fmaf( sourceDataInfo.vector.y,vector2.y , sourceDataInfo.vector.z*vector2.z)), vector2.w);
		

	value = fabsf( acosf(value));

	if ((value) > (dna))		
		return;

#if 0
	value =  fabsf(__expf(-M_PI * __powf(__fdividef(value, (dna)), 2)));
#else
	float2 angles;
	float r;
	//r = cubeData.metrsPerIndex.w;// norm3df(vector2.x, vector2.y, vector2.z);
	r = fabsf(atanf(__fdividef(cubeData.metrsPerIndex.w, vector2.w)));// *1.5;
	//r = 2 * M_PI*r*r;
	angles.x = value + r;
	angles.y = value - r;
	r = sqrtf(M_PI) / dna;
	value = fabsf( __fdividef(1, 2) * (erff(angles.x*r) - erff(angles.y*r)) );
	//value = 2 * M_PI* value *value;
#endif
	//value = 1/powf(value,2);

	value *= normKoeff;
	
	
	atomicMax(&cubeData.dataWeight_device[cellIndex], value);
	__syncthreads();
	if (cubeData.dataWeight_device[cellIndex] == (unsigned)value)
	{//if (value < 0)return;
	//atomicCAS(&cubeData.data_device[cellIndex], 0, value);
		sourceIndex = lastDiscrete - __fdividef((d - vector2.w), data->discreteSize);
		if (firstDiscrete > sourceIndex
			||
			sourceIndex < 1
			||
			sourceIndex > STROB_DATA_SIZE - 2
			)
			return;
		//value *= (data->rlData[sourceIndex]  + data->rlData[sourceIndex - 1] + data->rlData[sourceIndex +1])/3;
		//value = normKoeff / (value);
		//value *= normKoeff;
		if (useDnaKoeff != 0)
		{
			value = value*(data->rlData[sourceIndex]) / sourceDataInfo.minMidMaxThreshold.y;
		}
		else
		{
			value = normKoeff / sourceDataInfo.minMidMaxThreshold.y * (data->rlData[sourceIndex]);
		}
		//value *= normKoeff;
		//value /= sourceDataInfo.minMidMaxThreshold.y;
		cubeData.data_device[cellIndex] = value;
		//atomicMax(&cubeData.data_device[cellIndex], value);
	}
}

__global__ void fillSumImage(FullCubeData *cube, int cubesCount, SceneDataInfo* info,float noizePower)
{
	int index = 0;
	//cube = &cube[index];

	uint3 coord = { blockIdx.x, threadIdx.y ,blockIdx.y};
	int cubeY = threadIdx.x;
	if (cubeY >= info->cubesCount.y || coord.x >= cube->cube.size.x || coord.y >= cube->cube.size.y || coord.z >= cube->cube.size.z)
		return;

	auto summPowerImage_device = cube->summPowerImage.image_device;
	auto coordImage_device = cube->coordImage.image_device;
	
	int imageIndex = cube->cube.getCubeDataIndex(coord.x, 0, coord.z);
	
	//if (imageIndex >= cube->summPowerImage.elementsCount)
	//	return;

	//auto c = cube->cube.getCubeDataCoord(cube->cube.getCubeDataIndex(coord));
	//if (c.y > 10 || c.y < -10)
	//	return;
	auto res = cube[cubeY].cube.getCubeData(coord);
	if (res < normKoeff * noizePower)
		return;
	//res = log10f(res)*1000;
#if 0
	atomicCAS(&image_device[imageIndex], 0, res);
	atomicMin(&image_device[imageIndex], res);
#else
	atomicMax(&summPowerImage_device[imageIndex], res);
	__syncthreads();
	if (summPowerImage_device[imageIndex] == res)
		coordImage_device[imageIndex] = cube[cubeY].cube.__getCubeDataCoord(cube[cubeY].cube.getCubeDataIndex(coord)).y;

#endif
}

__global__ void fillSumImageXZ(FullCubeData *cube, int cubesCount, SceneDataInfo* info,float level)
{
	//cube = &cube[index];

	uint3 coord = { blockIdx.x, threadIdx.y ,blockIdx.y };
	int cubeY = threadIdx.x;
	if (cubeY >= info->cubesCount.y || coord.x >= cube->cube.size.x || coord.y >= cube->cube.size.y || coord.z >= cube->cube.size.z)
		return;

	auto cellCoord = cube[cubeY].cube.__getCubeDataCoord(cube[cubeY].cube.getCubeDataIndex(coord));

	if (cellCoord.y + cube[cubeY].cube.metrsPerIndex.y < level || cellCoord.y - cube[cubeY].cube.metrsPerIndex.y > level)
		return;

	auto summPowerImage_device = cube->summPowerImageXZ.image_device;
	//auto coordImage_device = cube->coordImage.image_device;

	int imageIndex = cube->cube.getCubeDataIndex(coord.x, 0, coord.z);

	//if (imageIndex >= cube->summPowerImage.elementsCount)
	//	return;

	//auto c = cube->cube.getCubeDataCoord(cube->cube.getCubeDataIndex(coord));
	//if (c.y > 10 || c.y < -10)
	//	return;
	auto res = cube[cubeY].cube.getCubeData(coord);
	if (res == 0)
		return;
#if 0
	atomicCAS(&image_device[imageIndex], 0, res);
	atomicMin(&image_device[imageIndex], res);
#else
	atomicMax(&summPowerImage_device[imageIndex], res);
	//__syncthreads();
	//if (summPowerImage_device[imageIndex] == res)
	//	coordImage_device[imageIndex] = cellCoord.y;

#endif
}
//bool isPointersInitialized = false;

namespace CudaFunctions
{
	bool initialize()
	{
		
		int deviceCount;
		if (!HANDLE_ERROR(cudaGetDeviceCount(&deviceCount)))
			return false;
		if (!multiVideocardPointers.isEmpty())
		{
			for (auto&card : multiVideocardPointers)
			{
				card.clear();
			}
			return true;
		}

		for (size_t i = 0; i < getDeviceCount(); i++)
		{
			multiVideocardPointers << MultiVideocardPointers();
		}
		return true;
	}
	QString testCuda()
	{
		initCudaCalcTime();

		QString str;
		int deviceCount;
		HANDLE_ERROR(cudaGetDeviceCount(&deviceCount));


		CubeData cubeData;
		startCudaCalcTime();
		cubeData.createCubeData(float4({ 0,0,0,0 }), int4({ 314572800,1,1,0 }), float4({ 0,0,0,0 }));
		elapsedCudaCalcTime();
		str.append(QString("\ntime: %1 ms").arg(gpuTime_a));

		for (size_t i = 0; i < cubeData.elementsCount && i < 50; i++)
		{
			auto coord = cubeData.getCubeDataCoord(i);
			str.append(QString("\n%4: x %1; y %2; z %3").arg(coord.x).arg(coord.y).arg(coord.z).arg(i));
		}
		int * res;
		HANDLE_ERROR(cudaMalloc((void**)&res, sizeof(*res)* (1)));
		HANDLE_ERROR(cudaMemset(res, 0, sizeof(*res)* (1)));

		startCudaCalcTime();
		calculateTestData << < dim3(128, 128, 128), dim3(512) >> > (cubeData, res);
		elapsedCudaCalcTime();
		str.append(QString("\ncalc time: %1 ms").arg(gpuTime_a));

		int outRes;
		HANDLE_ERROR(cudaMemcpy(&outRes, res, 1 * sizeof(int), cudaMemcpyDeviceToHost));
		str.append(QString("\nres: %1").arg(outRes));
		//str.append(QString("\ndevice res: %1").arg(*res));
		startCudaCalcTime();
		cubeData.deleteCubeData();
		elapsedCudaCalcTime();
		str.append(QString("\ntime: %1 ms").arg(gpuTime_a));
		return str;
	}

	int getDeviceCount()
	{
		int deviceCount;
		HANDLE_ERROR(cudaGetDeviceCount(&deviceCount));
		//int device;
		//for (device = 0; device < deviceCount; ++device) {
		//	cudaDeviceProp deviceProp;
		//	cudaGetDeviceProperties(&deviceProp, device);
		//}
		//int ***a;
		//a[1][2][3] = 0;

		//if (isPointersInitialized == false)
		//{
		//	//multiVideocardPointers.resize(deviceCount);
		//	isPointersInitialized = true;
		//}
		return deviceCount;

	}

	void preLoadData(QList <CollectedSourceData> &collectedSourceData)
	{
		
		initCudaCalcTime();
		startCudaCalcTime();
		if (collectedSourceData.isEmpty())
			return;
		auto &cubes = getCurrentCubes();
		cubes.clear();
		cubes.load(collectedSourceData);

	}
	   	  
	QString loadData(QList <CollectedSourceData> &collectedSourceData)
	{

		QString str; 
		initCudaCalcTime();
		startCudaCalcTime();
		if (collectedSourceData.isEmpty())
			return str;
		auto &cubes = getCurrentCubes();
		cubes.clear();
		cubes.load(collectedSourceData);
		elapsedCudaCalcTime();
		str.append(QString("\n loadData time: %1 ms; size %2").arg(gpuTime_a).arg(collectedSourceData.size()));
		str.append(QString("\n perstrob time: %1 ms").arg(gpuTime_a/collectedSourceData.size()));
		str.append(QString("\n x:\t%1\t%2").arg(cubes.info.minCoord.x).arg(cubes.info.maxCoord.x));
		str.append(QString("\n y:\t%1\t%2").arg(cubes.info.minCoord.y).arg(cubes.info.maxCoord.y));
		str.append(QString("\n z:\t%1\t%2").arg(cubes.info.minCoord.z).arg(cubes.info.maxCoord.z));
		str.append(QString("\n w:\t%1\t%2").arg(cubes.info.minCoord.w).arg(cubes.info.maxCoord.w));
		str.append(QString("\n cubes:\t%1; (%2 %3 %4); cells %5")
			.arg(cubes.cubes.size())
			.arg(cubes.info.cubesCount.x)
			.arg(cubes.info.cubesCount.y)
			.arg(cubes.info.cubesCount.z)
			.arg(cubes.cubes.size()*cubes.config.cubeMaxSize_X*cubes.config.cubeMaxSize_Y*cubes.config.cubeMaxSize_Z));

		startCudaCalcTime();
		auto fillCounts = cubes.fillCubes();
		elapsedCudaCalcTime();
		str.append(QString("\n fillCubes time: %1 ms; count %2; threads per count %3; threads %4")
			.arg(gpuTime_a)
			.arg(fillCounts)
			.arg((float)cubes.config.cubeMaxSize_X*cubes.config.cubeMaxSize_Y*cubes.config.cubeMaxSize_Z*THREAD_SIZE_calculateSourceDataInfo)
			.arg((float)fillCounts*cubes.config.cubeMaxSize_X*cubes.config.cubeMaxSize_Y*cubes.config.cubeMaxSize_Z*THREAD_SIZE_calculateSourceDataInfo));

		startCudaCalcTime();
		cubes.createSumImage();
		elapsedCudaCalcTime();
		str.append(QString("\n createSumImage time: %1 ms").arg(gpuTime_a));

		
		unsigned max = 0;// cubes.cubes[0].summPowerImage.image[0];
		auto min = 0xFFFFFFFF;


		int index = 0;
		for (size_t x = 0; x < cubes.info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < cubes.info.cubesCount.z; z++)
			{
				size_t y = 0;
				//for (size_t y = 0; y < cubes.info.cubesCount.y; y++)
				//{
				index = y + z * cubes.info.cubesCount.y + x * cubes.info.cubesCount.y*  cubes.info.cubesCount.z;
				auto &var = cubes.cubes[index];

				for (size_t i = 0; i < var.summPowerImage.elementsCount; i++)
				{
					max = std::max(max, var.summPowerImage.image[i]);
					if (var.summPowerImage.image[i] != 0)
					min = std::min(min, var.summPowerImage.image[i]);
				}
				//}
				//HANDLE_ERROR(cudaThreadSynchronize());
			}
		}
		str.append(QString("\n sumImage min: %1; max: %2").arg(min).arg(max));


		return str;
	}

	QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>> getSurf(float addMin)
	{
		auto &cubes = getCurrentCubes();
		if (cubes.cubes.isEmpty())
		return QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>>();

		cubes.createSumImage();

		QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>> list;


		unsigned max = 0x0;// cubes.cubes[0].summPowerImage.image[0];
		auto min = 0xFFFFFFFF;

		int index = 0;
		for (size_t x = 0; x <cubes.info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < cubes.info.cubesCount.z; z++)
			{
				size_t y = 0;
					index = y + z * cubes.info.cubesCount.y + x * cubes.info.cubesCount.y*  cubes.info.cubesCount.z;
					auto &var = cubes.cubes[index];

					for (size_t i = 0; i < var.summPowerImage.elementsCount; i++)
					{
						max = std::max(max, var.summPowerImage.image[i]);

						if(var.summPowerImage.image[i] != 0)
						min = std::min(min, var.summPowerImage.image[i]);
					}
			}
		}
		if (max == 0) max = 1;
		if (min == max)min--;
		for (size_t x = 0; x < cubes.info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < cubes.info.cubesCount.z; z++)
			{
				size_t y = 0;
				//for (size_t y = 0; y < cubes.info.cubesCount.y; y++)
				//{
					index = y + z * cubes.info.cubesCount.y + x * cubes.info.cubesCount.y*  cubes.info.cubesCount.z;
					auto &var = cubes.cubes[index];

					QImage image(var.cube.size.x, var.cube.size.z, QImage::Format_ARGB32);
					image.fill(Qt::white);
					uint32_t* data = (uint32_t*)(image.bits());

#if 0
					for (size_t i = 0; /*i < var.summPowerImage.elementsCount && */i < cubeMaxSize_X*cubeMaxSize_Z; i++)
					{
						data[i] = qRgb(var.summPowerImage.image[i] / max * 255, 0, 0);
					}
					list << QPair<QtDataVisualization::QSurfaceDataArray*, QImage>(getSurfaceData(var), image);
#else
					QtDataVisualization::QSurfaceDataArray *surfData = new QtDataVisualization::QSurfaceDataArray;
					int imageIndex;
					int color;
					float coord;
					for (int j = 0; j < var.cube.size.z; j++)
					{
						auto dataRow = new QtDataVisualization::QSurfaceDataRow();
						for (int i = 0; i < var.cube.size.x; i++)
						{
							//if (j == var.cube.size.z || i == var.cube.size.x)
							//{

							//	
							//}
							imageIndex = var.cube.getCubeDataIndex(i, 0, j);
							
							color = ((float)var.summPowerImage.image[imageIndex] - min)/ (max-min) * 255;
							if (var.summPowerImage.image[imageIndex] != 0)
							data[imageIndex] = qRgb(color, color, color);
							coord = var.coordImage.image[imageIndex];
							if (coord < addMin || coord == 0)coord = addMin;
							
							*dataRow << (QVector3D(
								i*var.cube.metrsPerIndex.x + var.cube.coord.x, 
								coord,
								//var.summPowerImage.image[imageIndex] ,// / max * 255, 
								j*var.cube.metrsPerIndex.z + var.cube.coord.z));
						}
						*surfData << dataRow;
					}
					list << QPair<QtDataVisualization::QSurfaceDataArray*, QImage>(surfData, image.scaled(image.size() * 10));
#endif
					//QtDataVisualization::QSurfaceDataArray surf;

					
				//}
			}
		}


		return list;
	}
	QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>> getImageXZ(float level)
	{
		auto &cubes = getCurrentCubes();
		if (cubes.cubes.isEmpty())
			return QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>>();

		QList<QPair<QtDataVisualization::QSurfaceDataArray*, QImage>> list;

		cubes.createSumImageXZ(level);

		unsigned max = 0x0;// cubes.cubes[0].summPowerImage.image[0];
		auto min = 0xFFFFFFFF;

		int index = 0;
		for (size_t x = 0; x < cubes.info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < cubes.info.cubesCount.z; z++)
			{
				size_t y = 0;
				index = y + z * cubes.info.cubesCount.y + x * cubes.info.cubesCount.y*  cubes.info.cubesCount.z;
				auto &var = cubes.cubes[index];

				for (size_t i = 0; i < var.summPowerImageXZ.elementsCount; i++)
				{
					max = std::max(max, var.summPowerImageXZ.image[i]);

					if (var.summPowerImageXZ.image[i] != 0)
						min = std::min(min, var.summPowerImageXZ.image[i]);
				}
			}
		}
		if (max == 0) max = 1;
		if (min == max)min--;
		for (size_t x = 0; x < cubes.info.cubesCount.x; x++)
		{
			for (size_t z = 0; z < cubes.info.cubesCount.z; z++)
			{
				size_t y = 0;
				//for (size_t y = 0; y < cubes.info.cubesCount.y; y++)
				//{
				index = y + z * cubes.info.cubesCount.y + x * cubes.info.cubesCount.y*  cubes.info.cubesCount.z;
				auto &var = cubes.cubes[index];

				QImage image(var.cube.size.x, var.cube.size.z, QImage::Format_ARGB32);
				image.fill(Qt::white);
				uint32_t* data = (uint32_t*)(image.bits());

#if 0
				for (size_t i = 0; /*i < var.summPowerImage.elementsCount && */i < cubeMaxSize_X*cubeMaxSize_Z; i++)
				{
					data[i] = qRgb(var.summPowerImage.image[i] / max * 255, 0, 0);
				}
				list << QPair<QtDataVisualization::QSurfaceDataArray*, QImage>(getSurfaceData(var), image);
#else
				QtDataVisualization::QSurfaceDataArray *surfData = new QtDataVisualization::QSurfaceDataArray;
				int imageIndex;
				int color;
				float coord;

#if 0
				for (int j = 0; j < var.cube.size.z; j+= var.cube.size.z-1)
				{
					auto dataRow = new QtDataVisualization::QSurfaceDataRow();
					for (int i = 0; i < var.cube.size.x; i+= var.cube.size.x-1)
#else
				for (int j = 0; j < var.cube.size.z; j ++)
				{
					auto dataRow = new QtDataVisualization::QSurfaceDataRow();
					for (int i = 0; i < var.cube.size.x; i ++)
#endif
					{
						*dataRow << (QVector3D(
							i*var.cube.metrsPerIndex.x + var.cube.coord.x,
							level,
							//var.summPowerImage.image[imageIndex] ,// / max * 255, 
							j*var.cube.metrsPerIndex.z + var.cube.coord.z));
					}
					*surfData << dataRow;
				}

				for (int j = 0; j < var.cube.size.z; j++)
				{
					//auto dataRow = new QtDataVisualization::QSurfaceDataRow();
					for (int i = 0; i < var.cube.size.x; i++)
					{
						imageIndex = var.cube.getCubeDataIndex(i, 0, j);
						
						color = ((float)var.summPowerImageXZ.image[imageIndex] - min) / (max - min) * 255;
						if (var.summPowerImageXZ.image[imageIndex] != 0)
						data[imageIndex] = qRgb(color, color, color);
						//*dataRow << (QVector3D(
						//	i*var.cube.metrsPerIndex.x + var.cube.coord.x,
						//	level,
						//	//var.summPowerImage.image[imageIndex] ,// / max * 255, 
						//	j*var.cube.metrsPerIndex.z + var.cube.coord.z));
					}
					//*surfData << dataRow;
				}
				list << QPair<QtDataVisualization::QSurfaceDataArray*, QImage>(surfData, image.scaled(image.size() * 10));
#endif
				//QtDataVisualization::QSurfaceDataArray surf;


			//}
			}
		}


		return list;
	}

	QVector3D getCubesCount()
	{
		auto &cubes = getCurrentCubes();
		if (cubes.cubes.isEmpty())
			return QVector3D(0, 0, 0);
		
		return QVector3D(cubes.info.cubesCount.x, cubes.info.cubesCount.y, cubes.info.cubesCount.z);
	}

	void setConfig(CudaConfig cfg)
	{
		auto &cubes = getCurrentCubes();
		cubes.config = cfg;
	}
	CudaConfig getConfig()
	{
		auto &cubes = getCurrentCubes();
		return cubes.config;
	}

	SceneDataInfo getSceneDataInfo()
	{
		auto &cubes = getCurrentCubes();
		return cubes.info;
	}
}