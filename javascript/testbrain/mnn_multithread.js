//var MNN = require('./mnn.js');
//var _sqlite3 = require('better-sqlite3');//('foobar.db', options);
//var mnn_indicator = require('./mnn_indicator.js'); 

var threads = require('threads');
var spawn = threads.spawn;
var Thread = threads.Thread;
var Worker = threads.Worker;
var expose = threads.expose;
//var isMainThread = threads.worker_threads.isMainThread;

const {
  MessageChannel, MessagePort, isMainThread, parentPort
} = require('worker_threads');

var brain = require('brain.js');

/* class testAsyncClass {
  log(arg) { console.log("testAsyncClass " + arg) }
  toJSON() {
    return { name: "testAsyncClass" }
  }
} */

if (isMainThread) {


  async function multithread0(result, a, b, shared) {
    console.log("start0 " + b)
    let tt = new testAsyncClass
    const multithread = await spawn(
      new Worker("./mnn_multithread"/* , { eval: true } */)
    )
    const sum = await multithread.add(a, b)

    let startFun = await multithread.startFun(2, { a: 2, b: 3, c: 0 })
    console.log(startFun)
    result.push(sum);
    console.log("multithread0: " + `2 + 3 = ${sum}`)
    Atomics.notify(shared, 0)
    //Atomics.wake(shared,0)
    //await Thread.completed(multithread)
    await Thread.terminate(multithread)
  }

  async function wait() {
    const pool = Pool(() => spawn(new Worker("./mnn_multithread")), 8 /* optional size */)
    pool.queue(multithread0
      /*     async multiplier => {
        const multiplied = await multiplier(2, 3)
        console.log(`2 * 3 = ${multiplied}`)
      } */
    )
    await pool.completed()
    await pool.terminate()
  }
  async function train(startIndex, trainSize, name, rows) {
    const multithread = await spawn(new Worker("./mnn_multithread"/* , { eval: true } */))
    let result = await multithread.train(startIndex, trainSize, name, rows)
    console.log(result)

    await Thread.terminate(multithread)
  }

  module.exports.multithread0 = multithread0;
  module.exports.wait = wait;
  module.exports.train = train;
}
else {

  var expose = threads.expose;
  var multithread = {
    add(a, b) {
      for (let index = 0; index < 200000000; index++) {
        a = a

      }
      return a + b
    },
    startFun(fun, arg) {
      //fun.log(arg)
      //workerData.log()
      //console.log("startFun")
      let tt = new testAsyncClass
      return tt//.toJSON()
      return "startFun:\t" + JSON.stringify(arg)//"returned startFun"
    },

    train(startIndex, trainSize, name, rows) {
/*        let candlesCollector = new mnn_indicator.CandleInputDataCreator;

      //console.log("------------------");
      //console.log(startIndex + "\t" + rows.length);
      //console.log("------------------");
      let secondName = rows[startIndex].start;
      let mnn = new MNN.MyNeuralNetwork(name, secondName);
      //console.log(mnn.hello());
      //console.log(mnn.name + "\t" + secondName);
      let chooseInterval = 0

      let intervalStep = 0//rows.length / trainSize;    
      for (let index = startIndex; index < rows.length - candlesCollector.interval - 1 && mnn.data.length < trainSize; index++) {
        let element = rows[index];
        if (candlesCollector.pushNewTestData(element)) {
          chooseInterval++;
          if (chooseInterval > intervalStep) {
            let data = candlesCollector.getComplyteArray();
            let testPrices = new Array;
            for (let index2 = index + 1; index2 <= index + candlesCollector.interval; index2++) {
              testPrices.push(rows[index2].close);
            }
            let out = candlesCollector.getOutParams(element.close, testPrices);
            mnn.data.push({ id: element.start, input: data, output: out });
            chooseInterval = 0;
          }
        }
      }

      let data_sell = mnn.data.filter(data => data.output.sell > 0)
      let data_hold = mnn.data.filter(data => data.output.hold > 0)
      let data_buy = mnn.data.filter(data => data.output.buy > 0)

      let minDataSize = Math.min(data_sell.length, data_hold.length, data_buy.length);
      if (minDataSize > 10) {
        //console.log("train data: " + minDataSize + "\tfrom " + [data_sell.length, data_hold.length, data_buy.length])
        mnn.data = []
        for (let shuffleIndex = 0; shuffleIndex < minDataSize; shuffleIndex++) {
          mnn.data.push(data_sell[shuffleIndex]);
          mnn.data.push(data_hold[shuffleIndex]);
          mnn.data.push(data_buy[shuffleIndex]);
        }
        if (1)
          mnn.train();

        //mnn.selfTest();
        //mnn.writer();
      } else {
        //console.log("NOT ENOUTH train data: " + minDataSize + "\tfrom " + [data_sell.length, data_hold.length, data_buy])
      }  */
      return 777;
    }
  }
  //expose(mulfun)

  expose(multithread)

}