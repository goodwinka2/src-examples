
var MNN = require("./mnn.js");
var _sqlite3 = require('better-sqlite3');//('foobar.db', options);
var mnn_indicator = require("./mnn_indicator.js");
var mnn_thread = require("./mnn_multithread.js")
var fmin = require('fmin')
var math = require('mathjs')

class PrecentViewer {
  constructor(max, min = 0) {
    this.max = max;
    this.min = min;
    this.onePrecent = (max - min) / 100
    this.lastPrecent = 0
  }
  update(value) {
    let precent = (value - this.min) / this.onePrecent;
    if (precent - this.lastPrecent > 5) {
      this.lastPrecent = precent;
      console.log("ready " + Math.floor(this.lastPrecent) + " %")
    }
  }
  lastPrecent
  onePrecent
  max
  min
}

getErrArray = (arr1, arr2, value = 4) => {
  let newarr = Object.assign({}, arr1)// arr.slice();;// = new Array;//arr.slice();
  /*   for (key in arr) {
      if (arr.hasOwnProperty(key)) 
      newarr.push(arr[key]);
    }   */
  value = Math.pow(10, value);
  for (key in newarr) {
    if (newarr.hasOwnProperty(key))
      newarr[key] = parseInt((arr1[key] - arr2[key]) * value, 10) / value;
    //newarr[key] = parseInt(newarr[key]*value,10)/value;//Math.floor( newarr[key]*value)/value;
  }
  return newarr;
};



testBrain = () => {
  mnn = new MNN.MyNeuralNetwork("TEST_CURRENCY");
  console.log(mnn.hello());
  console.log(mnn.name);
  console.time('createtime');

  let max = 1000.0;
  for (let i = 1; i < max; i += 1) {
    //mnn.data.push(i);
    testData = new Float32Array(200);
    f = Math.sin(i);
    f2 = Math.cos(i);
    let index;
    if (true) {
      for (index = 1; index < testData.length / 2; index++) {
        testData[index] = 3 * Math.abs(Math.sin(f + (index) * Math.PI / 180));
      }
      for (index; index < testData.length; index++) {
        testData[index] = -1 * Math.abs(Math.sin(f2 + (index) * Math.PI / 180));
      }
    }
    else {
      for (index = 1; index < testData.length; index++) {
        testData[index] = (Math.abs(Math.sin(f + (index) * Math.PI / 180)) + Math.abs(Math.sin(f2 + (index) * Math.PI / 180))) / 2;

      }
    }
    //if (i != 0)
    newTestData = MNN.normArray(testData);
    //mnn.data.push({ id: i, input: testData, output: [testf(testData)] });
    mnn.data.push({ id: i, input: newTestData, output: [f, f2] });
    //mnn.writer();
    mnn.tryTrain();
    //mnn.data.push({ input: testData, output: testData });
  }
  console.timeEnd('createtime');
  for (let i = 0; i < mnn.data.length; i++) {
    //console.log(mnn.data[i])
  }
  mnn.writer();
  mnn.train();
  mnn.selfTest();


  /* mnn.writer();
  mnn.reader();
  mnn.train();
  mnn.selfTest(); */


  return
}


loadAndTest = (dbName) => {
  let dbData = new _sqlite3(dbName);

  let rows = dbData.prepare("SELECT * FROM sqlite_master WHERE type = 'table'").all();
  let candlesCollector = new mnn_indicator.CandleInputDataCreator;

  rows.forEach(element => {
    if (element.name.indexOf("candles_") == -1)
      return;
    let name = element.name;

    let rows = dbData.prepare(`SELECT * FROM ${name}`).all();

    rows.sort(function (a, b) {
      return a.start - b.start;
    });

    let trainSize = 20000;
    let startIndex = 1
    let finishIndex = startIndex - 1 - trainSize + trainSize * (1 + 1)// */+ rows.length
    for (startIndex = 0; startIndex < finishIndex /* rows.length - trainSize-1 */; startIndex += trainSize) {
      //}
      console.log("------------------");
      console.log(startIndex + "\t" + rows.length);
      console.log("------------------");
      let secondName = rows[startIndex].start;
      mnn = new MNN.MyNeuralNetwork(name, secondName);
      console.log(mnn.hello());
      console.log(mnn.name + "\t" + secondName);
      let chooseInterval = 0

      let intervalStep = 0//rows.length / trainSize;    
      for (let index = startIndex; index < rows.length - candlesCollector.interval - 1 && mnn.data.length < trainSize; index++) {
        let element = rows[index];
        if (candlesCollector.pushNewTestData(element)) {
          chooseInterval++;
          if (chooseInterval > intervalStep) {
            let data = candlesCollector.getComplyteArray();
            let testPrices = new Array;
            for (let index2 = index + 1; index2 <= index + candlesCollector.interval; index2++) {
              testPrices.push(rows[index2].close);
            }
            let out = candlesCollector.getOutParams(element.close, testPrices);
            mnn.data.push({ id: element.start, input: data, output: out });
            chooseInterval = 0;
          }
        }

        //mnn.writer();
        //mnn.tryTrain();
      }

      let data_sell = mnn.data.filter(data => data.output.sell > 0)
      let data_hold = mnn.data.filter(data => data.output.hold > 0)
      let data_buy = mnn.data.filter(data => data.output.buy > 0)

      let minDataSize = Math.min(data_sell.length, data_hold.length, data_buy.length);
      if (minDataSize > 10) {
        console.log("train data: " + minDataSize + "\tfrom " + [data_sell.length, data_hold.length, data_buy.length])
        mnn.data = []
        for (let shuffleIndex = 0; shuffleIndex < minDataSize; shuffleIndex++) {
          //const element = array[index];
          mnn.data.push(data_sell[shuffleIndex]);
          mnn.data.push(data_hold[shuffleIndex]);
          mnn.data.push(data_buy[shuffleIndex]);
        }
        //mnn.data.concat(data_sell.slice(0,minDataSize), data_hold.slice(0,minDataSize), data_buy.slice(0,minDataSize));
        //mnn.loadNet();
        if (1)
          mnn.train();

        mnn.selfTest();
        //mnn.writer();
      } else {
        console.log("NOT ENOUTH train data: " + minDataSize + "\tfrom " + [data_sell.length, data_hold.length, data_buy])
      }

    }
  });



}




//testBrain();
//loadAndTest("binance_0.1.db");


loadNetAndTest = (dbName, netDbName) => {
  let dbData = new _sqlite3(dbName);
  let rows = dbData.prepare("SELECT * FROM sqlite_master WHERE type = 'table'").all();
  let candlesCollector = new mnn_indicator.CandleInputDataCreator;

  rows.forEach(element => {

    if (element.name.indexOf("candles_") == -1)
      return;
    let name = element.name;

    let rows = dbData.prepare(`SELECT * FROM ${name}`).all();

    rows.sort(function (a, b) {
      return a.start - b.start;
    });


    let myNets = new Array;
    let dbNetData = new _sqlite3(netDbName);
    let netTable = dbNetData.prepare("SELECT additionalName FROM candles_USDT_BTC ").all();

    //let name = "candles_USDT_BTC";

    //let trainSize = 5000;
    let startIndex = 1
    netTable.forEach(element => {

      let secondName = element.additionalName;
      myNets.push(new MNN.MyNeuralNetwork(name, secondName));
      let mnn = myNets[myNets.length - 1];
      mnn.loadNet();
      let netErrors = new Array;
      console.log(mnn.hello());
      console.log(mnn.name + "\t" + secondName);
      let chooseInterval = 0

      let intervalStep = 0//rows.length / trainSize;    
      for (let index = startIndex; index < rows.length / 100; index++) {

        let element = rows[index];
        if (candlesCollector.pushNewTestData(element)) {
          chooseInterval++;
          if (chooseInterval > intervalStep) {
            let data = candlesCollector.getComplyteArray();
            let testPrices = new Array;
            for (let index2 = index + 1; index2 <= index + candlesCollector.interval; index2++) {
              testPrices.push(rows[index2].close);
            }
            let out = candlesCollector.getOutParams(element.close, testPrices);
            //mnn.data.push({ id: element.start, input: data, output: out });
            let netOut = mnn.run(data);
            let netErr = netOut - out;
            netErrors.push(netErr);
            console.log(mnn.name + "\t" + mnn.additionalName + "\t" + JSON.stringify(MNN.floorArray(out)) + "\t\t" + JSON.stringify(MNN.floorArray(netOut)) + "\t\t" + JSON.stringify(getErrArray(out, netOut)) /* netErr */);
            chooseInterval = 0;
          }
        }


      }

      //

      //mnn.writer();
      console.log(mnn.name + "\t" + mnn.additionalName + "\tmean\t" + MNN.mean(netErrors) + "\t\tmedian\t" + MNN.median(netErrors));
    })

  });


}

//loadNetAndTest("binance_0.1.db","MyNeuralNetwork_dbNets.sqlite");
/* let arr = new Array;
for (let index = 0; index < 10; index++) {
  arr.push({a: index, b:0});  
};
for(let element of  arr){
  element.b = 99;
}  */
loadAndTestIndicator = (dbName, maxData = 10000) => {
  let mnnIndicator = new mnn_indicator.MnnIndicator("candles_USDT_BTC");

  let dbData = new _sqlite3(dbName);
  let rows = dbData.prepare("SELECT * FROM sqlite_master WHERE type = 'table'").all();
  //let mnnIndicator = new mnn_indicator.MnnIndicator;

  rows.forEach(element => {

    if (element.name.indexOf("candles_") == -1)
      return;
    let name = element.name;

    let row = dbData.prepare(`SELECT * FROM ${name}`).all();

    row.sort(function (a, b) {
      return a.start - b.start;
    });

    let trade = {
      isTrade: false,
      buePrice: 0,
      totalDynamicPrecent: 0,
      totalDynamicDelta: 0,
      totalDynamicDeltaWhithTax: 0,
      tax: 0.1 / 100,
      totalTrades: 0,
      totalTry: 0,
      max: 0,
      min: 0,
      canSell: 0,
      canBuy: 0
    }

    let p = new PrecentViewer(maxData/* row.length / 2 */);
    for (let index = 0; index < row.length && index < maxData; index++) {
      p.update(index);
      let element = row[index];
      if (mnnIndicator.update(element)) {
        let result = mnnIndicator.getResult();
        if (result != undefined) {
          trade.totalTry++;
          if (result.sell > result.hold && result.sell > result.buy) {
            trade.canSell++
          }
          if (result.buy > result.hold && result.buy > result.sell) {
            trade.canBuy++
          }
          if (trade.isTrade) {
            let precent = (element.close - trade.buePrice) / (trade.buePrice / 100)
            if (result.sell > result.hold && result.sell > result.buy  /*|| precent< 3  /* && (element.close-trade.buePrice)/(trade.buePrice/100)>0.1 */) {
              trade.totalDynamicPrecent += precent
              let delta = (element.close - trade.buePrice)
              trade.max = Math.max(trade.max, delta)
              trade.min = Math.min(trade.min, delta)
              trade.totalDynamicDelta += delta
              trade.totalDynamicDeltaWhithTax += delta - element.close * trade.tax;
              //buePrice = element.close;
              trade.isTrade = false;
              trade.totalTrades++;
            }

          }
          else {
            if (result.buy > result.hold && result.buy > result.sell) {
              trade.buePrice = element.close;
              trade.isTrade = true;
            }
          }
          //result.buy;
          //result.hold;
          //result.sell;
        }
      }
    }

    for (let element of mnnIndicator.mnnChecker.nmm) {
      console.log("rnet " + element.net.additionalName
        + "\tchoosed: " + element.totalChoosed
        + "\tgood: " + JSON.stringify(element.totalGoodChoices)
        + "\tbad: " + JSON.stringify(element.totalBadChoices)
        + "\terr: " + JSON.stringify(MNN.floorArray([element.error]))
        + "\terrEMA: " + JSON.stringify(MNN.floorArray([element.dynamicError.get()]))

      )
    }
    console.log("totalDataWithNet: " + mnnIndicator.mnnChecker.totalDataWithNet);
    console.log("totalDataWithoutNet: " + mnnIndicator.mnnChecker.totalDataWithoutNet);
    console.log("result: " + 100 * mnnIndicator.mnnChecker.totalDataWithNet / (mnnIndicator.mnnChecker.totalDataWithNet + mnnIndicator.mnnChecker.totalDataWithoutNet));
    console.log("totalGood: " + JSON.stringify(mnnIndicator.mnnChecker.totalGoodChoices));
    console.log("totalBad: " + JSON.stringify(mnnIndicator.mnnChecker.totalBadChoices));
    console.log("result: " + 100 * mnnIndicator.mnnChecker.totalGoodChoices.total / (mnnIndicator.mnnChecker.totalGoodChoices.total + mnnIndicator.mnnChecker.totalBadChoices.total) + " %");
    console.log("totalDynamicPrecent " + trade.totalDynamicPrecent + " %")
    console.log("totalDynamicDelta " + trade.totalDynamicDelta)
    console.log("totalDynamicDeltaWhithTax " + trade.totalDynamicDeltaWhithTax)
    console.log("totalTrades " + trade.totalTrades)
    console.log("totalTry " + trade.totalTry + "\tcanSell:\t" + trade.canSell + "\tcanBuy:\t" + trade.canBuy)
    console.log("min " + trade.min)
    console.log("max " + trade.max)
  });
}

const child_process = require('child_process')

function promiseFromChildProcess(child) {
  return new Promise(function (resolve, reject) {
    child.addListener("error", reject);
    child.addListener("exit", resolve);
  });
}


async function asyncLoadAndTrain(dbName) {
  let dbData = new _sqlite3(dbName);

  let rows = dbData.prepare("SELECT * FROM sqlite_master WHERE type = 'table'").all();
  console.time('asyncLoadAndTrain');
  let waitPromises = [];
  rows.forEach(element => {
    if (element.name.indexOf("candles_") == -1)
      return;
    let name = element.name;

    let rows = dbData.prepare(`SELECT * FROM ${name}`).all();

    rows.sort(function (a, b) {
      return a.start - b.start;
    });

    let trainSize = 20000;
    let startIndex = 1
    let finishIndex = startIndex - 1 - trainSize + trainSize * (1 + 1)// */+ rows.length

    for (startIndex = 0; startIndex < finishIndex /* rows.length - trainSize-1 */; startIndex += trainSize) {
      //let out = mnn_thread.train(startIndex, trainSize, name, rows)
      //waitPromises.push(out);
      console.log(`Try start : ${startIndex}`);
      const child = child_process.fork('mnn_child.js',
        [],
        {
          detached: true,
          //silent: false,
          //stdio: ['inherit', 'inherit','ipc','inherit']
        }
      )
      waitPromises.push(promiseFromChildProcess(child))
      let outData = { startIndex: startIndex, trainSize: trainSize, name: name, rows: rows }
      child.send({ command: 'init', data: outData })

      child.on('message', (code) => {
        console.log(`Message to parent: ${code}`)

      }
      )

      /*       child.on('close', (code) =>
              console.log(`Child process exited. Code: ${code}`)
            ) */

      //child.send(/* JSON.stringify */({command:'exit',data: []}))
    }
  });



  if (1)
    for (out of waitPromises) {
      await out
    }//Atomics.wait(shared,0) 
  console.log("FINNISH!")
  console.timeEnd('asyncLoadAndTrain');

  loadAndTestIndicator("binance_0.1_test.db");
}

var IndicatorCollector = require('./my_indicators.js').IndicatorCollector;
loadAndTestSimpleIndicator = (dbName, maxData = 10000) => {
  //let mnnIndicator = new mnn_indicator.MnnIndicator("candles_USDT_BTC");

  let dbData = new _sqlite3(dbName);
  let rows = dbData.prepare("SELECT * FROM sqlite_master WHERE type = 'table'").all();
  //let mnnIndicator = new mnn_indicator.MnnIndicator;

  rows.forEach(element => {

    if (element.name.indexOf("candles_") == -1)
      return;
    let name = element.name;

    let row = dbData.prepare(`SELECT * FROM ${name}`).all();

    row.sort(function (a, b) {
      return a.start - b.start;
    });

    if (1) {
      //let indicatorCollector = new IndicatorCollector
      let param = {
        //solution is at 12886.716001707595,9505.895147627041,12.808868232365219,-103.44178971380674
        //solution is at 20383.038204403176,-21488.955896192856

        a0: 972.7960693359364,//710,
        a1: 228.05908203125,//543
        //a2: 12.808868232365219,//710,//
        //a3: -103.44178971380674,//543,//
      }
      let indicatorCollector = new IndicatorCollector(param)
      let p = new PrecentViewer(maxData/* row.length / 2 */);
      let index = 0
      let prevSize = 0
      for (index = 0; index < row.length && index < maxData; index++) {
        p.update(index);
        let element = row[index];
        indicatorCollector.updateCandle(element)
        if(prevSize < indicatorCollector.indicators[0].history.length){
            let yahuu = 777
        }
        prevSize = indicatorCollector.indicators[0].history.length
      }
      console.log("days: \t\t" + index / 60 / 24);
      console.log("trade/day: \t\t" + indicatorCollector.indicators[0].total * 60 * 24 / index);
      console.log("total: \t\t" + indicatorCollector.indicators[0].total);
      console.log("totalCorrect: \t" + indicatorCollector.indicators[0].totalCorrect);
      console.log("correct:\t" + indicatorCollector.indicators[0].correctPrecent);
      console.log("result: \t" + indicatorCollector.indicators[0].result + "\t" + indicatorCollector.indicators[0].result / indicatorCollector.indicators[0].startPrice * 100);

    }else{

      let optFun = (x) =>{
        
        let paramIndex = 0

        let param = {
          minValue: x[paramIndex++],
          //count: x[paramIndex++],
          //a0: 1,//x[paramIndex++],//710,//
          minDelta: x[paramIndex++],
          //a1: x[paramIndex++],//543,//
          //a2: x[paramIndex++],//710,//
          //a3: x[paramIndex++],//543,//
          //a4: x[paramIndex++],//710,//
/*            bk0:x[paramIndex++],
          bk1:x[paramIndex++],
          bk2:x[paramIndex++],
          bk3:x[paramIndex++],
          sk0:x[paramIndex++],
          sk1:x[paramIndex++],
          sk2:x[paramIndex++],
          sk3:x[paramIndex++],
          hk0:x[paramIndex++],
          hk1:x[paramIndex++],
          hk2:x[paramIndex++],
          hk3:x[paramIndex++],  */
        }
        let indicatorCollector = new IndicatorCollector(param)

        //let p = new PrecentViewer(maxData/* row.length / 2 */);
        let index = 0
        for (index = 0; index < row.length && index < maxData; index++) {
          //p.update(index);
          let element = row[index];
          indicatorCollector.updateCandle(element)
        }
        console.log("correct:\t" + indicatorCollector.indicators[0].correctPrecent +"\t" + "result: \t" + indicatorCollector.indicators[0].result + "\t" + indicatorCollector.indicators[0].result / indicatorCollector.indicators[0].startPrice * 100);
        //return -indicatorCollector.indicators[0].result

        let toReturn = 0;
        let median = -9999
        if(indicatorCollector.indicators[0].history.length > 10){
          median = math.median(indicatorCollector.indicators[0].history)
          toReturn = -9999
        }


        
        //toReturn-=median*1
        toReturn-=indicatorCollector.indicators[0].correctPrecent*1
        toReturn-=indicatorCollector.indicators[0].result / indicatorCollector.indicators[0].startPrice * 100
        return math.pow(toReturn,1)//-median*1 - indicatorCollector.indicators[0].correctPrecent*1 - 0*indicatorCollector.indicators[0].result / indicatorCollector.indicators[0].startPrice * 100
      }

      let parameters = {
        rho:50,
        chi:100,
        //psi:-0.8,
        //sigma:0.2,
        //minErrorDelta: 1e-9
        //zeroDelta: 1e-0,
        maxIterations:800
      }
/*       var maxIterations = parameters.maxIterations || x0.length * 200,
      nonZeroDelta = parameters.nonZeroDelta || 1.05,
      zeroDelta = parameters.zeroDelta || 0.001,
      minErrorDelta = parameters.minErrorDelta || 1e-6,
      minTolerance = parameters.minErrorDelta || 1e-5,
      rho = (parameters.rho !== undefined) ? parameters.rho : 1,
      chi = (parameters.chi !== undefined) ? parameters.chi : 2,
      psi = (parameters.psi !== undefined) ? parameters.psi : -0.5,
      sigma = (parameters.sigma !== undefined) ? parameters.sigma : 0.5; */
      var solution = fmin.nelderMead(optFun, 
        [//5.110997884814012,-9.569123647786625 //,100//10, 10, 1, 1,1
          0,0.2

          //852.2894966877793,-209.21764551675142,-23.601746184240255,-2.3585725298899263,-11.280471384283459
        //-419.2193057550361 ,-1966.0093012071388 ,19.296688477809397,1.9034995362741793,8.875043969807447
        //10,10,10
        //-1, 0, 0 , 0
        //,1, 0, 0 , 0
        //,0, 1, 1, 1
        //-1, 1, 1 , 1
      //,1, -1, 1 , 1
      //,0, 0, -1, -1
      ]  
      //solution is at 832.5838353289987,-2962.360798561025
      //[16659.387052409467,-8241.01859122196]
      //math.random([12], -1, 1)
      ,parameters);
      console.log("solution is at " + solution.x);      


    }
  });
}

/* 
var threads = require(`threads`);
//import { expose } from "threads/worker"
//import { spawn, Thread, Worker } from "threads"
var spawn = threads.spawn;
var Thread = threads.Thread;
var Worker = threads.Worker;
var expose = threads.expose;



let res = []

function waitForAll(arr) {
  var count = arr.length;
  arr.forEach((p) => {
    p.then(
      (r) => {
        count--;
        if (count == 0) console.log('all done');
      },
      (e) => {
        count--;
        if (count == 0) console.log('all done');
      });
  });
  return true;
};
const shared = new Int32Array(new SharedArrayBuffer(4))
async function asyncWait(){
  let wait = []
  for (let index = 0; index < 1; index++) {
    let out = mnn_thread.multithread0(res,0,index,shared)
    wait.push(out);

    //await out;
    //Promise.all([out])
    //console.log(waitForAll([out]));  
    //mnn_thread.wait();
    console.log(res);
  }
  if(1)
  for(out of wait)
  {
    await out
  }//Atomics.wait(shared,0)

  console.log(res);
  
}
testasync=()=>{
asyncWait();

//Atomics.wait(shared,0)
//main().catch(console.error)
console.log("end testasync")
} */

//testasync()




//loadAndTestSimpleIndicator("binance_0.1.db",60*24*7*2);
loadAndTestSimpleIndicator("binance_0.1_test.db", 60 * 24 * 7 * 4);
//loadAndTest("binance_0.1.db");
//asyncLoadAndTrain("binance_0.1.db");
//loadNetAndTest("binance_0.1.db","MyNeuralNetwork_dbNets.sqlite");
//loadAndTestIndicator("binance_0.1_test.db",60*24*7*4);
//loadAndTestIndicator("binance_0.1.db",60*24*7*4);
console.log("end");
//while(true){}



/*
var http = require('http');

http.createServer(function(request, respone){
  respone.writeHead(200, {'Content-type':'text/plan'});
  response.write('Hello Node JS Server Response');
  response.end( );

}).listen(7000); */


/* const http = require("http");
const fs = require('fs').promises;

const host = '127.0.0.1';
const port = 8000;

const books = JSON.stringify([
  { title: "The Alchemist", author: "Paulo Coelho", year: 1988 },
  { title: "The Prophet", author: "Kahlil Gibran", year: 1923 }
]);

const authors = JSON.stringify([
  { name: "Paulo Coelho", countryOfBirth: "Brazil", yearOfBirth: 1947 },
  { name: "Kahlil Gibran", countryOfBirth: "Lebanon", yearOfBirth: 1883 }
]);

const requestListener = function (req, res) {
  //
/*   res.setHeader("Content-Type", "text/html");
  res.writeHead(200);
  res.end(`<html><body><h1>This is HTML</h1></body></html>`);
  //fs.readFile("index.html")
  fs.readFile("chart.html")
                            .then(contents => {
                              res.setHeader("Content-Type", "text/html");
                              res.writeHead(200);
                              res.end(contents);
                          })
                          .catch(err => {
                            res.writeHead(500);
                            res.end(err);
                            return;
                          });
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});
*/