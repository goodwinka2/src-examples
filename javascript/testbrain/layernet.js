const brain = require('brain.js');
const { PerformanceObserver, performance } = require('perf_hooks');

const defaultLayerCount = 2;

strangeToArray=(arr)=>
{
  let size = 0;
  let result = new Array;
  for (key in arr) {
    if (arr.hasOwnProperty(key)) 
    {result.push(arr[key]);size++;}
    
}
return result;
}
floorArray = (arr,value = 4) =>{
  let newarr = new Array;//arr.slice();
  for (key in arr) {
    if (arr.hasOwnProperty(key)) 
    newarr.push(arr[key]);
  }  
  value = Math.pow(10,value);
  for (key in newarr) {
    if (newarr.hasOwnProperty(key)) 
    newarr[key] = parseInt(newarr[key]*value,10)/value;//Math.floor( newarr[key]*value)/value;
  }  
  return newarr;
};

summArray = (arr) => {
  var element = 0.0;
  var size = 0, key;
  for (key in arr) {
      if (arr.hasOwnProperty(key)) 
      element += Math.abs(arr[key]);
  }
  return element;
  var element = 0.0;
  for (let index = 0; index < arr.length; index++) {
    element += Math.abs(arr[index]);
  }
  //if(element==0)element=1;
  return element;
};

function median(values){
  if(values.length ===0) return 0;

  values.sort(function(a,b){
    return a-b;
  });

  var half = Math.floor(values.length / 2);

  if (values.length % 2)
    return values[half];

  return (values[half - 1] + values[half]) / 2.0;
}

function mean(values){
  return summArray(values)/values.length;
}

//var currentLayerNetwork;

class LayerNetwork {
    constructor(num=defaultLayerCount,name) {
      //currentLayerNetwork = this;
      this.setlayerCount(num);
    }
    setlayerCount(num) {
      //this.nets.clear();
      this.nets = new Array;
      //this.nets.push(new this.layerNets());
      //this.nets.shift();
      for (let index = 0; index < num; index++) {
        this.nets.push(new this.layerNets());
      }
      this.finalNet = new this.finalNetType;
    }
    train(data, configDefault) {
      let result;
      
      console.time('trainLayers');
      let count = data[0].input.length*5;
      this.configLayer.hiddenLayers = [
        Math.floor(count),
        Math.floor(count/1.0),
        Math.floor(count/1.0),
        Math.floor(count/1.0),
/*         Math.floor(count/1.0),
        Math.floor(count/1.0),
        Math.floor(count/1.0),
        Math.floor(count/1.0),   
        Math.floor(count/1.0),
        Math.floor(count/1.0),  */
        Math.floor(count/2.0) ,
        //Math.floor(count/3),        
        //Math.floor(count/4.0),
        //Math.floor(count/1.0),
      ];
      this.layerOutputIndex =this.configLayer.hiddenLayers.length+1
      //this.configLayer.errorThresh = (mean(data[0].input)/count)*0.01;
      console.log("Layer "+ count +" hiddenLayers = " + this.configLayer.hiddenLayers);
      console.log(JSON.stringify(this.configLayer))
      let layerData;// = data.map(element => ({ input: element.input, output: element.input }));
      layerData = new Array;
      for (let index = 0; index < data.length; index++) {
        layerData.push({ input: data[index].input, output: data[index].output });
      }
  
      for (let index = 0; index < this.nets.length; index++) {
        this.prevError = 1
        this.trainTime = performance.now();
        //this.configLayer.hiddenLayers[this.layerOutputIndex-1]++
        if(this.useCrossValidateLayer){
          let crossValidate = new brain.CrossValidate(this.layerNets/* brain.NeuralNetwork */, this.configLayer);
          this.currentTrainedNet = crossValidate;
          result = crossValidate.train(layerData, this.configLayer);
          result = result.avgs;
          this.nets[index] =  crossValidate.toNeuralNetwork();
        }
        else
          {
            this.nets[index] = new this.layerNets(this.configLayer);
            this.currentTrainedNet =this.nets[index];
            result = this.nets[index].train(layerData, this.configLayer);
          }

        //for (let index2 = 0; index2 < data.length; index2++){
        //  layerData[index2].input = this.nets[index].run(layerData[index2].input);
        //}
        

        console.log(result);
      }
      console.timeEnd('trainLayers');
      

      layerData = new Array;// = data.map(element => ({ input: this.runLayerNet(element.input), output: element.output }));
      for (let index = 0; index < data.length; index++) {
        layerData.push({ input: this.runLayerNet(data[index].input), output: data[index].output });
      }
      
      console.time('trainFinal');
      this.configFinal.hiddenLayers = [
        Math.round(layerData[layerData.length-1].input.length/1),
        Math.round(layerData[layerData.length-1].input.length/1),
        Math.round(layerData[layerData.length-1].input.length/1),
        Math.round(layerData[layerData.length-1].input.length/1),
        Math.round(layerData[layerData.length-1].input.length/1),
        //Math.round(layerData[layerData.length-1].input.length/4),
/*        Math.round(layerData[layerData.length-1].input.length/1.0),
       Math.round(layerData[layerData.length-1].input.length/1.2),
       Math.round(layerData[layerData.length-1].input.length/1.4),
       Math.round(layerData[layerData.length-1].input.length/1.6),
       Math.round(layerData[layerData.length-1].input.length/1.8), */
      ];
      console.log("Final hiddenLayers = " + this.configFinal.hiddenLayers);
      this.prevError = 1
      this.trainTime = performance.now();
      if(this.useCrossValidate){
        let crossValidate = new brain.CrossValidate(this.finalNetType, this.configFinal);
        this.currentTrainedNet = crossValidate;
        result =  crossValidate.train(layerData, this.configFinal);
        result = result.avgs;
        this.finalNet =  crossValidate.toNeuralNetwork();
      }
      else
        {
          this.finalNet = new this.finalNetType(this.configFinal);
          this.currentTrainedNet =this.finalNet;
          result = this.finalNet.train(layerData,this.configFinal);
        }
      console.log(result);
      console.timeEnd('trainFinal');
      return result;
    }
    toJSON()
    {
      //json = new JSON;
      let layersJson = new Array;
      this.nets.forEach(element => {
        layersJson.push(JSON.stringify(element.toJSON()));
      });
      let json= {
        layerCount: this.nets.length, 
        layers: layersJson, 
        finalLayer: JSON.stringify(this.finalNet.toJSON()),
        useCrossValidate: this.useCrossValidate,
        useCrossValidateLayer: this.useCrossValidateLayer,
        layerOutputIndex: this.layerOutputIndex
      };
      return json;
    }
    fromJSON(json)
    {
      this.setlayerCount(json.layers.length);
      for (let index = 0; index < this.nets.length; index++) {
        this.nets[index].fromJSON(JSON.parse(json.layers[index]));      
      }
      this.finalNet.fromJSON(JSON.parse(json.finalLayer)); 
      this.useCrossValidate = JSON.parse(json.useCrossValidate);
      this.useCrossValidateLayer = JSON.parse(json.useCrossValidateLayer);
      this.layerOutputIndex = JSON.parse(json.layerOutputIndex);
    }
    
    run(data) {
      let result = this.runLayerNet(data);
      let result2 = this.finalNet.run(result);
      return result2;
    }
    runLayerNet(data) {
      let result =/* []// */ data.slice();
      if(this.nets.length != 0){
      for (let index = 0; index < this.nets.length; index++) {
        /* result = */ (this.nets[index].run(data));
        result = [].concat(result,strangeToArray(this.nets[index].outputs[this.layerOutputIndex]));
        //result = arrayToFloat32Arrays(result);
      }
      //result = (this.nets[this.nets.length-1].outputs[this.layerOutputIndex]);
      }
      //result
  /*     let a = new Float32Array(result.length);
      for (let index = 0; index < result.length; index++) {
        a[index] = result[index];
        
      } */
      return strangeToArray(result);
    }
    prevError = 1
    trainTime = 0
    searchDirection = true

    layerOutputIndex
    crossValidate;    
    useCrossValidate = false;
    useCrossValidateLayer = false;
    nets = new Array;
    //useFinalNets = true;
    //finalNets = new Array;
    currentTrainedNet
    finalNet
    finalNetType = brain.NeuralNetwork;//new brain.recurrent.GRUTimeStep;//new brain.NeuralNetwork;
    layerNets = brain.NeuralNetwork;//brain.recurrent.GRUTimeStep//brain.NeuralNetworkGPU//
    configLayer = {
      praxis: 'adam',
      activation: 'tanh', // activation function   | 'sigmoid' | 'relu' | 'leaky-relu' | 'tanh';
      //activation: 'sigmoid',
      hiddenLayers: [],//[50,5,50],
      iterations: 20, // the maximum times to iterate the training data --> number greater than 0
      errorThresh: 0.005, // the acceptable error percentage from training data --> number between 0 and 1
      log: false, // true to use console.log, when a function is supplied it is used --> Either true or a function
      logPeriod: 1, // iterations between logging out --> number greater than 0
      learningRate: 0.0003/2, // scales with delta to effect training rate --> number between 0 and 1
      momentum: 0.0001/2, // scales with next layer's change value --> number between 0 and 1
      //binaryThresh: 0.005,
      callback: (state)=>{
        if(this.useCrossValidateLayer){
          const eps = 0.0001
          let elapsed = performance.now() - this.trainTime;
          let dynamicError = this.prevError - state.error;
          //let timeToFinish = (state.error - this.currentTrainedNet.trainOpts.errorThresh )/dynamicError* elapsed /1000;
          console.log(state.iterations +
            "\terr: " + JSON.stringify(floorArray([state.error]) )+ 
            "\tderr: "+JSON.stringify(floorArray([dynamicError],6)) +
            "\tt: "+JSON.stringify(floorArray([elapsed])) );
            //"\tendt: " +JSON.stringify( floorArray([timeToFinish])) + " [s]") ;
            if(Math.abs(dynamicError) < eps && dynamicError > 0){
              //this.currentTrainedNet.trainOpts.errorThresh = state.error;
              console.log("cant train net");
            }    
          this.prevError = state.error;
          this.trainTime = performance.now();
          return;
        }
        //const eps =this.currentTrainedNet.trainOpts.errorThresh/5//0.00001;
        const eps = 0.0001
        let elapsed = performance.now() - this.trainTime;
        let dynamicError = this.prevError - state.error;
        let timeToFinish = (state.error - this.currentTrainedNet.trainOpts.errorThresh )/dynamicError* elapsed /1000;
        console.log(state.iterations +
          "\terr: " + JSON.stringify(floorArray([state.error]) )+ 
          "\tderr: "+JSON.stringify(floorArray([dynamicError],6)) +
          "\tt: "+JSON.stringify(floorArray([elapsed])) +
          "\tendt: " +JSON.stringify( floorArray([timeToFinish])) + " [s]") ;
          if(Math.abs(dynamicError) < eps && dynamicError > 0){
            this.currentTrainedNet.trainOpts.errorThresh = state.error;
            console.log("cant train net");
          }    
        this.prevError = state.error;
        this.trainTime = performance.now();
      },// null, // a periodic call back that can be triggered while training --> null or function
      callbackPeriod: 1, // the number of iterations through the training data between callback calls --> number greater than 0
      timeout: 1000*60*30,//Infinity, // the max number of milliseconds to train for --> number greater than 0
    }
    configFinal = {
      praxis: 'adam',
      activation: 'tanh', // activation function   | 'sigmoid' | 'relu' | 'leaky-relu' | 'tanh';
      //activation: 'sigmoid',
      hiddenLayers: [],//[50,5,50],
      iterations: 15, // the maximum times to iterate the training data --> number greater than 0
      errorThresh: 0.00515, // the acceptable error percentage from training data --> number between 0 and 1
      log: false, // true to use console.log, when a function is supplied it is used --> Either true or a function
      logPeriod: 1000, // iterations between logging out --> number greater than 0
      learningRate: 0.0003/2, // scales with delta to effect training rate --> number between 0 and 1
      momentum: 0.0001/2, // scales with next layer's change value --> number between 0 and 1
      callback: (state)=>{
        //if(this.useCrossValidateLayer)return;
        const eps = 0.0001//this.configFinal.errorThresh/50//0.0001;

        let elapsed = performance.now() - this.trainTime;
        let dynamicError = this.prevError - state.error;

        let timeToFinish = (state.error - this.configFinal.errorThresh )/dynamicError* elapsed /1000;

        console.log(state.iterations +
          "\terr: " + JSON.stringify(floorArray([state.error])) + 
          "\tderr: "+JSON.stringify(floorArray([dynamicError],6)) +
          "\tt: "+JSON.stringify(floorArray([elapsed])) +
          "\tendt: " + JSON.stringify(floorArray([timeToFinish])) + " [s]") ;


        if(0){
          if(dynamicError < -eps*1)
          this.searchDirection = !this.searchDirection;
          if(/* dynamicError>0 &&  */this.searchDirection == true){
            if(Math.abs(dynamicError) < eps){
              this.finalNet.learningRate+=this.configFinal.learningRate/2;
              this.finalNet.momentum+=this.configFinal.momentum/2;
              this.finalNet.trainOpts.learningRate = this.finalNet.learningRate;
              this.finalNet.trainOpts.momentum = this.finalNet.momentum;
              console.log("new state+\t" + floorArray([this.finalNet.learningRate]) +"\t"+floorArray([this.finalNet.momentum]));
            }
          }
          else{
            if(Math.abs(dynamicError) < eps){
              this.finalNet.learningRate-=this.configFinal.learningRate/2;
              this.finalNet.momentum-=this.configFinal.momentum/2;
              this.finalNet.trainOpts.learningRate = this.finalNet.learningRate;
              this.finalNet.trainOpts.momentum = this.finalNet.momentum;
              console.log("new state-\t" + floorArray([this.finalNet.learningRate]) +"\t"+floorArray([this.finalNet.momentum]));
            }
            // this.searchDirection = false;
            // if(Math.abs(dynamicError)<0.001/10)
            //   this.searchDirection = true
            
          }
        }
        else{
          if(Math.abs(dynamicError) < eps && dynamicError > 0){
            this.finalNet.trainOpts.errorThresh = state.error;
            console.log("cant train net");
          }
        }

        this.prevError = state.error;
        this.trainTime = performance.now();
      },//null, // a periodic call back that can be triggered while training --> null or function
      callbackPeriod: 1, // the number of iterations through the training data between callback calls --> number greater than 0
      timeout: Infinity,//Infinity, // the max number of milliseconds to train for --> number greater than 0
    }
  }
  
  module.exports.LayerNetwork = LayerNetwork;