var MNN = require('./mnn.js');
var _sqlite3 = require('better-sqlite3');//('foobar.db', options);
var mnn_indicator = require('./mnn_indicator.js');
var math = require('mathjs')

console.time('createProcess');
for (arg of process.argv)
  console.log(`Child process: ${arg} `)

var processName = "123415rqr"
process.on('message', (message) => {

  console.log('Message to child: ', message.command)

  switch (message.command) {
    case 'init':
      let inputData = message.data
      train(inputData.startIndex, inputData.trainSize, inputData.name, inputData.rows)

      console.timeEnd('createProcess');
      process.exit()
      break;
    case 'exit':
      console.timeEnd('createProcess');
      process.exit()
      break;

    default:
      console.log(`Child process ${processName} uncknown command`)
      break;
  }

  //let inputData =JSON.parse(message)
  //train(inputData.startIndex, inputData.trainSize, inputData.name, inputData.rows)
}

)
//let inputData =JSON.parse(process.argv[2])



process.send('Ping parent')
//process.exit()

class PrecentViewer {
  constructor(max, min = 0) {
    this.max = max;
    this.min = min;
    this.onePrecent = (max - min) / 100
    this.lastPrecent = 0
  }
  update(value) {
    let precent = (value - this.min) / this.onePrecent;
    if (precent - this.lastPrecent > 1) {
      this.lastPrecent = precent;
      console.log("ready " + Math.floor(this.lastPrecent) + " %")
    }
  }
  lastPrecent
  onePrecent
  max
  min
}

function train(startIndex, trainSize, name, rows) {
  let candlesCollector = new mnn_indicator.CandleInputDataCreator;

  console.log("------------------");
  console.log(startIndex + "\t" + rows.length);
  console.log("------------------");
  let secondName = rows[startIndex].start;
  let mnn = new MNN.MyNeuralNetwork(name, secondName);
  //console.log(mnn.hello());
  console.log(mnn.name + "\t" + secondName);
  let chooseInterval = 0
  let p = new PrecentViewer(trainSize/* row.length / 2 */);
    
  let intervalStep = 0//rows.length / trainSize;    
  for (let index = startIndex; index < rows.length - candlesCollector.interval - 1 && mnn.data.length < trainSize; index++) {
    p.update(mnn.data.length);
    let element = rows[index];
    if (candlesCollector.pushNewTestData(element)) {
      chooseInterval++;
      if (chooseInterval > intervalStep) {
        let data = candlesCollector.getComplyteArray();
        let testPrices = new Array;
        for (let index2 = index + 1; index2 <= index + candlesCollector.interval; index2++) {
          testPrices.push(rows[index2].close);
        }
        
        let out = candlesCollector.getOutParams(candlesCollector.data.close/* element.close */, testPrices);
        //let out = candlesCollector.getOutParams(MNN.median(candlesCollector.data.close), testPrices);
        if(index-startIndex > candlesCollector.windowSize)
        //mnn.data.push({ id: element.start, input: [out.sell, out.buy, out.hold, math.random(0,1), math.random(0,1), math.random(0,1)], output: out });
        mnn.data.push({ id: element.start, input: data, output: out });
        chooseInterval = 0;
      }
    }
  }

  let data_sell = mnn.data.filter(data => data.output.sell > 0)
  let data_hold = mnn.data.filter(data => data.output.hold > 0)
  let data_buy = mnn.data.filter(data => data.output.buy > 0)

  let minDataSize = Math.min(data_sell.length, data_hold.length, data_buy.length);
  if (minDataSize > 10) {
    console.log("train data: " + minDataSize + "\tfrom " + [data_sell.length, data_hold.length, data_buy.length])
    mnn.data = []
    for (let shuffleIndex = 0; shuffleIndex < minDataSize; shuffleIndex++) {
      mnn.data.push(data_sell[shuffleIndex]);
      mnn.data.push(data_hold[shuffleIndex]);
      mnn.data.push(data_buy[shuffleIndex]);
    }
    if (1)
      mnn.train();

    //mnn.selfTest();
    //mnn.writer();
  } else {
    console.log("NOT ENOUTH train data: " + minDataSize + "\tfrom " + [data_sell.length, data_hold.length, data_buy])
  }
  return 777;
}
