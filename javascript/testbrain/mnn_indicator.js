
var MNN = require("./mnn.js");
var _sqlite3 = require('better-sqlite3');//('foobar.db', options);

var math = require('mathjs')
/* const config = {
  epsilon: 1e-7,//1e-12,
  matrix: 'Matrix',
  number: 'number',
  precision: 64/8,
  predictable: false,
  randomSeed: null
}
const math = math_.create(math_.all, config) */


/* var SVD = require('svd-js').SVD;
var regression = require('regression'); */

var getOutParams = require('./my_indicators.js').getOutParams;
var getSSA = require('./my_indicators.js').getSSA;
var MySSAResultCollector = require('./my_indicators.js').MySSAResultCollector;
var MyLinearResultCollector = require('./my_indicators.js').MyLinearResultCollector;
var MyEMA = require('./my_indicators.js').MyEMA;
var MyMACAD = require('./my_indicators.js').MyMACAD;
var MyKDJ = require('./my_indicators.js').MyKDJ;
var MyTSI = require('./my_indicators.js').MyTSI;
var MyMACADResultCollector = require('./my_indicators.js').MyMACADResultCollector;
var MyKDJResultCollector = require('./my_indicators.js').MyKDJResultCollector;
var MyKDJ_MACDResultCollector = require('./my_indicators.js').MyKDJ_MACDResultCollector;
var MySimpleKDJ_MACDResultCollector = require('./my_indicators.js').MySimpleKDJ_MACDResultCollector;




class FilterCollector {
  constructor(window, interval) {
    this.window = window
    this.interval = interval

    this.filters = [
      //new MyMACAD(Math.floor(this.window * 1), Math.floor(this.window * 0.9)),
      //new MyMACAD(Math.floor(this.window * 1), Math.floor(this.window * 0.8)),
      new MyMACAD(Math.floor(this.window * 1), Math.floor(this.window * 0.6)),
      //new MyMACAD(Math.floor(this.window * 1), Math.floor(this.window * 0.4)),
      //new MyMACAD(Math.floor(this.window * 1), Math.floor(this.window * 0.2)),
      //new MyMACAD(Math.floor(this.window * 1), Math.floor(this.window * 0.1))
    ];
    this.momentumFilters = [
      //new MyTSI(Math.floor(this.window * 1), Math.floor(this.window * 0.9)),
      //new MyTSI(Math.floor(this.window * 1), Math.floor(this.window * 0.8)),
      new MyTSI(Math.floor(this.window * 1), Math.floor(this.window * 0.6)),
      //new MyTSI(Math.floor(this.window * 1), Math.floor(this.window * 0.4)),
      //new MyTSI(Math.floor(this.window * 1), Math.floor(this.window * 0.2)),
      //new MyTSI(Math.floor(this.window * 1), Math.floor(this.window * 0.1))
    ]
  }
  update(value) {
    this.filterResult = new Array;//.concat();
    for (let filter of this.filters) {
      this.filterResult = this.filterResult.concat(filter.update(value));
    }
    //this.momentum = 0;
    if (this.prevValue != undefined)
      this.momentum = value - this.prevValue;

    if (1)
      for (let filter of this.momentumFilters) {
        if (this.prevValue != undefined)
          this.filterResult = this.filterResult.concat(filter.update(this.momentum));
        else
          this.filterResult = this.filterResult.concat(1);
      }



    this.prevValue = value;


    return this.filterResult;
  }


  window
  interval
  filterResult
  prevFilterResult
  prevValue = undefined
  momentum
}

let getFilterArr = (arr) => {
  let result = new Array(arr[0].length)
  result.fill([])
  arr.forEach(elements => {
    for (let key in elements) {
      result[key] = [].concat(result[key], [elements[key]])
    }
  })
  return result
}

fibonacci = (n) => {
  let value = 5
  let prevValue = 0
  let summ = 0
  let arr = new Array
  for (let index = 0; index < n; index++) {
    summ += value
    arr.push(value)
    let prval = prevValue
    prevValue = value
    value += 0// prval/6

  }
  arr.reverse();
  return { size: summ, counts: arr }
}

// ------------------------------------------
// === class to create input data to nets ===
// ------------------------------------------
class CandleInputDataCreator {
  constructor() {
    this.useNormalData = true;


    if (this.useNormalData) {
      this.complyteArraySize = 120//120//this.inputDataFormat.size
      this.windowSize = 26//120//this.complyteArraySize
      this.interval = Math.floor(this.complyteArraySize / 4)//60//this.windowSize/4
    } else {
      this.inputDataFormat = fibonacci(60)//{ size: summ, counts: arr }
      this.complyteArraySize = this.inputDataFormat.size
      this.windowSize = this.complyteArraySize / 1
      this.interval = Math.floor(this.complyteArraySize / 5)//60//this.windowSize/4
      console.log(JSON.stringify(this.inputDataFormat) + "\t" + this.interval)
    }
    this.createNewData();
  }

  pushNewTestData(element) {
    // element.close;//
    // element.high;//
    // element.low;//
    // element.start;
    // element.trades;//
    // element.volume;//
    // element.vwp;//

    this.pushToData(element);

    while (this.data.length > this.complyteArraySize)
      this.shiftFromData();

    if (/* this.data.length */this.windowElementCount < this.complyteArraySize) {
      return false;
    }
    else {
      this.windowElementCount--;
      return true;
    }
  }
  pushToData(element) {
    if (this.data.length > 0)
      if (element.start - this.data.start[this.data.start.length - 1] != 60)
        this.createNewData();
    if (element.trades == 0 || element.volume == 0) {
      this.createNewData();
      return;
    }
    this.data.candles.push(element);

    for (let collector of this.resultCollectors) {
      collector.updateCandles(this.data.candles);
    }

    this.data.close.push(element.close);
    this.data.delta.push(element.close - element.open);

    this.data.high.push(element.high);
    this.data.low.push(element.low);
    //this.data.tsi_close.update(element.close-element.open);
    //this.data.tsi_close2.update(element.close-element.open);
    //this.data.tsi_close3.update(element.close-element.open);

    this.filtersClose.update(element.close);
    this.filtersTrades.update(element.trades);
    this.filtersVolume.update(element.volume);
    this.filtersHigh.update(element.high);
    this.filtersLow.update(element.low);
    this.filtersVpw.update(element.vwp);
    this.data.filtersClose.push(this.filtersClose.filterResult);
    this.data.filtersTrades.push(this.filtersTrades.filterResult);
    this.data.filtersVolume.push(this.filtersVolume.filterResult);
    this.data.filtersHigh.push(this.filtersHigh.filterResult);
    this.data.filtersLow.push(this.filtersLow.filterResult);
    this.data.filtersVpw.push(this.filtersVpw.filterResult);

    this.data.start.push(element.start);
    this.data.trades.push(element.trades);
    this.data.volume.push(element.volume);
    this.data.vwp.push(element.vwp);
    this.data.length++;
    this.windowElementCount++;
  }
  shiftFromData() {
    if (this.data.length <= 0)
      return;
    this.data.candles.shift();
    this.data.close.shift();
    this.data.delta.shift();
    this.data.high.shift();
    this.data.low.shift();
    this.data.start.shift();
    this.data.trades.shift();
    this.data.volume.shift();
    this.data.vwp.shift();
    this.data.filtersClose.shift();
    this.data.filtersTrades.shift();
    this.data.filtersVolume.shift();
    this.data.length--;
  }

  putResultCollectorsToArray() {
    for (let resultCollector of this.resultCollectors) {
      this.complyteArray = this.complyteArray.concat(resultCollector.get())
    }
  }

  getComplyteArray() {
    this.complyteArray = new Array;
    //this.data.forEach(element => {
    let element = this.data;
    //this.inputDataFormat

    let getSplicedArray = (array) => {
      let arr = array.slice()
      let splicedArray = []
      for (let count of this.inputDataFormat.counts) {
        //for (let arr of array) {
        if (arr.length == 0)
          break
        let filtredArray = arr.splice(0, count)//  MNN.mean(arr.splice(0, count));
        splicedArray = splicedArray.concat([filtredArray[filtredArray.length - 1]]);
        //}
      }
      return splicedArray
    }
    let putFilterToArray = (result, array) => {
      for (let arr of getFilterArr(array)) {
        result.push(/*  MNN.normArray */ getSvdFilter(getSplicedArray(arr)));
      }
    }


    if (this.useNormalData) {
      /*       this.complyteArray = [].concat(
              //MNN.normArray(element.close),
      
            ); */
      this.complyteArray = [];
      this.putResultCollectorsToArray()

/*       let result = []
      //result = [].concat(result,element.filtersTrades);
      result.push(element.filtersTrades[element.filtersTrades.length-1])
      for (let arr of result) {
        this.complyteArray = this.complyteArray.concat((arr));
      } */
    }
    else {
      let result = [
        //MNN.normArray(element.close),
        //MNN.normArray(element.trades),
        //MNN.normArray(element.volume),


        //MNN.normArray(element.delta),

        //MNN.sigmoidArray(element.close,MNN.median(element.close)),
        //MNN.sigmoidArray(element.trades,MNN.median(element.trades)),

        //MNN.normArray(element.vwp),
        //MNN.normArray(element.high),
        //MNN.normArray(element.low),
      ];




      putFilterToArray(result, element.filtersClose);
      putFilterToArray(result, element.filtersTrades);
      putFilterToArray(result, element.filtersVolume);
      //putFilterToArray(result,element.filtersVpw);
      //putFilterToArray(result,element.filtersHigh);
      //putFilterToArray(result,element.filtersLow);

      for (let arr of result) {
        this.complyteArray = this.complyteArray.concat(getSplicedArray(arr));
      }
      //this.complyteArray = this.complyteArray.concat( getSvdFilter(getSplicedArray(element.close),this.inputDataFormat.counts.length/2 ) )
      //this.complyteArray = this.complyteArray.concat( getSvdFilter(getSplicedArray(element.volume),this.inputDataFormat.counts.length/2 ) )
      //this.complyteArray = this.complyteArray.concat( getSvdFilter(getSplicedArray(element.trades),this.inputDataFormat.counts.length/2 ) )
      this.complyteArray = this.complyteArray.concat(getSvdFilter((element.close) /* ,element.close.length/2  */))
      //this.complyteArray = this.complyteArray.concat( getSvdFilter((element.volume)) )
      //this.complyteArray = this.complyteArray.concat( getSvdFilter((element.close),4) )
      //this.complyteArray = this.complyteArray.concat( getSvdFilter((element.close),5) )
      //this.complyteArray = this.complyteArray.concat( getSvdFilter((element.trades)) );
    }


    return this.complyteArray;
  }
  createNewData() {
    this.resultCollectors = new Array
    this.resultCollectors.push(new MyKDJ_MACDResultCollector)
    this.resultCollectors.push(new MyKDJResultCollector)
    this.resultCollectors.push(new MyMACADResultCollector)
    this.resultCollectors.push(new MySimpleKDJ_MACDResultCollector)
    this.resultCollectors.push(new MyLinearResultCollector(this.complyteArraySize,this.interval))
    this.resultCollectors.push(new MyLinearResultCollector(this.complyteArraySize/2,this.interval))
    this.resultCollectors.push(new MyLinearResultCollector(this.complyteArraySize/3,this.interval))
    //this.resultCollectors.push(new MySSAResultCollector(this.complyteArraySize,this.interval))
    


    this.data = {
      candles: new Array,
      close: new Array,
      delta: new Array,
      high: new Array,
      low: new Array,
      //tsi_close: new MyTSI(this.windowSize,this.interval),//(25,13),
      //tsi_close2: new MyTSI(Math.floor(this.windowSize/2),Math.floor(this.interval/2)),//(25,13),
      //tsi_close3: new MyTSI(),//(25,13),
      start: new Array,
      trades: new Array,
      volume: new Array,
      vwp: new Array,
      filtersClose: new Array,
      filtersTrades: new Array,
      filtersVolume: new Array,
      filtersHigh: new Array,
      filtersLow: new Array,
      filtersVpw: new Array,
      length: 0
    }
    this.filtersClose = new FilterCollector(this.windowSize, this.interval),
      this.filtersTrades = new FilterCollector(this.windowSize, this.interval),
      this.filtersVolume = new FilterCollector(this.windowSize, this.interval),
      this.filtersHigh = new FilterCollector(this.windowSize, this.interval),
      this.filtersLow = new FilterCollector(this.windowSize, this.interval),
      this.filtersVpw = new FilterCollector(this.windowSize, this.interval),
      this.windowElementCount = 0;
  }

  getOutParams(currentPrice, nextPrices) {
    return getOutParams(currentPrice, nextPrices)
  }

  getOutParamError(realOut, netOut) {
    let error = 0;
    if (realOut.sell > realOut.buy && realOut.sell > realOut.hold ) {
      if (netOut.buy >= netOut.sell)
        error = 1;
      else
        if (netOut.hold >= netOut.sell)
          error = 1;
        else
          /*       if(netOut.sell<0)  
                  error = 1/2;
                else */
          error = realOut.sell - netOut.sell;
    }
    else
      if (realOut.buy > realOut.sell && realOut.buy > realOut.hold) {
        if (netOut.sell >= netOut.buy)
          error = 1;
        else
          if (netOut.hold >= netOut.buy)
            error = 1;
          else
            /*       if(netOut.buy < 0)  
                    error = 1/2;
                  else */
            error = realOut.buy - netOut.buy;
      }
    if (realOut.hold > 0) {
      if (netOut.sell >= netOut.hold)
        error = 1;
      else
        if (netOut.buy >= netOut.hold)
          error = 1;
        else
          /*       if(netOut.hold <0)  
                  error = 1/2;
                else */
          error = realOut.hold - netOut.hold;
    }


    return error
  }

  interval
  windowSize
  complyteArraySize
  windowElementCount
  data
  complyteArray// = new Array

};

// ----------------------------------
// === class to check nets result ===
// ----------------------------------
class MnnChecker {

  setNets(nets) {
    nets.forEach(element => {
      this.nmm.push({
        net: element,
        netResults: new Array,
        error: 0,
        totalChoosed: 0,
        dynamicError: new MyEMA(this.candleInputDataCreator.windowSize * 2),
        totalGoodChoices: { sell: 0, buy: 0, hold: 0, total: 0 },
        totalBadChoices: { sell: 0, buy: 0, hold: 0, total: 0 }
      });

    });
    this.actualNet = this.nmm[0];
    this.testInterval = this.candleInputDataCreator.interval
  }
  update(price, input) {
    this.nextPrices.push(price);
    this.netHistory.push({ 
      price: price, 
      actualNet: this.actualNet ,
      logPrices: this.nextPrices.slice()
      /* , inputData:this.candleInputDataCreator.data */ 
    });

    for (let element of this.nmm) {

      element.netResults.push(element.net.run(input));
    }


    while (this.nextPrices.length > this.testInterval) {
      this.nextPrices.shift();
      this.netHistory.shift();
      for (let element of this.nmm) {
        element.netResults.shift();
      };
    }
    if (this.nextPrices.length < this.testInterval)
      return;

    let lastRealResult = this.candleInputDataCreator.getOutParams(this.netHistory[0].logPrices/* this.nextPrices[0] */, this.nextPrices);
    //let lastRealResult = this.candleInputDataCreator.getOutParams(MNN.median(this.netHistory[0].inputData.close), this.nextPrices);

    for (let element of this.nmm) {
      element.error = this.candleInputDataCreator.getOutParamError(lastRealResult, element.netResults[0]);
      element.dynamicError.update(Math.abs(element.error));
      //element.netResults.push(element.net.run(input));


      if (Math.abs(element.error) < 1) {
        if (lastRealResult.sell > 0)
          element.totalGoodChoices.sell += 1
        if (lastRealResult.buy > 0)
          element.totalGoodChoices.buy += 1
        if (lastRealResult.hold > 0)
          element.totalGoodChoices.hold += 1
        element.totalGoodChoices.total++
      }
      else {
        if (lastRealResult.sell > 0)
          element.totalBadChoices.sell += 1
        if (lastRealResult.buy > 0)
          element.totalBadChoices.buy += 1
        if (lastRealResult.hold > 0)
          element.totalBadChoices.hold += 1
        element.totalBadChoices.total++
      }
    }
    this.nmm.sort(function (a, b) {
      //return Math.abs(a.error) - Math.abs(b.error);
      return (a.dynamicError.get()) - (b.dynamicError.get());
    });
    for (let nnmIndex = 0; nnmIndex < this.nmm.length; nnmIndex++) {
      this.actualNet = this.nmm[nnmIndex];

      if (/* Math.abs(this.actualNet.error) > 0.999  || */ Math.abs(this.actualNet.dynamicError.get()) > 10.3999) {
        //console.log("best net not found" +"\terr: "+MNN.floorArray([this.actualNet.error])+"\terrEMA: "+MNN.floorArray([this.actualNet.dynamicError.get()]) +"\t\t");
        this.actualNet = undefined;
        //return;
      } else { break }
    }
    if (this.actualNet != undefined) {
      this.isReady = true;
      //this.actualNet  =  this.nmm[0];
      if (this.useLog)
        console.log("best net " + this.actualNet.net.additionalName + "\terr: " + JSON.stringify(MNN.floorArray([this.actualNet.error])) + "\terrEMA: " + JSON.stringify(MNN.floorArray([this.actualNet.dynamicError.get()])))
      this.actualNet.totalChoosed++;
      this.totalDataWithNet++
    }
    else {
      this.totalDataWithoutNet++
      if (this.useLog)
        console.log("NOT HAVE best net ")
    }
    this.netHistory[this.netHistory.length - 1].actualNet = this.actualNet;
    let choosedNmm = this.nmm.filter(element => element == this.netHistory[0].actualNet);

    if (choosedNmm === undefined) {
      // choosedNmm = {net:{additionalName:"FUCK"},error: NaN};
    }
    else
      choosedNmm = choosedNmm[0];
    if (choosedNmm != undefined) {
      if (Math.abs(choosedNmm.error) < 1) {
        if (lastRealResult.sell > 0)
          this.totalGoodChoices.sell += 1
        if (lastRealResult.buy > 0)
          this.totalGoodChoices.buy += 1
        if (lastRealResult.hold > 0)
          this.totalGoodChoices.hold += 1
        this.totalGoodChoices.total++
      }
      else {
        if (lastRealResult.sell > 0)
          this.totalBadChoices.sell += 1
        if (lastRealResult.buy > 0)
          this.totalBadChoices.buy += 1
        if (lastRealResult.hold > 0)
          this.totalBadChoices.hold += 1
        this.totalBadChoices.total++
      }
      if (this.useLog)
        console.log("\t\t\tchoosed net " + choosedNmm.net.additionalName + "\terr: " + JSON.stringify(MNN.floorArray([choosedNmm.error])) + "\terrEMA: " + JSON.stringify(MNN.floorArray([choosedNmm.dynamicError.get()])));
    }
  }
  clear() {
    this.isReady = false;
    this.nextPrices = new Array;
    for (let element of this.nmm) {
      element.netResults = new Array;
      element.error = 0;
    };
  }
  getResult() {
    if (this.actualNet == undefined)
      return undefined;
    return this.actualNet.netResults[this.actualNet.netResults.length - 1];
  }
  testInterval
  totalDataWithNet = 0;
  totalDataWithoutNet = 0;
  totalGoodChoices = { sell: 0, buy: 0, hold: 0, total: 0 };
  totalBadChoices = { sell: 0, buy: 0, hold: 0, total: 0 };
  nextPrices = new Array;
  netHistory = new Array;
  isReady = false
  actualNet;
  nmm = new Array;
  //windowSize
  candleInputDataCreator = new CandleInputDataCreator;
  useLog = false
}

// ------------------------------------
// === class to calculate indicator ===
// ------------------------------------
class MnnIndicator {
  constructor(name, netDbName = "MyNeuralNetwork_dbNets.sqlite") {
    let dbNetData = new _sqlite3(netDbName);
    let netTable = dbNetData.prepare(`SELECT additionalName, result FROM ${name}`).all();
    dbNetData.close();
    dbNetData = null

    netTable.sort(function (a, b) {
      return JSON.parse(a.result).error - JSON.parse(b.result).error;
    });

    for (let index = 0; index < netTable.length && index < 33; index += 1) {
      let element = netTable[index];

      //}
      //netTable.forEach(element => {
      let secondName = element.additionalName;
      //if(secondName != "1583881800")continue
      this.nets.push(new MNN.MyNeuralNetwork(name, secondName));
      let mnn = this.nets[this.nets.length - 1];
      mnn.loadNet();
      //let netErrors = new Array;
      console.log(mnn.hello() + "\t" + secondName + "\t" + element.result);
    }//);
    this.mnnChecker.setNets(this.nets);
    //netTable = null;
  }

  update(values) {
    this.isReady = false
    //console.log("up: "+JSON.stringify(values));
    if (this.mnnChecker.candleInputDataCreator.pushNewTestData(values)) {
      //let data = candlesCollector.getComplyteArray();
      //let netOut = mnn.run(candlesCollector.getComplyteArray());
      if (this.useLog)
        console.log("================= test data " + values.start + " =================");
      this.mnnChecker.update(values.close, this.mnnChecker.candleInputDataCreator.getComplyteArray());
      //console.log("====================================================================")
      this.isReady = true;
    }
    else {
      if (this.useLog)
        console.log("clear");
      this.mnnChecker.clear();
    }
    return this.isReady;
  }
  getResult() {
    return this.mnnChecker.getResult();
  }

  //candleInputDataCreator = new CandleInputDataCreator;
  isReady = false
  nets = new Array;
  mnnChecker = new MnnChecker
  useLog = false
}


module.exports.CandleInputDataCreator = CandleInputDataCreator;
module.exports.MnnIndicator = MnnIndicator;