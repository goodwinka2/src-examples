
var SVD = require('svd-js').SVD;
var regression = require('regression');
var math = require('mathjs')

function sigmoid(t) {
  return 2 / (1 + Math.pow(Math.E, -t)) - 1;
}


function getOutParams(currentPrice, nextPrices) {
  if (0) {
    let res = { sell: 0, buy: 0, hold: 0 }
    let precent = 0.06
    let k = 0.4//precent*nextPrices[0]/nextPrices.length/100
    // a+p*a/100 = a*(1+p/100)  | -a=>  fa=a*(p/100) p=fa*100/a
    // t*k+a                    | -a=>  fa=t*k
    // p=t*k*100/a || k=p*a/t/100

    if (currentPrice.length < 5) {
      res.hold = 1
      return res
    }

    let result_currentPrice = regression.linear(currentPrice.map(function (x, i) { return [i, x]; }));
    let result_nextPrices = regression.linear(nextPrices.map(function (x, i) { return [i, x]; }));
    let startPrice = result_currentPrice.predict(currentPrice.length)[1]
    let finalPrice = result_nextPrices.predict(nextPrices.length)[1]
    if (finalPrice == NaN || startPrice == NaN)
      console.log("ERR")

    let deltaK = result_nextPrices.equation[0] - result_currentPrice.equation[0]
    //let result = regression.logarithmic(nextPrices.map(function(x,i) {   return [i+1,x]; }));
    if (math.abs(deltaK) < k)
      res.hold = 1
    else
      if (deltaK > 0)
        res.buy = 1
      else
        if (deltaK < 0)
          res.sell = 1
        else {
          res.hold = 1
          //res.sell = 1/2
          //res.buy = 1/2
        }
    /*       let res2 = new Array(3)
          res2[0]=res.sell;
          res2[1]=res.hold;
          res2[2]=res.bye; */
    return res//res;
  }

  if (1) {
    let res = { sell: 0, buy: 0, hold: 0 }
    let price = currentPrice[currentPrice.length - 1]
    let precent = 0.001
    let max = price * (1 + precent)
    let min = price * (1 - precent)
    /* 
        res.sell = nextPrices.reduce((accumulator, currentValue) => {if(currentValue<min)accumulator+=1;return accumulator},0.0)
        res.sell/= nextPrices.length
        res.buy = nextPrices.reduce((accumulator, currentValue) => {if(currentValue>max)accumulator+=1;return accumulator},0.0)
        res.buy/= nextPrices.length
        res.hold = nextPrices.reduce((accumulator, currentValue) => {if(currentValue<min)accumulator+=1;return accumulator},0.0) */

    res.sell = nextPrices.reduce((accumulator, currentValue) => { if (currentValue < min) accumulator += 1; return accumulator }, 0.0)
    res.sell /= nextPrices.length
    res.buy = nextPrices.reduce((accumulator, currentValue) => { if (currentValue > max) accumulator += 1; return accumulator }, 0.0)
    res.buy /= nextPrices.length
    if (res.buy > 0)
      res = { sell: 0, buy: 1, hold: 0 }
    else
      res = { sell: res.sell, buy: 0, hold: 1 - res.sell }
    return res//res;
  }

  if (0) {
    let res = { sell: 0, buy: 0, hold: 0 }
    let precent = 0.001
    let max = currentPrice * (1 + precent)//math.max(nextPrices)/currentPrice-1
    let min = currentPrice * (1 - precent)//math.min(nextPrices)/currentPrice-1
    //let median = math.median(nextPrices) / (currentPrice) - 1
    //let mean = math.median(nextPrices)/ (currentPrice) - 1
    //let precent = math.median(nextPrices) / (currentPrice) - 1
    let result = regression.linear([].concat([currentPrice], nextPrices).map(function (x, i) { return [i, x]; }));
    let finalPrice = result.predict(nextPrices.length)[1]
    if (finalPrice == NaN)
      console.log("ERR")
    //let result = regression.logarithmic(nextPrices.map(function(x,i) {   return [i+1,x]; }));
    if (max >= finalPrice && min <= finalPrice)
      res.hold = 1
    else
      if (max < finalPrice)
        res.buy = 1
      else
        if (min > finalPrice)
          res.sell = 1
        else {
          res.hold = 1
          //res.sell = 1/2
          //res.buy = 1/2
        }
    /*       let res2 = new Array(3)
          res2[0]=res.sell;
          res2[1]=res.hold;
          res2[2]=res.bye; */
    return res//res;
  }



  if (0) {
    let res = { sell: 0, buy: 0, hold: 0 }
    let precent = math.median(nextPrices) / (currentPrice) - 1
    if (Math.abs(precent) < 0.002) {
      res.hold = 1
      //res.sell = 1/2
      //res.buy = 1/2
    }
    else
      if (precent > 0)
        res.buy = 1
      else
        res.sell = 1
    /*       let res2 = new Array(3)
          res2[0]=res.sell;
          res2[1]=res.hold;
          res2[2]=res.bye; */
    return res//res;
  }
  //return [percentForUp];
  //return [midUp];
}


function getSSA(x, h, interpolationSize) {
  //console.time("getSSA")
  let x2 = x.slice()
  let n = x.length
  let n2 = n + interpolationSize
  let m = math.ceil(n * h)
  let hankel = []
  for (let index = 0; index <= n - m; index++) {
    hankel.push(x2.slice(index, index + m))
  }
  let X = math.multiply(math.transpose(hankel), hankel)
  //console.timeEnd("getSSA")
  //console.time("getSSA")
  let s = SVD(X)
  s.u = math.transpose(s.u)
  let Vtr = []
  let v = []
  s.q = s.q.map((x, i) => { return { index: i, data: x } })
  s.q.sort((a, b) => { return b.data - a.data })
  for (let i = 0; i < s.q.length / 2; i++) {
    //if(s.q[i].data < s.q[0].data/10)
    //  break
    if (s.q[i].data > 0) {
      Vtr.push(s.u[s.q[i].index])
      v.push(Vtr[Vtr.length - 1].pop())
    }

  }
  for (let index = n; index < n2; index++) {

    let Q = x2.slice(1 - m)
    let V = math.transpose(Vtr)
    V = math.multiply(math.inv(math.multiply(Vtr, V)), Vtr)
    x2.push(math.multiply(math.multiply(v, V), Q))
    //hankel.push(x2.slice(index-m+1,index+1))
  }
  //console.timeEnd("getSSA")
  return x2.slice(-interpolationSize)
}

function getHystogram(candles, fixedstep = 0.1, count = 10) {
  //console.time("getSSA")
  //let x2 = x.slice()
  let closes = candles.map(function (x, i) { return x.close; })
  let min = math.min(closes)
  let max = math.max(closes)
  let current = candles[candles.length - 1].close
  if (0) {
    var step = (max - min) / count
  } else {
    var step = fixedstep / 100 * current
    count = (max - min) / step
    count = math.ceil(count)
    if (count < 2) {
      count = 2
    }
  }
  let hystogram = new Array(count)

  for (let index = 0; index < hystogram.length; index++) {
    hystogram[index] = {
      min: min + index * step,
      max: min + (index + 1) * step,
      mid: min + (index + 0.5) * step,
      data: 0,
      count: 0,
      maxTrade: 0,
      presentFromCurrent: 0,
      isLocalMaximum: 0,
      isCurrent: 0
    }
    if (hystogram[index].min <= current && hystogram[index].max >= current)
      hystogram[index].isCurrent = 1
    hystogram[index].presentFromCurrent = 100 - 100 * current / hystogram[index].mid
  }

  for (let index = 0; index < candles.length; index++) {
    let candle = candles[index]
    for (let hyst of hystogram) {
      if (hyst.min <= candle.close && hyst.max >= candle.close) {
        hyst.data += candle.trades
        hyst.count++
        if (hyst.maxTrade < candle.trades)
          hyst.maxTrade = candle.trades
      }


    }
  }

  /*   for (let index = 1; index < hystogram.length - 1; index++) {
      hystogram[index].data /= hystogram[index].count
      
    } */

  for (let index = 1; index < hystogram.length - 1; index++) {
    if (hystogram[index].data > hystogram[index - 1].data && hystogram[index].data > hystogram[index + 1].data) {
      hystogram[index].isLocalMaximum = 1
    }

  }
  if (hystogram[0].data > hystogram[1].data)
    hystogram[0].isLocalMaximum = 1
  if (hystogram[hystogram.length - 1].data > hystogram[hystogram.length - 2].data)
    hystogram[hystogram.length - 1].isLocalMaximum = 1

  for (let index = 1; index < hystogram.length - 1; index++) {
    if (hystogram[index].data < hystogram[index - 1].data && hystogram[index].data < hystogram[index + 1].data) {
      hystogram[index].isLocalMaximum = -1
    }
  }
  if (hystogram[0].data < hystogram[1].data)
    hystogram[0].isLocalMaximum = -1
  if (hystogram[hystogram.length - 1].data < hystogram[hystogram.length - 2].data)
    hystogram[hystogram.length - 1].isLocalMaximum = -1

  return hystogram
}


class StepCounter {
  constructor() {
    this.clear()
  }
  clear() {
    this.count = 0
  }
  update(max) {
    this.count++
    if (this.count >= max) {
      this.clear()
      return true;
    }
    else
      return false
  }
}

class MySSAResultCollector {
  constructor(window, interval) {
    this.window = window
    this.interval = interval
    this.prevValue = undefined
    this.history = []//{value:[],candles:[]}
    this.count = 0
    this.prevPeriod = 0
    this.counter = new StepCounter
  }

  updateCandles(candles) {
    this.filterResult = { sell: 0, buy: 0, hold: 0 }

    this.filterResult = []
    for (let h = 0.05; h <= 0.96; h += 10.9) {
      if (candles.length < this.window) {
        this.filterResult.push(0)
        this.filterResult.push(0)
        this.filterResult.push(1)
        continue
      }
      let currentPrices = candles.slice(-this.window).map(function (x, i) { return x.close; })
      let result = getSSA(currentPrices,
        h, this.interval);
      result.splice(0, 1)
      let start = result.splice(0, 1)[0]//candles[candles.length-1].close
      let filterResult = getOutParams(currentPrices/* start */, result.slice(1))
      this.filterResult.push(filterResult.sell)
      this.filterResult.push(filterResult.buy)
      this.filterResult.push(filterResult.hold)
    }


    this.count++
    //this.prevValue = value;
    return this.filterResult;
  }
  get() {
    return this.filterResult;
  }
}

class MyLinearResultCollector {
  constructor(window, interval) {
    this.window = window
    this.interval = interval
    this.prevValue = undefined
    this.history = []//{value:[],candles:[]}
    this.count = 0
    this.prevPeriod = 0
    this.counter = new StepCounter
  }

  updateCandles(candles) {
    this.filterResult = { sell: 0, buy: 0, hold: 0 }


    //if(this.counter.update(5))
    let currentPrices = candles.map(function (x, i) { return x.close; })
    let result = regression.linear(candles.slice(-this.window).map(function (x, i) { return [i, x.close]; }));
    let value = result.equation[0]

    //let finalPrice = result.predict(nextPrices.length)[1]
    this.filterResult = getOutParams(currentPrices/* candles[candles.length-1].close */,
      Array.from({ length: this.interval }, (_, i) => i).
        map(function (x, i) { return result.predict(i)[1]; }))

    this.history.push({
      value: value,
      candles: candles[candles.length - 1].close,
      sell: this.filterResult.sell,
      buy: this.filterResult.buy,
      hold: this.filterResult.hold
    })
    while (this.history.length > candles.length) {
      this.history.shift();
    }

    this.count++
    this.prevValue = value;
    return [this.filterResult.sell, this.filterResult.buy, this.filterResult.hold];
  }
  get() {
    return [this.filterResult.sell, this.filterResult.buy, this.filterResult.hold];
  }
}


class MyEMA {
  constructor(N = 25) {
    this.k = 2 / (N + 1);
    this.N = N;
  }
  update(value) {
    if (this.EMA === undefined)
      this.EMA = value
    this.EMA = this.k * value + (1 - this.k) * this.EMA;
    return this.EMA;
  }
  get() {
    return this.EMA;
  }
  k
  N
  EMA = undefined
}

class MyMACAD {
  constructor(N = 26, M = 12, K = 9) {
    this.N = N;
    if (M === undefined)
      this.M = Math.floor(N / 3);
    else
      this.M = M;
    this.TSI = 0;
    this.ema0 = new MyEMA(this.N)
    this.ema1 = new MyEMA(this.M)
    this.ema2 = new MyEMA(Math.floor(K))
  }
  update(value) {
    this.MyMACAD = this.ema1.update(value) - this.ema0.update(value);
    this.signal = this.ema2.update(this.MyMACAD);
    /*     if (Math.abs(value) > 0.000001)
          this.gyst = math.sigmoid(this.MyMACAD - this.signal) * 2 - 1//(Math.abs(this.MyMACAD)+Math.abs(this.signal)) //;/ value;
        else
          this.gyst = 0; */
    this.gyst = this.MyMACAD - this.signal
    return this.gyst;
  }
  get() {
    return this.gyst;
  }
  MyMACAD
  signal
  gyst
  N
  M
  ema0// = new MyEMA(this.N)
  ema1// = new MyEMA(this.N)
  ema2
}

class MyKDJ {
  constructor(N = 9, a = 3, b = 3) {
    this.N = N;
    this.RSV = 0
    this.K = 50
    this.D = 50
    this.J = 0
  }
  updateArray(candles, close = undefined) {
    let L = math.min(candles.map(function (candle) { return candle.low }))
    let H = math.max(candles.map(function (candle) { return candle.high }))
    if (close == undefined)
      close = candles[candles.length - 1].close
    //this.RSV = 100 * (candles[candles.length - 1].close - L) / (H - L)
    this.RSV = 100 * (close - L) / (H - L)
    this.K = 2 / 3 * this.K + 1 / 3 * this.RSV
    this.D = 2 / 3 * this.D + 1 / 3 * this.K
    this.J = 3 * this.K - 2 * this.D
    return this.J - this.D;
  }
  get() {
    return this.J - this.D;
  }
}

class MyTSI {
  constructor(N = 25, M = undefined) {
    this.N = N;
    if (M === undefined)
      this.M = Math.floor(N / 3);
    else
      this.M = M;
    this.TSI = 0;
    this.ema0 = new MyEMA(this.N)
    this.ema1 = new MyEMA(this.N)
    this.ema2 = new MyEMA(this.M)
    this.ema3 = new MyEMA(this.M)
  }
  update(momentum) {
    this.TSI = this.ema2.update(this.ema0.update(momentum)) / this.ema3.update(this.ema1.update(Math.abs(momentum)));
    return this.TSI;
  }
  get() {
    return this.TSI;
  }
  TSI
  N
  M
  ema0// = new MyEMA(this.N)
  ema1// = new MyEMA(this.N)
  ema2// = new MyEMA(this.M)
  ema3// = new MyEMA(this.M)
}

class MyMACADResultCollector {
  constructor(param = undefined) {
    this.param = param
    this.myMACAD = new MyMACAD(26 * 1, 12 * 1, 9 * 1)
    this.prevValue = undefined
    this.history = []//{value:[],candles:[]}
    this.count = 0
    this.prevPeriod = 0
    this.counter = new StepCounter
    this.price = 0
    this.buyValue = 0
    this.tax = 0.1
    if (param == undefined) {
      this.param.minValue = 0
      this.param.a0 = 1
    }

    if (param == undefined) {
    } else {

      //this.param.minValue = math.abs(param.minValue)
      //this.param.count = math.abs(param.count)
    }
    this.ema0 = new MyEMA(1)
  }

  //update(candle) {
  updateCandles(candles) {
    this.filterResult = { sell: 0, buy: 0, hold: 0 }
    //let value = this.myMACAD.update(candle.close)
    //let value = this.myMACAD.update(candles[candles.length - 1].close)

    let value = this.myMACAD.get()
    //if(this.counter.update(5))
    value = this.myMACAD.update(math.mean(candles.map((x, i) => { return x.close }).slice(-1)))
    value = this.ema0.update(value)
    this.history.push({
      value: value,
      candles: candles[candles.length - 1].close,
      sell: this.filterResult.sell,
      buy: this.filterResult.buy,
      hold: this.filterResult.hold,
      delta: 0
    })


    if (this.prevValue == undefined) {
      this.filterResult.hold = 1
      this.prevValue = value
    }
    else {


      if (0) {
        if (value < 0) {
          this.count++
          if (this.prevValue < value && value < -this.param.minValue && this.count > this.param.count/* && this.prevPeriod*3/4 < this.count */)
            this.filterResult.buy = 1
          else
            this.filterResult.hold = 1
        }
        else {
          this.count = 0
          if (this.prevValue > value /* && this.prevPeriod*3/4 < this.count */)
            this.filterResult.sell = 1
          else
            this.filterResult.hold = 1
        }
      }

      if (1) {
        if (candles.length > 60) {
          //let values = this.history.map((x, i) => { return x.value })
          //let out = getSSA(values, 0.95, values.length / 3)
          let hystogram = getHystogram(candles,this.tax)

          let currentIndex = 0
          for (let index = 0; index < hystogram.length; index++) {
            if (hystogram[index].isCurrent == 1 ) {
              currentIndex = index
              break
            }        
          }
          //let down
          if(hystogram[currentIndex].isLocalMaximum <= 0 
            && currentIndex > 0
            && currentIndex < hystogram.length-1){
            if (hystogram[currentIndex+1].data - hystogram[currentIndex-1].data >0) {
              let negativeIndex = currentIndex
              for (let index = currentIndex; index >= 0; index--) {
                if (hystogram[index].isLocalMaximum == 1 ) {
                  negativeIndex = index                    
                  break
                }        
              }
              for (let index = currentIndex; index < hystogram.length; index++) {
                if (hystogram[index].isLocalMaximum == 1 ) {
                  if(hystogram[index].presentFromCurrent < this.tax*1.5
                    || hystogram[index].presentFromCurrent > this.tax*3
                    //&& hystogram[index].data < hystogram[negativeIndex].data
                    )
                    this.filterResult.hold = 1
                  break
                }        
              }
              if(this.filterResult.hold == 0)
                this.filterResult.buy = 1
            }
            else{
              this.filterResult.hold = 1
            }


          }else{
            if(hystogram[currentIndex].isLocalMaximum == 1)
              this.filterResult.sell = 1
            else
              if(hystogram[currentIndex].isLocalMaximum == -1)
                this.filterResult.hold = 1
          }


          
          let rrr=0
        }
      }

      if (0) {
        if (candles.length > 60) {
          let values = this.history.map((x, i) => { return x.value })
          let out = getSSA(values, 0.95, values.length / 3)

          if (this.prevInterpolation != undefined) {

            var isUp = (next) => {
              let threshold = 0
              let result = false
              let prevValue = next[0]
              for (let val of next) {
                if (val > prevValue && val > threshold) {
                  result = true
                  break
                }
                if (val < prevValue)
                  break
                prevValue = val
              }
              return result
            }
            let isPositive = isUp(out)

            if (0) {

              if (this.prevValue >= value && isPositive && value < 0) {
                this.filterResult.buy = 1
              }
              else if (!isPositive)
                this.filterResult.sell = 1
              else
                this.filterResult.hold = 1

            } else {
              if (this.prevValue < value && isPositive && value < 0) {
                this.count++
                if (this.count > 5)
                  this.filterResult.buy = 1
              }
              else {
                let delta = (candles[candles.length - 1].close - this.price) / this.price * 100
                if (!isPositive && this.buyValue < value/* || delta < -0.1 */) {
                  this.filterResult.sell = 1
                  this.count = 0
                }
                else
                  this.filterResult.hold = 1
              }
            }


          }
          this.prevInterpolation = out
        }
      }

      if (0) {
        if (candles.length > 30)
          if (value <= 0) {
            this.filterResult.sell = 1
            this.price = 0

          }
          else {

            if (this.price != 0) {
              let delta = (candles[candles.length - 1].close - this.price) / this.price * 100
              if (delta >= this.param.minDelta)
                this.filterResult.sell = 1
              if (this.prevValue > value && delta > this.param.minDelta)
                this.filterResult.sell = 1
              //if(delta < -this.param.minDelta)
              //  this.filterResult.buy = 1
            }
            else {
              if (this.prevValue < value && value >= this.param.minValue) {

                /*             let price = candles[candles.length - 1].close
                            for (let index = 0; index < close.length; index++) {
                              let res = close[index]//out[index]
                              let delta =  (res - price)/price*100
                              if(delta > this.param.minDelta){
                                this.filterResult.buy = 1
                                break
                              }
                            } */
                this.filterResult.buy = 1
              }

              else
                this.filterResult.hold = 1
            }

          }
      }
    }



    this.history[this.history.length - 1].sell = this.filterResult.sell
    this.history[this.history.length - 1].buy = this.filterResult.buy
    this.history[this.history.length - 1].hold = this.filterResult.hold
    /*     this.history.push({
          value: value,
          candles: candles[candles.length - 1].close,
          sell: this.filterResult.sell,
          buy: this.filterResult.buy,
          hold: this.filterResult.hold
        }) */

    //if(this.filterResult.buy == 1)
    //this.price = candles[candles.length - 1].close

    if (this.history.length > 2) {
      let prevData = this.history[this.history.length - 2]
      if (prevData.sell <= 0 && this.filterResult.sell > 0 && this.price > 0) {
        let delta = (candles[candles.length - 1].close - this.price) / this.price * 100
        this.history[this.history.length - 1].delta = delta
        this.price = 0
        this.buyValue = 0
      }

      if (prevData.buy <= 0 && this.filterResult.buy > 0) {
        this.price = candles[candles.length - 1].close
        this.buyValue = value
      }
    }

    if (this.history.length < candles.length) {
      this.filterResult = { sell: 0, buy: 0, hold: 1 }
    }
    while (this.history.length > candles.length) {
      this.history.shift();
    }

    /*     if(math.sign(this.prevValue) == math.sign(value))
        this.count++
        else{
          this.prevPeriod = this.count
          this.count = 1
        }
         */

    this.prevValue = value;
    return [this.prevValue, this.filterResult.sell, this.filterResult.buy/* , this.filterResult.hold */];
  }
  get() {
    return [this.prevValue, this.filterResult.sell, this.filterResult.buy/* , this.filterResult.hold */];
  }
  getNormal() {
    return this.filterResult
  }
}

class MyKDJResultCollector {
  constructor(param = undefined) {
    this.param = param
    this.myKDJ = new MyKDJ()
    this.myTradeKDJ = new MyKDJ()

    if (param == undefined) {
      this.param.minValue = 25
      this.param.a0 = 1
    }


    if (param == undefined) {

      this.ema0 = new MyEMA(710 * 1)
      this.ema1 = new MyEMA(543 * 1)
    } else {
      //if(param.a0>0 && param.a0<1000)
      this.ema0 = new MyEMA(math.abs(param.a0))
      //this.ema1 = new MyEMA(math.abs(param.a1))
      this.param.minValue = math.abs(param.minValue)
      //this.ema2 = new MyEMA(math.abs(param.a2))
      //this.ema3 = new MyEMA(math.abs(param.a3))
      //this.ema4 = new MyEMA(math.abs(param.a4))

      //this.ema0 = new MyEMA(710)
      //this.ema1 = new MyEMA(543)
    }
    //this.ema0 = new MyEMA(710)
    //this.ema1 = new MyEMA(10)

    this.prevValue = undefined
    this.prevValueTrade = undefined
    this.count = 0

    this.history = []
  }

  updateCandles(candles) {
    this.filterResult = { sell: 0, buy: 0, hold: 0 }
    //let value = this.myKDJ.updateArray(candles.slice(-this.myKDJ.N))
    //let value = this.myKDJ.updateArray(candles.slice(0))
    let value = this.myKDJ.updateArray(candles, math.mean(candles.map((x, i) => { return x.close }).slice(-1)))
    //let valueTrade = this.myTradeKDJ.updateArray(candles, math.mean(candles.map((x, i) => { return x.trades }).slice(-1)))
    value = this.ema0.update(value)
    //valueTrade = this.ema1.update(valueTrade)
    if (value <= 0)
      this.count++
    else
      this.count = 0
    //math.mean(candles.map((x,i)=>{return x.close}).slice(-15))
    if (this.prevValue == undefined) {
      this.filterResult.hold = 1
    }
    else {

      /*       let toBuy = this.param.k0*(value-this.prevValue) + this.param.k1*(valueTrade-this.prevValueTrade)
            //let toSell = this.param.k2*value + this.param.k3*valueTrade
            if (toBuy > 0) {
              this.filterResult.buy = 1
            }else{
              this.filterResult.sell = 1
            } */
      if (0) {
        let calcCoeff = (k0, k1, k2, k3) => {
          return k0 * sigmoid(value) +
            k2 * sigmoid(valueTrade) +
            k1 * sigmoid(value - this.prevValue) +
            k3 * sigmoid(valueTrade - this.prevValueTrade)
        }

        this.filterResult.buy = calcCoeff(this.param.bk0, this.param.bk1, this.param.bk2, this.param.bk3)
        this.filterResult.sell = calcCoeff(this.param.sk0, this.param.sk1, this.param.sk2, this.param.sk3)
        this.filterResult.hold = calcCoeff(this.param.hk0, this.param.hk1, this.param.hk2, this.param.hk3)

        // b = -1, 1, 1 , 1
        // s = 1, -1, 1 , 1
        // h = 0, 0, -1, - 1
      }
      if (1) {
        if (value < 0) {
          if (this.prevValue <= value &&
            math.abs(this.prevValue) >= this.param.minValue/* &&
            this.prevValueTrade < valueTrade &&
            valueTrade > 0 */
          )
            this.filterResult.buy = 1
          else
            this.filterResult.hold = 1
        }
        else {
          if (this.prevValue >= value /* &&
            this.prevValueTrade < valueTrade &&
            valueTrade > 0 */
          )
            this.filterResult.sell = 1
          else
            this.filterResult.hold = 1
        }

        if (0) {
          //this.filterResult.buy = this.ema2.update(this.filterResult.buy)
          //this.filterResult.sell = this.ema3.update(this.filterResult.sell)
          //this.filterResult.hold = this.ema4.update(this.filterResult.hold)
        }
      }
      /*        if (this.prevValue < value  ) {
              if (value <= 0)
                this.filterResult.buy = 1
              else
                this.filterResult.hold = 1
            }
            else {
              if (this.prevValue > value && this.prevValue > 0)
                this.filterResult.sell = 1
              else
                this.filterResult.hold = 1
            }  */
    }

    this.history.push({
      value: value,
      candles: candles[candles.length - 1].close,
      sell: this.filterResult.sell,
      buy: this.filterResult.buy,
      hold: this.filterResult.hold
    })
    while (this.history.length > candles.length) {
      this.history.shift();
    }

    //this.prevValueTrade = valueTrade;
    this.prevValue = value;
    return [this.prevValue, this.filterResult.sell, this.filterResult.buy/* , this.filterResult.hold */];
  }
  get() {
    return [this.prevValue, this.filterResult.sell, this.filterResult.buy/* , this.filterResult.hold */];
  }
  getNormal() {
    return this.filterResult
  }
}

class MyKDJ_MACDResultCollector {
  constructor() {
    this.myKDJ = new MyKDJ()
    this.myMACAD = new MyMACAD()
    this.ema0 = new MyEMA(5)
  }

  updateCandles(candles) {
    this.filterResult = { sell: 0, buy: 0, hold: 0 }
    //let valueKDJ = this.myKDJ.updateArray(candles.slice(-this.myKDJ.N))
    //let valueMACD = this.myMACAD.update(candles[candles.length - 1].close)

    let valueKDJ = this.myKDJ.updateArray(candles, math.mean(candles.map((x, i) => { return x.close }).slice(-5)))
    valueKDJ = this.ema0.update(valueKDJ)
    let valueMACD = this.myMACAD.update(math.mean(candles.map((x, i) => { return x.close }).slice(-15)))

    if (valueMACD < 0 && valueKDJ > 0) {
      this.filterResult.buy = 1
    }
    else {
      if (valueMACD > 0 && valueKDJ < 0) {
        this.filterResult.sell = 1
      }
      else
        this.filterResult.hold = 1
    }

    return [this.filterResult.sell, this.filterResult.buy, this.filterResult.hold];
  }
  get() {
    return [this.filterResult.sell, this.filterResult.buy, this.filterResult.hold];
  }
  getNormal() {
    return this.filterResult
  }
}

class MySimpleKDJ_MACDResultCollector {
  constructor() {
    this.myKDJ = new MyKDJ()
    this.myMACAD = new MyMACAD()
    this.ema0 = new MyEMA(5)
  }

  updateCandles(candles) {
    this.filterResult = { sell: 0, buy: 0, hold: 0 }
    this.filterResult2 = { sell: 0, buy: 0, hold: 0 }
    //let valueKDJ = this.myKDJ.updateArray(candles.slice(-this.myKDJ.N))
    //let valueMACD = this.myMACAD.update(candles[candles.length - 1].close)

    let valueKDJ = this.myKDJ.updateArray(candles, math.mean(candles.map((x, i) => { return x.close }).slice(-5)))
    valueKDJ = this.ema0.update(valueKDJ)
    let valueMACD = this.myMACAD.update(math.mean(candles.map((x, i) => { return x.close }).slice(-15)))

    if (valueMACD > 0)
      this.filterResult.buy = 1
    else
      this.filterResult.sell = 1

    if (valueKDJ > 0)
      this.filterResult2.buy = 1
    else
      this.filterResult2.sell = 1

    return [this.filterResult.sell, this.filterResult.buy, this.filterResult2.sell, this.filterResult2.buy];
  }
  get() {
    return [this.filterResult.sell, this.filterResult.buy, this.filterResult2.sell, this.filterResult2.buy];
  }
  getNormal() {
    return this.filterResult
  }
}

class IndicatorCollector {
  constructor(param) {
    this.complyteArraySize = 120
    this.tax = 0.001
    this.indicators = []
    this.candles = []
    //this.indicators.push({ indicator: new MyKDJResultCollector(param), name: "MyKDJResultCollector", correctPrecent: 0, totalCorrect: 0, total: 0, buyPrice: 0, history: [], result: 0, startPrice: 1 })
    this.indicators.push({ indicator: new MyMACADResultCollector(param), name: "MyMACADResultCollector", correctPrecent: 0, totalCorrect: 0, total: 0, buyPrice: 0, history: [], result: 0, startPrice: 1 })
  }

  updateCandle(candle) {
    let state = this.pushNewCandle(candle)
    if (state == "incorrectCandle")
      return false

    for (let indicator of this.indicators) {
      indicator.indicator.updateCandles(this.candles)
    }
    this.checkIndicators(candle)
    return true
  }

  checkIndicators(candle) {
    //let lastRealResult = getOutParams(this.netHistory[0].logPrices/* this.nextPrices[0] */, this.nextPrices);
    for (let indicator of this.indicators) {
      let result = indicator.indicator.getNormal()
      if (indicator.buyPrice == 0) {
        if (result.buy > result.sell && result.buy > result.hold) {
          indicator.buyPrice = candle.close
          if (indicator.startPrice == 1)
            indicator.startPrice = candle.close
        }
      } else {
        if (result.sell > result.buy) {
          let delta = candle.close - indicator.buyPrice - this.tax * indicator.buyPrice
          if (delta > 0)
            indicator.totalCorrect++
          indicator.total++
          indicator.correctPrecent = indicator.totalCorrect / indicator.total * 100
          indicator.result += delta
          indicator.history.push(delta)

          indicator.buyPrice = 0
        }
        else {
          if (0)
            if (candle.close < indicator.buyPrice * (0.99/* 1 - this.tax * 20 */)) {
              let delta = candle.close - indicator.buyPrice - this.tax * indicator.buyPrice
              indicator.total++
              indicator.result += delta
              indicator.correctPrecent = indicator.totalCorrect / indicator.total * 100
              indicator.history.push(delta)
              indicator.buyPrice = 0
            }
        }
      }
    }
  }

  pushNewCandle(candle) {
    // element.close;//
    // element.high;//
    // element.low;//
    // element.start;
    // element.trades;//
    // element.volume;//
    // element.vwp;//

    if (this.candles.length > 0)
      if (candle.start - this.candles[this.candles.length - 1].start != 60)
        this.candles = []
    if (candle.trades == 0 || candle.volume == 0) {
      this.candles = []
      return "incorrectCandle";
    }
    this.candles.push(candle);

    while (this.candles.length > this.complyteArraySize)
      this.candles.shift()

    if (this.candles.length < this.complyteArraySize) {
      return "incorrectLength";
    }
    else {
      return "correct";
    }
  }
}


module.exports.getOutParams = getOutParams;
module.exports.getSSA = getSSA;
module.exports.MySSAResultCollector = MySSAResultCollector;
module.exports.MyLinearResultCollector = MyLinearResultCollector;
module.exports.MyEMA = MyEMA;
module.exports.MyMACAD = MyMACAD;
module.exports.MyKDJ = MyKDJ;
module.exports.MyTSI = MyTSI;
module.exports.MyMACADResultCollector = MyMACADResultCollector;
module.exports.MyKDJResultCollector = MyKDJResultCollector;
module.exports.MyKDJ_MACDResultCollector = MyKDJ_MACDResultCollector;
module.exports.MySimpleKDJ_MACDResultCollector = MySimpleKDJ_MACDResultCollector;
module.exports.IndicatorCollector = IndicatorCollector;

