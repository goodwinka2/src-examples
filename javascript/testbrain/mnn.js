var brain = require('brain.js');
var layernet = require("./layernet.js");
const { PerformanceObserver, performance } = require('perf_hooks');

var MainNet = brain.NeuralNetwork;


var usingNetTypes = [
  {name: "NeuralNetwork", net: brain.NeuralNetwork},
  {name: "LayerNetwork", net: layernet.LayerNetwork},
];

var ActualNet = usingNetTypes[1];


var useStandartsqlite3 = false;
if (useStandartsqlite3)
  var _sqlite3 = require('sqlite3');
else
  var _sqlite3 = require('better-sqlite3');//('foobar.db', options);
//var wait = require('wait.for');

// brain.NeuralNetwork - Feedforward Neural Network with backpropagation
// brain.NeuralNetworkGPU - Feedforward Neural Network with backpropagation, GPU version
// brain.recurrent.RNNTimeStep - Time Step Recurrent Neural Network or "RNN"
// brain.recurrent.LSTMTimeStep - Time Step Long Short Term Memory Neural Network or "LSTM" 
// brain.recurrent.GRUTimeStep - Time Step Gated Recurrent Unit or "GRU"
// brain.recurrent.RNN - Recurrent Neural Network or "RNN"
// brain.recurrent.LSTM - Long Short Term Memory Neural Network or "LSTM"
// brain.recurrent.GRU - Gated Recurrent Unit or "GRU"

function sigmoid(t) {
  return 1/(1+Math.pow(Math.E, -t));
}

sigmoidArray = (arr,shift = 0) =>
{
  let newarr = arr.slice();
  for (key in newarr) {
    if (newarr.hasOwnProperty(key)) 
      {
        newarr[key] = sigmoid(newarr[key] - shift);
      }
    }
  return newarr;
}

strToFloat = (tmpArr) => {
  //let tmpArr = row.data_input.split(',');
  tmpArr.forEach((element, index) => {
    tmpArr[index] = parseFloat(element);
  });
  
  return tmpArr;
};

testf = (value) => {

  var element = 0.0;
  for (let index = 0; index < value.length; index++) {
    element += /* Math.abs */(value[index]);
  }
  if (element == 0)
    element = 1;
  return  (1 / (element));
};
roundArray = (arr) => {
  for (let index = 0; index < arr.length; index++) {
    arr[index] = Math.round(arr[index]);
  }
};

floorArray = (arr,value = 4) =>{
  let newarr =Object.assign({},arr)// arr.slice();;// = new Array;//arr.slice();
/*   for (key in arr) {
    if (arr.hasOwnProperty(key)) 
    newarr.push(arr[key]);
  }   */
  value = Math.pow(10,value);
  for (key in newarr) {
    if (newarr.hasOwnProperty(key)) 
    newarr[key] = parseInt(newarr[key]*value,10)/value;//Math.floor( newarr[key]*value)/value;
  }  
  return newarr;
};

summArray = (arr) => {
  var element = 0.0;
  var size = 0, key;
  for (key in arr) {
      if (arr.hasOwnProperty(key)) 
      element += Math.abs(arr[key]);
  }
  return element;
  var element = 0.0;
  for (let index = 0; index < arr.length; index++) {
    element += Math.abs(arr[index]);
  }
  //if(element==0)element=1;
  return element;
};

function median(arr){
  if(arr.length ===0) return 0;

  let values = arr.slice();

  values.sort(function(a,b){
    return a-b;
  });

  var half = Math.floor(values.length / 2);

  if (values.length % 2)
    return values[half];

  return (values[half - 1] + values[half]) / 2.0;
}

function medianIRQ(arr, newShift = undefined){
  if(arr.length ===0) return 0;

  let values = arr.slice();
  values.sort(function(a,b){
    return a-b;
  });
  let getFromIndex = (index)=>{return values[Math.floor(index)];};
  //var half = Math.floor(values.length / 2);
  let result = {
    Q1: getFromIndex(values.length / 4),
    median: getFromIndex(values.length / 2),
    Q3: getFromIndex(values.length * 3 / 4),
    IRQ: 0,
    min: 0,
    max: 0
  };
  result.IRQ = result.Q3 - result.Q1;
  if(newShift == undefined){
  result.min = result.Q1 - result.IRQ*1.5;
  result.max = result.Q3 + result.IRQ*1.5;
  }else{
  let max =Math.max( Math.abs(getFromIndex(0)),Math.abs( getFromIndex(values.length-1)))
    result.min = max
    result.max = max
    result.median = newShift
  }
  return result;
}

function mean(values){
  return summArray(values)/values.length;
}

normArray = (arr,newShift = undefined) =>
{

  let newarr = arr.slice();
  let element = NaN;
  let isInit = false;
  

  let irq = medianIRQ(newarr,newShift)
  let shift =  irq.median;
  let max =  irq.max - shift;//median(newarr)*2;
  let min = shift - irq.min;
  if(max == 0)max = 1;
  if(min == 0)min = 1;

  
  
  for (key in arr) {
    if (arr.hasOwnProperty(key)) 
      {
        newarr[key] = arr[key]-shift;
        if(isInit == false)
          {
            element = newarr[key]-shift;
            //max = element;
            isInit = true;
          }
        
        //if(max < Math.abs(newarr[key]))  
        //  max = Math.abs(newarr[key]);   
      }
  }
  if(max == 0)
    max = 1

  //max*=2;

  for (key in newarr) {
    if (newarr.hasOwnProperty(key)) 
      {
        if(newarr[key] > 0)
          newarr[key]/=max; 
        else
          newarr[key]/=min; 

          //newarr[key]+=1;
          //newarr[key]/=2;

        if(newarr[key]>1)
          newarr[key] = 1;
        else
        if(newarr[key]<-1)
          newarr[key]=-1;
      }
  }
  return newarr;
}

diffArray = (arr1, arr2) => {
  let element = 0.0;
  let size = 0;
  for (key in arr1) {
    if (arr1.hasOwnProperty(key)) 
    {element += Math.abs(arr1[key]-arr2[key]);size++;}
}
  //if(element==0)element=1;
  return element/size;
};

function dbErrorHandle(err) {
  if (err) {
    return console.error("==== dbErrorHandle ==== :" + err.message);
  };
}

class MyNeuralNetwork {
  constructor(name,additionalName = this.additionalName) {
    this.name = name;
    this.additionalName = additionalName;
    this.currentNet = new ActualNet.net();//new layernet.LayerNetwork();//new MainNet;
    


  }
  destructor() {
    /*     db.close((err) => {
          if (err) {
            return console.error(err.message);
          }
          console.log('Close the database connection.');
        }); */
    //this.dbTrainExample.close();
  }
  hello() {
    return 'Hello, I am ' + this.name + '.'
  }
  openDb()
  {
    if (useStandartsqlite3)
    {
      this.dbTrainExample = new _sqlite3.Database('MyNeuralNetwork_dbTrainExample.sqlite', dbErrorHandle);
      //this.dbNets = new _sqlite3.Database('MyNeuralNetwork_dbNets.sqlite', dbErrorHandle);
    }
  else
    {
      this.dbTrainExample = new _sqlite3('MyNeuralNetwork_dbTrainExample.sqlite');//, dbErrorHandle);
      this.dbNets = new _sqlite3('MyNeuralNetwork_dbNets.sqlite');
    }
  let tablename = this.name;//"tests_ivert";
  //let result = this.dbTrainExample.run('CREATE TABLE IF NOT EXISTS ' + tablename + '(info,data_input,data_output)', dbErrorHandle);
  let result = this.dbTrainExample.prepare('CREATE TABLE IF NOT EXISTS ' + tablename + '(info,data_input,data_output)').run();
  this.dbNets.prepare('CREATE TABLE IF NOT EXISTS ' + tablename + '(netType,json_data,result,additionalName)').run();

  }

  closeDb(){
    this.dbNets.close()
    this.dbTrainExample.close();
    this.dbNets = null;
    this.dbTrainExample = null;
  }

  writer() {
    this.openDb();

    if (useStandartsqlite3) {
      this.dbTrainExample.run("BEGIN TRANSACTION", dbErrorHandle);
      this.data.forEach(element => {
        //id++;
        this.dbTrainExample.run("INSERT INTO " + this.name + "(info,data_input,data_output) SELECT ?,?,? WHERE NOT EXISTS (SELECT 1 FROM " + this.name + " WHERE info = ?)",
          [element.id, element.input, element.output, element.id],
          dbErrorHandle);
      });
      this.dbTrainExample.run("END TRANSACTION;", dbErrorHandle);
    }
    else {
      let returns = this.dbTrainExample.prepare("BEGIN TRANSACTION").run();
      this.data.forEach(element => {
        //id++;
        let returns =this.dbTrainExample.prepare("INSERT INTO " + this.name + "(info,data_input,data_output) SELECT ?,?,? WHERE NOT EXISTS (SELECT 1 FROM " + this.name + " WHERE info = ?)").
          run(element.id, element.input.join(','), element.output.join(','), element.id);
      });
      this.dbTrainExample.prepare("END TRANSACTION;").run();
    }
    this.closeDb();
  }

  reader() {
    this.openDb();
    console.time('startReadData');
    //
    if (useStandartsqlite3) {
      this.dbTrainExample.each(
        'SELECT info,data_input input,data_output output FROM ' + this.name, [],
        (err, row) => {
          if (err) {
            dbErrorHandle(err);
            return;
          }
          this.data.push({ input: (row.input.split(',')), output: row.output.split(',') });
          //parseFloat
        },
        (err, row) => {
          this.dbTrainExample.run("END TRANSACTION;", dbErrorHandle);
          console.log("read data:" + this.data.length);
          console.timeEnd('startReadData');
        }/* , 
    this.waiter2.bind(this) */ );
    }
    else {
      let rows = this.dbTrainExample.prepare("SELECT * FROM " + this.name).all();

      console.timeEnd('startReadData');
      console.time('startPushData');
      rows.forEach(row => {
        this.data.push({ input: strToFloat(row.data_input.split(',')), output: strToFloat(row.data_output.split(',')) });
      })
      console.timeEnd('startPushData');
      console.log("read data:" + this.data.length);
    }
    this.closeDb();
    //this.dbTrainExample.run("END TRANSACTION;",dbErrorHandle);
  }

  normalizeData()
  {
    this.dataNormal = this.data;
    for (let index = 0; index < this.dataNormal.length; index++) {
      this.dataNormal[index].input = normArray(this.dataNormal[index].input);
    }
    
  }

  tryTrain()
  {
    if (this.data.length-this.prevDataLength > this.tryTrainStep) {
      this.prevDataLength = this.data.length;
      //this.train();
      this.writer();
    }
  }



  train() {
    console.time('trainTime');

    let outputCount = this.data[0].output.length;
    let count = this.data[0].input.length;
    //this.config.hiddenLayers = [count,  Math.max((count)/3, 20),  count];
    this.config.hiddenLayers = [Math.floor((count*1)), Math.floor((count/1.1))];
    //this.config.hiddenLayers = [30];
    roundArray(this.config.hiddenLayers);

    console.log("default hiddenLayers=" + this.config.hiddenLayers);

    console.log("train size " + this.data.length);

    //if(!this.isTrained)
    //  this.loadNet();
    //console.time('normalizeData');
    //this.normalizeData();
    //console.timeEnd('normalizeData');

    let result = this.currentNet.train(this.data, this.config);
    this.isTrained = true;
    if (this.saveTrainedNet) 
      this.saveNet(result);    
    
    
    console.log("train error " + result.error);
    console.log("train iterations=" + result.iterations);
    console.timeEnd('trainTime');

  }
  saveNet(result)
  {
    this.openDb();
    let netjs = JSON.stringify(this.currentNet.toJSON());
    this.dbNets.prepare("DELETE FROM "+ this.name + " WHERE netType = ? AND additionalName = ?").run(ActualNet.name, this.additionalName);

    this.dbNets.prepare("INSERT INTO " + this.name + "(netType,json_data,result,additionalName) VALUES(?,json(?),json(?),?)").run(
      ActualNet.name,netjs,JSON.stringify(result), this.additionalName
    );
    console.log("saveNet: "+ JSON.stringify(result));
    this.closeDb();
    //netjs = null;
  }  
  loadNet()
  {
    this.openDb();
    let json = this.dbNets.prepare("SELECT * FROM "+ this.name +" WHERE netType = ? AND additionalName = ?");
    let net = json.get(ActualNet.name, this.additionalName).json_data;

    this.currentNet.fromJSON(JSON.parse(net));
    console.log("loadNet: "+ json.get(ActualNet.name, this.additionalName).result);
    this.isTrained = true;
    this.closeDb();
    //json = null;
    //net = null;
  }

  run(vars) {
    //this.currentNet =this.crossValidate.toNeuralNetwork();
    return this.currentNet.run(vars);
    //return v1;
  }
  selfTest() {
    console.time('testTime');
    for (let testIndex = 0; testIndex < this.data.length; testIndex += Math.ceil(this.data.length / 20)) {

      let testValue = this.data[testIndex].input;
      let output = this.run(testValue);
      //console.log("===== " + testIndex + "\tnet=\t" + output + "\tneed=\t" + this.data[testIndex].output + "\terr=\t" + (output - this.data[testIndex].output));
      if(0)
      console.log("===== " + testIndex + 
      "\tnet=\t" + summArray(output)/this.data[testIndex].output.length + 
      "\tneed=\t" + summArray(this.data[testIndex].output)/this.data[testIndex].output.length + 
      "\terr=\t" + (diffArray(output,this.data[testIndex].output)/this.data[testIndex].output.length)+
      "\tratio=\t" + (diffArray(output,this.data[testIndex].output))/summArray(output)
      );
      else
      console.log("===== " + testIndex + 
      "\t\t\tnet=\t" + JSON.stringify(floorArray(output)) + 
      "\t\t\tneed=\t" + JSON.stringify(floorArray(this.data[testIndex].output)) + 
      "\t\t\terr=\t" +JSON.stringify( floorArray([diffArray(output,this.data[testIndex].output)/this.data[testIndex].output.length]))+
      "\t\t\tratio=\t" + JSON.stringify(floorArray([diffArray(output,this.data[testIndex].output)/summArray(output)]))
      );

    }
    console.timeEnd('testTime');
  }
  saveTrainedNet = true;
  
  isTrained = false;
  dbTrainExample;
  dbNets;
  name = "MyNeuralNetworkName";
  additionalName = "NaN"
  data = new Array;
  dataNormal;
  prevDataLength = 0;
  tryTrainStep = 100;
  currentNet;
  crossValidate;
  prevError = 1
  trainTime = 0
  config = {
    activation: 'tanh', // activation function   | 'sigmoid' | 'relu' | 'leaky-relu' | 'tanh';
    //activation: 'sigmoid',
    //hiddenLayers: [50,5,50],
    iterations: 200000, // the maximum times to iterate the training data --> number greater than 0
    errorThresh: 0.005, // the acceptable error percentage from training data --> number between 0 and 1
    log: false, // true to use console.log, when a function is supplied it is used --> Either true or a function
    logPeriod: 10, // iterations between logging out --> number greater than 0
    learningRate: 0.003, // scales with delta to effect training rate --> number between 0 and 1
    momentum: 0.001, // scales with next layer's change value --> number between 0 and 1
    callback: (state)=>{
      const eps = 0.00001;
      let elapsed = performance.now() - this.trainTime;
      let dynamicError = this.prevError - state.error;
      let timeToFinish = (state.error - this.currentNet.trainOpts.errorThresh )/dynamicError* elapsed /1000;
      console.log(state.iterations +
        "\terr: " + JSON.stringify(floorArray([state.error])) + 
        "\tderr: "+JSON.stringify(floorArray([dynamicError],6)) +
        "\tt: "+JSON.stringify(floorArray([elapsed])) +
        "\tendt: " + JSON.stringify(floorArray([timeToFinish])) + " [s]") ;
        if(Math.abs(dynamicError) < eps){
          this.currentNet.trainOpts.errorThresh = state.error;
          console.log("cant train net");
        }    
      this.prevError = state.error;
      this.trainTime = performance.now();
    },// null, // a periodic call back that can be triggered while training --> null or function
    callbackPeriod: 10, // the number of iterations through the training data between callback calls --> number greater than 0
    timeout: Infinity, // the max number of milliseconds to train for --> number greater than 0
  }
}


module.exports.MyNeuralNetwork = MyNeuralNetwork;
module.exports.strToFloat=strToFloat;
module.exports.roundArray=roundArray;
module.exports.floorArray=floorArray;
module.exports.summArray=summArray;
module.exports.diffArray=diffArray;
module.exports.normArray=normArray;
module.exports.mean=mean;
module.exports.median=median;
module.exports.sigmoid=sigmoid;
module.exports.sigmoidArray=sigmoidArray;

