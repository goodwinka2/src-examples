
import numpy as np
import copy
def roll(ar,      # ND array
         w,      # rolling 1D window array
         dx=1):  # step size (horizontal)
    a = np.array(ar)
    b = np.zeros(w)
    shape = a.shape[:-1] + (int((a.shape[-1] - b.shape[-1]) / dx) + 1,) + b.shape
    strides = a.strides[:-1] + (a.strides[-1] * dx,) + a.strides[-1:]
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def plot(data):
    import plotly.graph_objects as go
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=data,
        line_color='rgb(255,0,0)',
        name='present'))
    fig.show()

def getSSA(data,slidePart,interpolationSize=1,sKoef = 0.5):
    slideSize = int(np.ceil(len(data)*slidePart))
    dataNew = copy.deepcopy(data)
    hankel = np.matrix(roll(data,slideSize))
    X = hankel.getT()*hankel
    U, S, V = np.linalg.svd(X)
    condition = S>=S[int(len(S)*sKoef)]
    #condition = S >= S[2]
    #plot(S)
    V = V[condition]
    v = V[:,:-1]
    vv = V[:,-1].getT()
    M = vv*(v*v.getT()).getI()*v
    for i in range(interpolationSize):
        Q = np.matrix(dataNew[1-slideSize:]).getT()
        dataNew.append((M*Q)[0,0])
    return dataNew

def getTrendSSA(data,sKoef = 0.5):
    slidePart = 0.5
    interpolationSize = 3
    dataNew = getSSA(data,slidePart,interpolationSize,sKoef)

    return dataNew[-1]>dataNew[-2] and dataNew[-2]>dataNew[-3]

if False:

    import plotly.graph_objects as go

    a = [0,1,2,3,4,5,6,7,8,9]
    a_sin = np.cos(a).tolist()
    r = getSSA(a_sin,0.5,20)
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        y=r,
        line_color='rgb(255,0,0)',
        name='present'))
    fig.add_trace(go.Scatter(
        y=a_sin,
        line_color='rgb(0,255,0)',
        name='present'))
    fig.show()