import os

from sqlitedict import SqliteDict
import simplethread
import logging as log
from datetime import datetime, timedelta, timezone
from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    Currency
)
from tinkoff.investments.utils.historical_data import HistoricalData
import codetiming
t = codetiming.Timer(name="class",logger=log.debug)
import functools
import config
candleMinutes = {
    CandleResolution.MIN_1.value :1,
    CandleResolution.MIN_2.value :1*2,
    CandleResolution.MIN_3.value :1*3,
    CandleResolution.MIN_5.value :1*5,
    CandleResolution.MIN_10.value :1*10,
    CandleResolution.MIN_15.value :1*15,
    CandleResolution.MIN_30.value :1*30,
    CandleResolution.HOUR.value :1*60,
    CandleResolution.DAY.value :1*60*24,
    CandleResolution.WEEK.value :1*60*24*7,
    CandleResolution.MONTH.value :1*60*24*30,

}

class TinkoffDB(simplethread.SimpleThread):
    def __init__(self,tinko):
        simplethread.SimpleThread.__init__(self)
        self.tinko = tinko
        self.dbName = './tinkoff_db.sqlite'
        self.dbNameUpdates='./tinkoff_updates_db.sqlite'

        #self.clear()

    def getTable(self, tablename):
        return SqliteDict(self.dbName, tablename=tablename, autocommit=True)

    def getTableUpdates(self, tablename):
        return SqliteDict(self.dbNameUpdates, tablename=tablename, autocommit=True)

    def clear(self):
        if os.path.exists(self.dbName):
            os.remove(self.dbName)
        if os.path.exists(self.dbNameUpdates):
            os.remove(self.dbNameUpdates)

    def getCandles(self,figi,interval=CandleResolution.HOUR):
        candles = self.getTable(figi).get(interval.value,default=[])
        self.normalizeCandles(candles)
        if config.staticSimulation:
            candles = self.getCandlesToNowData(candles)
        return candles

    def getCandlesToNowData(self,candles):
        return list(filter(lambda x: x.time <= datetime.now(timezone.utc),candles))

    def getIsNew(self,figi,interval=CandleResolution.HOUR):
        return self.getTableUpdates(figi).get(interval.value,default=True)

    def setIsNew(self,figi,isNew,interval=CandleResolution.HOUR):
        self.getTableUpdates(figi)[interval.value] = isNew

    def setCandles(self, figi,candles, interval=CandleResolution.HOUR):
        self.getTable(figi)[interval.value] = candles

    def setFavorite(self,favorite):
        self.getTable('data')['favorite'] = favorite

    def getFavorite(self):
        return self.getTable('data').get('favorite',default=[])

    def setInfo(self,figi,info):
        self.getTableUpdates('info')[figi] = info

    def setLastTradeInfo(self,figi,info):
        self.getTableUpdates('trade')[figi] = info

    def getLastTradeInfo(self,figi):
        return self.getTableUpdates('trade').get(figi,default=None)

    def clearLastTradeInfo(self):
        return self.getTableUpdates('trade').clear()

    def getInfo(self,figi):
        info = self.getTableUpdates('info').get(figi,default=None)
        if info is None:
            self.updateInfo(figi)
            info = self.getTableUpdates('info').get(figi, default=None)
        return info

    def getData(self,id):
        info = self.getTableUpdates('data').get(id,default=None)
        return info

    def setData(self,id,data):
        self.getTableUpdates('data')[id] = data

    def updateInfo(self,figi):
        info = None
        while info is None:
            info = self.tinko.blockingCall(self.tinko.get_stock_info, figi)
        self.setInfo(figi,info)

    def normalizeCandles(self,candles):
        def normalizeCandle(prev,current):
            if prev is not None:
                current.o = prev.c
                current.h = max(current.h,current.o)
                current.l = min(current.l, current.o)
            return current
        functools.reduce(normalizeCandle,candles,None)

    def getActualCandles(self, figi, count = 60, interval=CandleResolution.HOUR):
        #candles = self.getTable(figi).get(interval.value, default=[])
        #if not bool(candles):
        self.updateCandles(figi,interval,days = 0,hours = 0,minutes = candleMinutes[interval.value]*5*count)
        candles = self.getTable(figi).get(interval.value, default=[])
        self.normalizeCandles(candles)
        if config.staticSimulation:
            candles = self.getCandlesToNowData(candles)
        return candles[-count:]

    def updateCandles(self,figi, interval=CandleResolution.HOUR,days = 90,hours = 0, minutes = 0):
        #t.start()

        newCandlesCount = 0

        if config.staticSimulation:
            candles = self.getTable(figi).get(interval.value, default=[])
            self.normalizeCandles(candles)
            if not bool(candles):
                log.debug(
                    f'empty static {figi} candles {len(candles)} ({newCandlesCount} new) {interval.value}')
                return
            last = candles[-1].time
            now = datetime.now(timezone.utc)
            if now > last:
                candlesNew = self.tinko.blockingCall(self.tinko.get_candles, figi,
                                                  now - timedelta(days=days, hours=hours, minutes=minutes),
                                                  now + timedelta(days=3, hours=0, minutes=0), interval)
                for var in candlesNew:
                    if var.time > last:
                        candles.append(var)
                self.setCandles(figi, candles, interval)
                # t.stop()
            candles = self.getCandlesToNowData(candles)
            if bool(candles):
                newCandlesCount = 1 if candles[-1].time + timedelta(minutes=candleMinutes[interval.value]) > now else 0
            self.setIsNew(figi, newCandlesCount > 0)

            log.debug(
                f'updated static {figi} candles {len(candles)} ({newCandlesCount} new) {interval.value} last {candles[-1].time if bool(candles) else None}')
            return

        candles = self.getCandles(figi, interval)
        if bool(candles):
            first = candles[0].time
            last = candles[-1].time
            now = datetime.now(timezone.utc)
            if now - timedelta(days=days,hours = hours, minutes = minutes) >= first:
                candlesNew = None
                while candlesNew is None:
                    candlesNew = self.tinko.blockingCall(self.tinko.get_candles, figi, last, now, interval)
                for var in candlesNew:
                    if var.time > last:
                        candles.append(var)
                        newCandlesCount+=1
            else:
                candles = None
                while candles is None:
                    candles = self.tinko.blockingCall(self.tinko.get_candles, figi,
                                                      datetime.now() - timedelta(days=days,hours = hours, minutes = minutes),
                                                      datetime.now(), interval)
                newCandlesCount = len(candles)
        else:
            candles = None
            while candles is None:
                candles = self.tinko.blockingCall(self.tinko.get_candles,
                                                  figi, datetime.now() - timedelta(days=days,hours = hours, minutes = minutes),
                                                  datetime.now(), interval)
            newCandlesCount = len(candles)

        self.setCandles(figi,candles,interval)
        #t.stop()
        self.setIsNew(figi,newCandlesCount>0)

        if bool(candles):
            log.debug(f'updated {figi} candles {len(candles)} ({newCandlesCount} new) {interval.value} last {candles[-1].time}')
        else:
            log.debug(
                f'updated {figi} candles empty!')


    def getCurrency(self, figi):
        currency = ''
        info = self.getInfo(figi)
        if info is not None:
            currency = info.currency.value
        return currency

    def getBalance(self):
        portfolio = self.tinko.blockingCall(self.tinko.get_portfolio)
        currencies = self.tinko.blockingCall(self.tinko.get_currencies)
        portfolio_currencies = self.tinko.blockingCall(self.tinko.get_portfolio_currencies)
        balance = {}
        balance['RUB'] = 0
        for currency in currencies:
            balance[currency.currency.value] =0
        for currency in portfolio_currencies:
            balance[currency.currency.value] = currency.balance

        for stock in portfolio:

            if next((True for x in currencies if x.figi == stock.figi), False):
                continue
            price = 0
            candles = self.getCandles(stock.figi)
            if candles is not None:
                if len(candles)>0:
                    price = candles[-1].c
            balance[self.getCurrency(stock.figi)] += price*stock.balance

        return balance