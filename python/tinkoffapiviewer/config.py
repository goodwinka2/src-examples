
useDockerNetwork = True
startTrade = True
useProduction = False
reinitSand = True
useSimulation = True
useTelegram = False

loadDataForSimulation = False

usePriceFromOrderBook = False
simulationDays = 30
staticSimulation = True

clearDb = False
updateOnStart = False
usePrice = 1
maxIterations = 2

tokenSand = ""
tokenProd = ""
wait_on_rate_limit = False