import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from datetime import datetime, timedelta
import plotly.graph_objects as go
import plotly
import plotly.express as px
import pandas as pd
import numpy as np
import myIndicators

import io
import base64
from myLogger import logList
from tinkoff.investments import (
    Candle,
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    Currency,
)

import myMath

callbackList = []

BLOCK_STYLE = {
    #"width": "16rem",
    "padding": "1rem 2rem",
    "background-color": "#e5e5e5",
}
#https://coolors.co/000000-14213d-fca311-e5e5e5-ffffff


def getGraphFromMPL(plt):
    buf = io.BytesIO()  # in-memory files
    plt.savefig(buf, format = "png") # save to the above file object
    plt.close()
    data = base64.b64encode(buf.getbuffer()).decode("utf8") # encode to html elements
    return "data:image/png;base64,{}".format(data)

def createTemplateGraph():
    open_data = [33.0, 33.3, 33.5, 33.0, 34.1]
    high_data = [33.1, 33.3, 33.6, 33.2, 34.8]
    low_data = [32.7, 32.7, 32.8, 32.6, 32.8]
    close_data = [33.0, 32.9, 33.3, 33.1, 33.1]
    dates = [datetime(year=2013, month=10, day=10),
             datetime(year=2013, month=11, day=10),
             datetime(year=2013, month=12, day=10),
             datetime(year=2014, month=1, day=10),
             datetime(year=2014, month=2, day=10)]

    fig = go.Figure(data=[go.Candlestick(
        x=dates,
        open=open_data,
        high=high_data,
        low=low_data,
        close=close_data)
        ])
    g = dcc.Graph(id="graph",figure=fig)
    return html.Div([
    g
    ])

def createCallbackableTable(name,data,page_size=20):
    return html.Div([dash_table.DataTable(
        id=f'datatable-{name}',
        columns=[
            {'name': i, 'id': i, 'deletable': False,"selectable": True} for i in data.columns
            # omit the id column
            if i != 'id' and i!= 'data' and i!='stock'
        ],
        data=data.to_dict('records'),
        #editable=True,
        filter_action="native",
        sort_action="native",
        sort_mode='multi',
        column_selectable="multi",
        row_selectable='multi',
        row_deletable=False,
        selected_rows=[],
        selected_columns=[],
        #page_action='native',
        page_current=0,
        page_size=page_size,
        style_cell={
            'height': 'auto',
             'maxWidth': '400px',
            'whiteSpace': 'normal'
        },
        style_table={'overflowX': 'scroll'}
    ),html.P('\nGraph:\n'),html.Div(id=f'datatable-{name}-container')])

def createCallbackGraph(app,name,x='name',columns = ["Balance", "Lots"]):
    @app.callback(
        Output(f'datatable-{name}-container', "children"),
        Input(f'datatable-{name}', "derived_virtual_data"),
        Input(f'datatable-{name}', "derived_virtual_selected_rows"),
        Input(f'datatable-{name}', "selected_columns"),
        State(f'datatable-{name}', 'data')
    )

    def update_graphs(rows, derived_virtual_selected_rows,derived_virtual_selected_columns,data):

        if derived_virtual_selected_rows is None:
            derived_virtual_selected_rows = []

        dff = data if rows is None else pd.DataFrame(rows)

        colors = ['#7FDBFF' if i in derived_virtual_selected_rows else '#0074D9'
                  for i in range(len(dff))]

        return [
            dcc.Graph(
                id=column,
                figure={
                    "data": [
                        {
                            "x": dff[x],
                            "y": dff[column],
                            "type": "bar",
                            "marker": {"color": colors},
                        }
                    ],
                    "layout": {
                        "xaxis": {"automargin": True},
                        "yaxis": {
                            "automargin": True,
                            "title": {"text": column}
                        },
                        "height": 250,
                        "margin": {"t": 10, "l": 10, "r": 10},
                    },
                },
            )

            for column in derived_virtual_selected_columns if column in dff
        ]
    return update_graphs

def createGraph(candles,title=None):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    dates = []

    for candle in candles:
        open_data.append(candle.o)
        high_data.append(candle.h)
        low_data.append(candle.l)
        close_data.append(candle.c)
        dates.append(candle.time)

    fig = go.Figure(data=[go.Candlestick(
        x=dates,
        open=open_data,
        high=high_data,
        low=low_data,
        close=close_data)
        ])

    fig.update_xaxes(nticks=4)

    # fig.update_xaxes(
    #     rangeslider_visible=True,
    #     tickformatstops=[
    #         dict(dtickrange=[None, 1000], value="%H:%M:%S.%L ms"),
    #         dict(dtickrange=[1000, 60000], value="%H:%M:%S s"),
    #         dict(dtickrange=[60000, 3600000], value="%H:%M m"),
    #         dict(dtickrange=[3600000, 86400000], value="%H:%M h"),
    #         dict(dtickrange=[86400000, 604800000], value="%e. %b d"),
    #         dict(dtickrange=[604800000, "M1"], value="%e. %b w"),
    #         dict(dtickrange=["M1", "M12"], value="%b '%y M"),
    #         dict(dtickrange=["M12", None], value="%Y Y")
    #     ]
    # )
    fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_layout(title_text=title)
    fig.update_xaxes(showticklabels=False)
    fig.update_xaxes(rangeslider_visible=False)
    g = dcc.Graph(id="graph",figure=fig)
    return html.Div([
    g
    ])

def createPrecentCandleGraph(candles,title=None):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    dates = []

    for candl in candles:
        open_data.append((candl.o - candl.o)/candl.o*100)
        high_data.append((candl.h - candl.o) / candl.o * 100)
        low_data.append((candl.l - candl.o) / candl.o * 100)
        close_data.append((candl.c - candl.o) / candl.o * 100)
        dates.append(candl.time)

    fig = go.Figure(data=[go.Candlestick(
        x=dates,
        open=open_data,
        high=high_data,
        low=low_data,
        close=close_data)
        ])

    fig.update_xaxes(nticks=4)

    # fig.update_xaxes(
    #     rangeslider_visible=True,
    #     tickformatstops=[
    #         dict(dtickrange=[None, 1000], value="%H:%M:%S.%L ms"),
    #         dict(dtickrange=[1000, 60000], value="%H:%M:%S s"),
    #         dict(dtickrange=[60000, 3600000], value="%H:%M m"),
    #         dict(dtickrange=[3600000, 86400000], value="%H:%M h"),
    #         dict(dtickrange=[86400000, 604800000], value="%e. %b d"),
    #         dict(dtickrange=[604800000, "M1"], value="%e. %b w"),
    #         dict(dtickrange=["M1", "M12"], value="%b '%y M"),
    #         dict(dtickrange=["M12", None], value="%Y Y")
    #     ]
    # )
    fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_layout(title_text=title)
    fig.update_xaxes(showticklabels=False)
    fig.update_xaxes(rangeslider_visible=False)
    g = dcc.Graph(id="graph",figure=fig)
    return html.Div([
    g
    ])

def createLinePlot(data,date,title=None,x=None,y=None):
    #fig = px.line(data, x=x, y=y, title=title)
    fig = go.Figure(data=go.Scatter(x=date, y=data))
    fig.update_layout(dict(title_text=title,xaxis=dict(type="category", categoryorder='category ascending')))
    fig.update_layout(title_text=title)
    fig.update_xaxes(showticklabels=False)
    #fig.update_xaxes(rangeslider_visible=True)
    g = dcc.Graph(id="graph", figure=fig)
    return html.Div([ g ])

def createLinePlotFromCandle(candles,title = None):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    dates = []

    for candle in candles:
        open_data.append(candle.o)
        high_data.append(candle.h)
        low_data.append(candle.l)
        close_data.append(candle.c)
        dates.append(candle.time)

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=dates, y=close_data,
        line_color='rgb(0,0,0)',
        name='close',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=high_data,
        line_color='rgb(155,155,155)',
        name='hight',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=low_data,
        line_color='rgb(155,155,155)',
        name='low',
    ))
    if len(close_data) > 3:
        p = np.polynomial.polynomial.polyfit([i for i in range(int(len(close_data)*1))], close_data[:int(len(close_data)*1)], 1)
        ya = np.polynomial.polynomial.polyval( [i for i in range(int(len(close_data)*1))],p)
        fig.add_trace(go.Scatter(
            x=dates[:int(len(close_data)*1)], y=ya,
            line_color='rgb(255,0,0)',
            name=f'line',
        ))
        # ya = np.polyval(p, [i for i in range(int(len(close_data)*2/3),int(len(close_data)))])
        # fig.add_trace(go.Scatter(
        #     x=dates[int(len(close_data)*2/3):], y=ya,
        #     line_color='rgb(0,0,255)',
        #     name=f'line {p}',
        # ))
    fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_layout(title_text=title)
    fig.update_xaxes(showticklabels=False)
    g = dcc.Graph(id="graph", figure=fig)
    return html.Div([g])

def createChancePlotFromCandle(analyzer,candles,title = None):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    open_data_p = []
    high_data_p = []
    low_data_p = []
    close_data_p = []
    dates = []

    for candle in candles:
        open_data.append(candle.o)
        high_data.append(candle.h)
        low_data.append(candle.l)
        close_data.append(candle.c)
        open_data_p.append((candle.o - candle.o) / candle.o * 100)
        high_data_p.append((candle.h - candle.o) / candle.o * 100)
        low_data_p.append((candle.l - candle.o) / candle.o * 100)
        close_data_p.append((candle.c - candle.o) / candle.o * 100)
        dates.append(candle.time)

    close_dataPrecent = [(candl.c - candl.o) / candl.o * 100 for candl in candles]
    trend = [0 for i in range(15)]
    for data in myMath.roll(close_data, 15):
        #trend.append(np.polynomial.polynomial.polyfit([i for i in range(15)],data, 1)[1])
        trend.append(np.gradient(data)[-1])

    trend_trend = [0 for i in range(3)]
    for data in myMath.roll(trend, 3):
        #trend_trend.append(np.polynomial.polynomial.polyfit([i for i in range(3)],data, 1)[1])
        trend_trend.append(np.gradient(data)[-1])

    chance = analyzer.getPositiveCloseChance(trend_trend)
    next_chance = [chance for i in range(10)]
    for data in myMath.roll(trend_trend,10):
        next_chance.append(analyzer.getPositiveNextChance(data, chance)-0.5)

    fig = plotly.subplots.make_subplots(specs=[[{"secondary_y": True}]])#go.Figure()#
    t = 40
    t2=95+3
    fig.add_trace(go.Scatter(
        x=dates, y=myMath.getSSA(close_data[:-t2],0.5,t),
        line_color='rgb(255,0,0)',
        name='ssa'),secondary_y=False)
    fig.add_trace(go.Scatter(
        x=dates, y=close_data,
        line_color='rgb(155,155,155)',
        name='present'),secondary_y=False)
    fig.add_trace(go.Scatter(
        x=dates, y=close_data[:-t2],
        line_color='rgb(0,255,0)',
        name='present'),secondary_y=False)

    macad = myIndicators.MyMACD()
    gyst,sig0,sig1 = macad.getFromArray(close_data)

    RSV, K, D, J, kdj_gyst = myIndicators.MyKDJ().getFromArray(candles)
    if False:
        fig.add_trace(go.Scatter(
            x=dates, y=gyst,
            line_color='rgb(0,255,0)',
            name='trend'),secondary_y=True)
        fig.add_trace(go.Scatter(
            x=dates, y=myMath.getSSA(gyst[:-t2],0.5,t),
            line_color='rgb(255,0,0)',
            name='trend'),secondary_y=True)
    if True:
        fig.add_trace(go.Scatter(
            x=dates, y=kdj_gyst.tolist(),
            line_color='rgb(0,255,0)',
            name='trend'),secondary_y=True)
        fig.add_trace(go.Scatter(
            x=dates, y=myMath.getSSA(kdj_gyst.tolist()[:-t2],0.5,t),
            line_color='rgb(255,0,0)',
            name='trend'),secondary_y=True)
    # fig.add_trace(go.Scatter(
    #     x=dates, y=trend,
    #     line_color='rgb(255,255,155)',
    #     name='trend'),secondary_y=True)
    # fig.add_trace(go.Scatter(
    #     x=dates, y=trend_trend,
    #     line_color='rgb(155,155,255)',
    #     name='trend_trend'),secondary_y=True)
    # fig.add_trace(go.Scatter(
    #     x=dates, y=np.gradient(close_data),
    #     line_color='rgb(155,155,255)',
    #     name='grad'),secondary_y=True)
    # fig.add_trace(go.Scatter(
    #     x=dates, y=np.gradient(np.gradient(close_data)),
    #     line_color='rgb(155,155,255)',
    #     name='grad2'),secondary_y=True)

    # fig.add_trace(    go.Candlestick(
    #     x=dates,
    #     open=open_data_p,
    #     high=high_data_p,
    #     low=low_data_p,
    #     close=close_data_p),secondary_y=False)
    # fig.add_trace(go.Scatter(
    #     x=dates, y=next_chance,
    #     line_color='rgb(0,0,0)',
    #     name='chance'), secondary_y=True)




    #fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_layout(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_layout(title_text=title)
    fig.update_xaxes(showticklabels=False)
    g = dcc.Graph(id="graph", figure=fig)
    return html.Div([g])

def macadFromCandle(candles):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    dates = []

    for candle in candles:
        #open_data.append(candle.o)
        #high_data.append(candle.h)
        #low_data.append(candle.l)
        close_data.append(candle.c)
        dates.append(candle.time)

    macad = myIndicators.MyMACD()

    gyst,sig0,sig1 = macad.getFromArray(close_data)

    fig = go.Figure()

    fig.add_trace(go.Bar(
        x=dates, y=gyst,
        marker_color='rgb(0,0,0)',
        name='gyst',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=sig0,
        line_color='rgb(255,0,0)',
        name='sig0',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=sig1,
        line_color='rgb(0,0,255)',
        name='sig1',
    ))
    fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_xaxes(showticklabels=False)
    fig.update_layout(title_text='MACD')
    g = dcc.Graph(id="graph", figure=fig)
    return html.Div([g])

def kdjFromCandle(candles):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    dates = []

    for candle in candles:
        #open_data.append(candle.o)
        #high_data.append(candle.h)
        #low_data.append(candle.l)
        close_data.append(candle.c)
        dates.append(candle.time)

    kdj = myIndicators.MyKDJ()

    RSV,K,D,J,gyst = kdj.getFromArray(candles)

    fig = go.Figure()

    fig.add_trace(go.Bar(
        x=dates, y=gyst,
        marker_color='rgb(0,0,0)',
        name='gyst',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=K,
        line_color='rgb(255,0,0)',
        name='K',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=D,
        line_color='rgb(0,0,255)',
        name='D',
    ))
    fig.add_trace(go.Scatter(
        x=dates, y=J,
        line_color='rgb(0,255,0)',
        name='J',
    ))
    fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_xaxes(showticklabels=False)
    fig.update_layout(title_text='KDJ')
    g = dcc.Graph(id="graph", figure=fig)
    return html.Div([g])

from scipy.optimize import curve_fit
def tsiFromCandle(candles):
    open_data = []
    high_data = []
    low_data = []
    close_data = []
    dates = []

    for candle in candles:
        #open_data.append(candle.o)
        #high_data.append(candle.h)
        #low_data.append(candle.l)
        close_data.append(candle.c)
        dates.append(candle.time)

    TSI = myIndicators.MyTSI()

    tsi = TSI.getFromArray(candles)

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=dates, y=tsi,
        line_color='rgb(0,0,0)',
        name='tsi',
    ))
    if 1:
        def func(x,A0,w0,phi0):
            return A0*np.sin((np.pi+phi0)/w0*x)#+A1*np.sin(np.pi/w1*x+phi1)


        candleSeconds = {
            CandleResolution.MIN_1.value: 60,
            CandleResolution.MIN_2.value: 60 * 2,
            CandleResolution.MIN_3.value: 60 * 3,
            CandleResolution.MIN_5.value: 60 * 5,
            CandleResolution.MIN_10.value: 60 * 10,
            CandleResolution.MIN_15.value: 60 * 15,
            CandleResolution.MIN_30.value: 60 * 30,
            CandleResolution.HOUR.value: 60 * 60,
            CandleResolution.DAY.value: 60 * 60 * 24,
            CandleResolution.WEEK.value: 60 * 60 * 24 * 7,
            CandleResolution.MONTH.value: 60 * 60 * 24 * 30,

        }
        #secondsCount = candleSeconds[candles[0].interval.value]
        #time = [int(candle.time.timestamp() / secondsCount) for candle in candles]
        # time = [i for i in range(len(candles))]
        # time = time[40:]
        # y = tsi[40:]
        # popt, pcov = curve_fit(func, time, tsi,p0=[1,len(y)/10,0],bounds=([0.1,len(y)/10000,0],[1,len(y)/2,len(y)]))
        # tsi_func = [func(x,*popt) for x in time]
        # print(popt)
        def getifft(y,time):
            fft = np.fft.fft(y)
            freq = np.fft.fftfreq(len(y), d=1)
            magnitude = np.abs(fft)
            phase = np.angle(fft)
            #fft = {'freq':freq,'magnitude':magnitude,'phase':phase}
            #fft = sorted(fft,key='freq')
            fft = list(zip(*sorted(zip(magnitude,freq, phase))))

            def fromfft(fft,deg,x):
                y = 0
                for f in fft[-deg:]:
                    y+=f[0]*np.sin(f[1]*x+f[2])
                return y
            result = []
            for t in time:
                result.append(fromfft(fft,1,t))
            return result

        #tsi_func = getifft(tsi,time)

        # fig.add_trace(go.Scatter(
        #     x=dates, y=tsi_func,
        #     line_color='rgb(255,0,0)',
        #     name='tsi_sin',
        # ))

    fig.layout = dict(xaxis=dict(type="category", categoryorder='category ascending'))
    fig.update_xaxes(showticklabels=False)
    fig.update_layout(title_text='TSI')
    g = dcc.Graph(id="graph", figure=fig)
    return html.Div([g])

def requestCandleWidget(name, stocks, headName = None, showInput = False):
    if headName is None:
        headName = 'Stock'
    inputFigi = html.P('')
    if showInput:
        inputFigi = dbc.Input(id= name+"_input", placeholder="Search figi", type="text")
    return dbc.Row([
    html.P(f'{headName}:'),
    inputFigi,
    dbc.Select(
        id= name+"_stock",
        options=[{'label': stock.name, 'value': stock.figi} for stock in stocks],
    ),
    html.P('Interval:'),
    dbc.Input(id= name+'_interval', type='number', min=0, max=2000, step=1, value=30),
    dbc.Select(
        id= name+"_resolution",
        options=[{'label': str(res), 'value': res.value} for res in CandleResolution],
        value=CandleResolution.HOUR.value
    ),
    dbc.Select(
        id= name+"_source",
        options=[{'label': 'Tinkoff', 'value': 'tinko'}, {'label': 'Local DB', 'value': 'tinko_db'}],
        value='tinko'
    ),
    dbc.Button( "Show", id=name+"_show", block=True),
    ],style=BLOCK_STYLE)

def requestFreeCurrencyWidget(name):
    return dbc.Row([
    html.P('Currency hold free:'),
    dbc.Input(id= name+'_value', type='number', min=0, max=9999999, step=1, value=0),
    dbc.Select(
        id= name+"_currency",
        options=[{'label': currency.value, 'value': currency.value} for currency in Currency],
        value=Currency.USD.value
    ),
    dbc.Button( "Apply", id=name+"_apply", block=True),
    ],style=BLOCK_STYLE)



def requestFavoriteWidget(app,tinkoDB,name,favorites):
    a =[{'label': str(favor['description']), 'value': favor['figi']} for favor in favorites]
    w = dbc.Row([

    dbc.Checklist(
        id= name+"_check",
        options=[{'label': str(favor['description']), 'value':
            favor['figi']} for favor in favorites],
        value= [favor['figi'] for favor in favorites]
    ),
    #dbc.Button( "Reload", id=name+"_reload"),
    ],style=BLOCK_STYLE)

    #callbackList.append(render_page_content)

    return w
import dash_table
def requestPortfolioTable(portfolio,tinkoDB,):

    def getPrice(figi):
        price = 0
        info = tinkoDB.blockingCall(tinkoDB.getLastTradeInfo, figi)
        if info is not None:
            price = info.currentPrice
            return price
    def getDelta(figi):
        price = 0
        info = tinkoDB.blockingCall(tinkoDB.getLastTradeInfo, figi)
        if info is not None:
            price = info.currentPrice - info.price
        return price
    def getDeltaPrecent(figi):
        price = 0
        info = tinkoDB.blockingCall(tinkoDB.getLastTradeInfo, figi)
        if info is not None:
            price = 100.0*(info.currentPrice - info.price)/info.price
        return price
    def getCurrency(figi):
        currency = ''
        info = tinkoDB.blockingCall(tinkoDB.getInfo, figi)
        if info is not None:
            currency = info.currency.value
        return currency

    df = pd.DataFrame(
        {
            "Name": [stock.name for stock in portfolio],
            "Figi": [stock.figi for stock in portfolio],
            "Price": [getPrice(stock.figi) for stock in portfolio],
            "Currency": [getCurrency(stock.figi) for stock in portfolio],
            "Delta": [getDelta(stock.figi) for stock in portfolio],
            "DeltaPrecent": [getDeltaPrecent(stock.figi) for stock in portfolio],
            "Balance": [stock.balance for stock in portfolio],
            "Lots": [stock.lots for stock in portfolio],
            "Blocked": [stock.blocked for stock in portfolio],
            "expectedYield": [ str(stock.expectedYield) for stock in portfolio],
            "averagePositionPrice": [str(stock.averagePositionPrice) for stock in portfolio],
            "averagePositionPriceNoNkd": [str(stock.averagePositionPriceNoNkd) for stock in portfolio],
        }
    )

    balance = tinkoDB.blockingCall(tinkoDB.getBalance)
    balance = pd.DataFrame(
        {
            "Currency": balance.keys(),
            "Balance": balance.values()
        },index=None)

    return dbc.Row([ html.H3(f'Total:'),
                     dbc.Table.from_dataframe(balance, striped=True, bordered=True, hover=True),
                     createCallbackableTable('portfolio',df)],style=BLOCK_STYLE)

def requestCurrenciesTable(currencies):
    df = pd.DataFrame(
        {
            "Currency": [str(stock.currency) for stock in currencies],
            "Balance": [stock.balance for stock in currencies],
            "Blocked": [str(stock.blocked) for stock in currencies]
            # "Name": [stock.name for stock in currencies],
            # "Figi": [stock.figi for stock in currencies],
            # "Lot": [stock.lot for stock in currencies],
            # "ticker": [stock.ticker for stock in currencies],
            # "minPriceIncrement": [str(stock.minPriceIncrement) for stock in currencies],
            # "minQuantity": [str(stock.minQuantity) for stock in currencies],
            # "isin": [str(stock.isin) for stock in currencies],
            # "currency": [str(stock.currency) for stock in currencies],
            # "type": [str(stock.type) for stock in currencies],
        }
    )
    return dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)

def requestAnalyzeTable(df):
    return createCallbackableTable('analyze',df)

def requestLogTable():
    df = pd.DataFrame(list(reversed(logList)))
    return createCallbackableTable('logList',df,page_size=50)