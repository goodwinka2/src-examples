import logging
from logging.handlers import TimedRotatingFileHandler
import os
from enum import Enum
from apscheduler.schedulers.background import BackgroundScheduler
from pytz import utc
# CRITICAL = 50
# FATAL = CRITICAL
# ERROR = 40
# WARNING = 30
# WARN = WARNING
# INFO = 20
# DEBUG = 10
# NOTSET = 0
import telebot
import datetime
import config
bot = telebot.TeleBot(config.telegramBotId);

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    print(message)

ignoreList = ['sqlitedict','proactor_events','apscheduler', 'base','connectionpool']
telegram_id = config.telegramChatId
#bot.send_message(chat_id=telegram_id,text= "Start telebot")
#bot.polling()

logList = []

class MyHandler(logging.StreamHandler):
    def __init__(self,stream = None):
        super(MyHandler, self).__init__(stream)

        self.messageAdapters = {
            'CRITICAL' : lambda val: {bot.send_message(chat_id=telegram_id,text= val)} ,
            'FATAL' : lambda val: {bot.send_message(chat_id=telegram_id,text= val)},
            'ERROR' : lambda val: {bot.send_message(chat_id=telegram_id,text= val)},
            'WARNING' : lambda val: {bot.send_message(chat_id=telegram_id,text= val)},
            'WARN' : lambda val: {bot.send_message(chat_id=telegram_id,text= val)},
            'INFO' : lambda val: {bot.send_message(chat_id=telegram_id,text= val,disable_notification=True)},
            'DEBUG' : lambda val: {},
            'NOTSET' : lambda val: {}
        }
        pass
    def emit(self, record):

        #if record.module in ignoreList:
        #    return
        msg= self.format(record)
        print(msg)
        logList.append({
            'name':len(logList),
            'level':record.levelname,
            'time':self.formatter.formatTime(record),
            'msg' :record.getMessage(),
            'module':record.module})
        msg = f'{logList[-1]["level"]} \t {logList[-1]["time"]}\n{logList[-1]["msg"]}'
        if config.useTelegram:
            self.messageAdapters.get(record.levelname,lambda val: {print('uncknown lvl')})(msg)

        pass


myHandler = MyHandler()
myHandler.addFilter(lambda record: not (record.module in ignoreList))

def newLog():
    global logList
    logList.clear()
    startedTime = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    if not os.path.exists('logs'):
        os.mkdir('logs')
    fileHandler = logging.FileHandler(mode='w', filename=f"logs/log_{startedTime}.txt")
    logging.basicConfig(
        level=logging.DEBUG,
        # filename = "log.txt",
        format="%(asctime)s <%(levelname)9s>:: %(message)s\t\t::{%(lineno)d: %(module)s - %(funcName)s}",
        datefmt=None,  # '%Y-%m-%d %H:%M:%S',
        handlers=[myHandler, fileHandler],
        force=True
    )
newLog()

scheduler = BackgroundScheduler( timezone=utc)
scheduler.add_job(newLog, 'cron', hour='7',minute='0')
scheduler.start()

logging.debug('Start logger')
logging.getLogger('sqlitedict').setLevel('ERROR')

# import time
# time.sleep(30)
