import copy

from tinkoffthread import TinkoffThread
from tinko_db import TinkoffDB
from tinko_db_updater import TinkoffDBUpdater
import plotly.graph_objects as go
from datetime import datetime, timedelta, timezone
import dash_fabric as df
from myAnalyzer import CandleAnalyzer
from myTrader import TinkoffTrader
import pandas as pd
from forbiddenfruit import curse
from functools import singledispatch

from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    Currency,
)
from apscheduler.schedulers.background import BackgroundScheduler
from pytz import utc
import myLogger
import logging
import config
class TradeController():
    def __init__(self,useProduction = False):

        self.tinko = TinkoffThread()
        self.tinko.start()
        if useProduction:
            self.tinko.blockingCall(self.tinko.initProduction)
        else:
            self.tinko.blockingCall(self.tinko.initSandbox,reinitialize = config.reinitSand)



        self.tinkoDB = TinkoffDB(self.tinko)
        if config.clearDb:
            self.tinkoDB.clear()
        self.tinkoDB.start()
        if 0:
            self.tinkoDB.clearLastTradeInfo()
        self.candleAnalyzer = CandleAnalyzer(self.tinkoDB,self.tinko)

        self.tinkoUpdater = TinkoffDBUpdater(self.tinkoDB,self.tinko)
        self.candleAnalyzer.setTinkoUpdater(self.tinkoUpdater)
        self.tinkoUpdater.start()

        self.tinkoTrader = TinkoffTrader(self.tinkoDB,self.tinko)
        self.candleAnalyzer.start()
        self.tinkoTrader.setAnalyzer(self.candleAnalyzer)
        self.tinkoTrader.start()

        if config.loadDataForSimulation:
            self.startLoadData()
            return

        if config.startTrade:
            if not config.useSimulation:
                if config.updateOnStart:
                    self.tinkoUpdater.blockingCall(self.tinkoUpdater.updateAll)
                self.tinkoUpdater.blockingCall(self.tinkoUpdater.startUpdater)
                self.candleAnalyzer.blockingCall(self.candleAnalyzer.startAnalyzeAll)
                self.tinkoTrader.blockingCall(self.tinkoTrader.tryTrade)

                self.scheduler = BackgroundScheduler(timezone=utc)
                self.scheduler.add_job(lambda: {self.startDay()}, 'cron',hour ='6', minute='10')
                self.scheduler.add_job(lambda: {self.startHour()},'cron',hour='7-23/1',minute='15,45',second='0' )

                #self.scheduler.add_job(lambda: {logging.critical('=====shedul!======')}, 'cron',minute='0-59/1', second='10,20,40')

                self.scheduler.start()
            else:
                self.startSimulation()
        else:
            startTime = datetime(year=2021, month=2, day=1, tzinfo=timezone.utc)  # datetime.now(timezone.utc)
            endTime = startTime + timedelta(days=config.simulationDays + 3)
            self.currentTime = copy.deepcopy(endTime)

            def nextday(time):
                return (time + timedelta(days=1)).replace(hour=7, minute=0, second=0)


            curse(datetime, "now", self.now)

    def now(self,*args, **kwargs):
        return self.currentTime

    def startHour(self):
        #self.candleAnalyzer.blockingCall(self.candleAnalyzer.startAnalyzeAll)
        self.tinkoTrader.blockingCall(self.tinkoTrader.tryTrade)

    def startDay(self):
        self.tinkoTrader.blockingCall(self.tinkoTrader.saveTrades)
        self.tinkoUpdater.blockingCall(self.tinkoUpdater.updateAll)
        self.candleAnalyzer.blockingCall(self.candleAnalyzer.startAnalyzeAll)

    def startSimulation(self):
        if 0:
            endTime = datetime(year=2021,month=8,day=1,tzinfo=timezone.utc)#datetime.now(timezone.utc)
            startTime = endTime-timedelta(days=config.simulationDays)
        else:
            startTime = datetime(year=2021, month=2, day=1, tzinfo=timezone.utc)  # datetime.now(timezone.utc)# 1 16 23
            endTime = startTime + timedelta(days=config.simulationDays)
        currentTime = copy.deepcopy(startTime)
        def nextday(time):
            return (time + timedelta(days=1)).replace(hour=7,minute=0,second=0)


        def now(*args,**kwargs):
            return currentTime
        curse(datetime,"now",now)

        self.tinkoDB.clear()
        import shutil
        shutil.copy('./tinkoff_db_simulation.sqlite', self.tinkoDB.dbName)


        report = []

        for days in range(config.simulationDays):

            self.tinkoUpdater.blockingCall(self.tinkoUpdater.updateAll)
            self.candleAnalyzer.blockingCall(self.candleAnalyzer.startAnalyzeAll)

            for step in range(7,23):
                currentTime = currentTime.replace(hour=step, minute=0, second=0)
                self.tinkoTrader.blockingCall(self.tinkoTrader.tryTrade)

                balance = self.tinkoDB.blockingCall(self.tinkoDB.getBalance)
                balance['time'] = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
                report.append(balance)
                #self.tinkoTrader.blockingCall(self.tinkoTrader.saveTrades)

            logging.debug(datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))

            currentTime = nextday(currentTime)

        self.tinkoTrader.blockingCall(self.tinkoTrader.saveTrades)
        with pd.ExcelWriter(f'analyze_report/simulation_{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.xlsx') as writer:
            pd.DataFrame(report).to_excel(writer, sheet_name='all')

    def startLoadData(self):

        startTime = datetime(year=2021, month=2, day=1, tzinfo=timezone.utc)  # datetime.now(timezone.utc)
        endTime = startTime + timedelta(days=config.simulationDays+3)
        currentTime = copy.deepcopy(endTime)

        def nextday(time):
            return (time + timedelta(days=1)).replace(hour=7,minute=0,second=0)


        def now(*args,**kwargs):
            return currentTime
        curse(datetime,"now",now)

        self.tinkoDB.clear()
        #import shutil
        #shutil.copy('./tinkoff_db_simulation.sqlite', self.tinkoDB.dbName)


        for i, stock in enumerate(self.tinko.stocks):
            self.tinkoDB.blockingCall(self.tinkoDB.updateCandles,
                                      figi=stock.figi,
                                      interval=CandleResolution.DAY,
                                      days=360*4+30,
                                      hours=0,
                                      minutes=0)
            self.tinkoDB.blockingCall(self.tinkoDB.updateCandles,
                                      figi=stock.figi,
                                      interval=CandleResolution.HOUR,
                                      days=90,
                                      hours=0,
                                      minutes=0)
            logging.debug(f'ready {i} from {len(self.tinko.stocks)}')

        self.tinkoDB.blockingCall(self.tinkoDB.setData, 'updateTime', datetime.now())


