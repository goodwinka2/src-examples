import logging
import queue
import threading
import concurrent.futures
import asyncio
import traceback
import time

executor = concurrent.futures.ThreadPoolExecutor()
run = executor.submit






class SimpleThread(threading.Thread):
    def __init__(self, q = None, loop_time = 10.0):
        threading.Thread.__init__(self)
        self.q = queue.Queue()
        self.timeout = loop_time
        self.doRun = True
        self.doExceptionLoop = True
        self.exceptionLoopFunctions = []
        self.loopCounter = 0

    def get_or_create_eventloop(self):
        try:
            return asyncio.get_event_loop()
        except RuntimeError as ex:
            if "There is no current event loop in thread" in str(ex):
                print('ERR: There is no current event loop in thread')
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                return asyncio.get_event_loop()

    def quequedCall(self, function, *args, **kwargs):
        self.q.put({'function': function, 'args': args, 'kwargs': kwargs, 'outQueue': None, 'callback': None})

    def blockingCall(self, function, *args, **kwargs):
        qb = queue.Queue()
        self.q.put({'function': function, 'args': args, 'kwargs': kwargs, 'outQueue': qb, 'callback': None})
        return qb.get()

    def callbackCall(self, function,callback, *args, **kwargs):
        self.q.put({'function': function, 'args': args, 'kwargs': kwargs, 'outQueue': None, 'callback': callback})


    # def stopSimpleThread(self):
    #     self.doRun = False
    #     a = asyncio.all_tasks(get_or_create_eventloop())
    #
    #     for task in asyncio.all_tasks(get_or_create_eventloop()):
    #         task.result()
    #         asyncio.wait_for(task)
    #     print('1')
    #     get_or_create_eventloop().stop()
    #     #get_or_create_eventloop().close()
    #     self.join()
    #     print('2')

    def run(self):
        asyncio.run(self.loop())
        #self.get_or_create_eventloop().run_until_complete(self.loop())

    async def loop(self):
        #self.get_or_create_eventloop()
        while self.doRun:
            try:
                q = self.q.get(timeout=None)
                self.doExceptionLoop = True
                self.loopCounter = 0
                #print(self.getName() + self.__str__() + ' ==============================================='  + str(q['function']))
                while self.doExceptionLoop:
                    self.doExceptionLoop = q['function'] in self.exceptionLoopFunctions
                    try:
                        if asyncio.iscoroutinefunction(q['function']):
                            # r = asyncio.create_task(q['function'](*q['args'], **q['kwargs']))
                            # r = await r
                            #r = self.get_or_create_eventloop().run_until_complete(q['function'](*q['args'], **q['kwargs']))
                            r = await q['function'](*q['args'], **q['kwargs'])


                        else:
                            r = q['function'](*q['args'], **q['kwargs'])
                        self.doExceptionLoop = False
                    except Exception as e:
                        self.loopCounter+=1
                        logging.critical(f'EXCEPTION << {e.__str__()} >>\nfunc: {str(q["function"])}\nargs: {str(q["args"])}\nkeys: {str(q["kwargs"])}')
                        logging.debug(f'EXCEPTION << {e.__str__()} >>:\n{traceback.format_exc()}')
                        if self.doExceptionLoop:
                            time.sleep(30+5*60*(1 if self.loopCounter>5 else 0))
                        r = None
                if q['callback'] is not None:
                    q['callback'](r)
                if q['outQueue'] is not None:
                    q['outQueue'].put(r)


            except queue.Empty:
                self.idle()
            except Exception as e:
                logging.error(traceback.format_exc())


    def idle(self):
        time.sleep(1)
        # put the code you would have put in the `run` loop here
        pass
