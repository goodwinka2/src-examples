import logging

from tinko_db import TinkoffDB
import simplethread
import logging as log
from threading import Timer
from dataclasses import dataclass
from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    Currency
)
from pytz import utc
from datetime import datetime, timedelta, timezone
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import codetiming
t = codetiming.Timer(name="class",logger=log.debug)

@dataclass
class Config:
    days : 1
    hours : 0
    minutes : 0
    resolution : CandleResolution.HOUR




class TinkoffDBUpdater(simplethread.SimpleThread):
    def __init__(self,tinkoDb,tinko):
        simplethread.SimpleThread.__init__(self)
        self.tinkoDb = tinkoDb
        self.tinko = tinko
        self.configFavorite = Config(
            days = 30,
            hours = 0,
            minutes = 0,
            resolution = CandleResolution.HOUR)

        self.configAll = Config(
            days = 360*4,
            hours = 0,
            minutes = 0,
            resolution = CandleResolution.DAY)

        #Timer()



    def startUpdater(self):
        self.scheduler = BackgroundScheduler( timezone=utc)
        #self.scheduler = BlockingScheduler(timezone=utc)
        #logging.getLogger('apscheduler.executors.default').setLevel(logging.INFO)
        #self.scheduler.add_job(lambda: {self.blockingCall(self.updateAll)}, 'cron',hour ='7', minute='10')
        #self.scheduler.add_job(lambda:{self.blockingCall(self.updateFavorite)},'cron',hour='8-23',minute='*/30',second='59' )

        #self.scheduler.add_job(lambda: {logging.info('shedul!')}, 'cron', second='*/7')

        self.scheduler.start()
        #self.updateAll()
        #self.updateAllInfo()
        #self.updateFavorite()
        pass

    def updateAll(self):
        t.start()
        self.tinko.blockingCall(self.tinko.get_stocks)
        t.stop()
        t.start()
        updateTime = self.tinkoDb.blockingCall(self.tinkoDb.getData, 'updateTime')
        if updateTime is None:
            updateTime = datetime.now()
            updateTime= updateTime.replace(year=2000)
        if datetime.now() - timedelta(days=0, hours=12, minutes=0) > updateTime:
            for i,stock in enumerate(self.tinko.stocks):
                self.tinkoDb.blockingCall(self.tinkoDb.updateCandles,
                                          figi=stock.figi,
                                          interval = self.configAll.resolution,
                                          days=self.configAll.days,
                                          hours=self.configAll.hours,
                                          minutes=self.configAll.minutes)
                log.debug(f'ready {i} from {len(self.tinko.stocks)}')
            self.tinkoDb.blockingCall(self.tinkoDb.setData, 'updateTime',datetime.now())
        else:
            pass
        t.stop()

    def updateAllInfo(self):
        t.start()
        self.tinko.blockingCall(self.tinko.get_stocks)
        t.stop()
        t.start()
        for i,stock in enumerate(self.tinko.stocks):
            self.tinkoDb.blockingCall(self.tinkoDb.updateInfo, stock.figi)
            log.debug(f'ready {i} from {len(self.tinko.stocks)}')
        t.stop()

    def updateFavorite(self):
        t.start()
        favorite = self.tinkoDb.blockingCall(self.tinkoDb.getFavorite)
        t.stop()
        t.start()
        for figi in favorite:
            self.tinkoDb.blockingCall(self.tinkoDb.updateCandles,
                                      figi=figi,
                                      interval = self.configFavorite.resolution,
                                      days=self.configFavorite.days,
                                      hours=self.configFavorite.hours,
                                      minutes=self.configFavorite.minutes)
        t.stop()
