
def main():
    import logging

    import dash
    import dash_bootstrap_components as dbc
    import dash_core_components as dcc
    import dash_html_components as html
    from dash.dependencies import Input, Output, State
    from dash_extensions.enrich import DashProxy, TriggerTransform, MultiplexerTransform, ServersideOutputTransform, NoOutputTransform
    import zipfile
    import os
    import config

    app = DashProxy(external_stylesheets=[dbc.themes.BOOTSTRAP],transforms=[
        TriggerTransform(),  # enable use of Trigger objects
        MultiplexerTransform(),  # makes it possible to target an output multiple times in callbacks
        ServersideOutputTransform(),  # enable use of ServersideOutput objects
        NoOutputTransform(),  # enable callbacks without output
    ])
    app.config['suppress_callback_exceptions']=True

    from tinkoffthread import TinkoffThread
    from tinko_db import TinkoffDB
    from tinko_db_updater import TinkoffDBUpdater
    import plotly.graph_objects as go
    from datetime import datetime, timedelta
    import dash_fabric as df
    from myAnalyzer import CandleAnalyzer
    from myTrader import TinkoffTrader

    from tinkoff.investments import (
        CandleResolution,
        Environment,
        TinkoffInvestmentsRESTClient,
        Currency,
    )

    import myLogger
    from myTradeController import TradeController
    import config
    tradeController = TradeController(useProduction=config.useProduction)
    #exit(0)
    sidebar = html.Div(
        [
            html.H2("My Tincoff api viewer", className="display-7"),
            html.Hr(),
            html.P(
                "Instruments", className="lead"
            ),
            # dbc.Nav(
            #     [
            #         dbc.NavLink("Home", href="/", active="exact"),
            #         dbc.NavLink("Page 1", href="/page-1", active="exact"),
            #         dbc.NavLink("Page 2", href="/page-2", active="exact"),
            #     ],
            #     vertical=True,
            #     pills=True,
            # ),
            html.Div([dbc.Button("Download sqlite", id="btn_sqlite", block=True), dcc.Download(id="download-sqlite")]),
            html.Div([dbc.Button("Download logs", id="btn_logs", block=True), dcc.Download(id="download-logs")]),
            #html.Div([dbc.Button("Panic sell", id="btn_sellAll", block=True)]),
            dcc.ConfirmDialogProvider(
                    children=html.Button(
                        'Panic sell',
                    ),
                    id='btn_sellAll',
                    message='Danger panic sell! Are you sure you want to continue?'
                ),
            df.requestCandleWidget('global', tradeController.tinko.stocks, 'Stocks', True),
            df.requestCandleWidget('currencies',  tradeController.tinko.currencies, 'Currencies'),
            df.requestFreeCurrencyWidget('freeCurrency')
        ],
        style={    #"width": "16rem",
        "padding": "1rem 2rem",
        "background-color": "#fca311",
    }
    )

    #content = html.Div(id="page-stock")
    @app.callback(
        Input("btn_sellAll", "submit_n_clicks"),
        prevent_initial_call=True,
    )
    def func(submit_n_clicks):
        if submit_n_clicks:
            tradeController.tinkoTrader.blockingCall(tradeController.tinkoTrader.trySell,force=True)

    @app.callback(
        Output("download-sqlite", "data"),
        Input("btn_sqlite", "n_clicks"),
        prevent_initial_call=True,
    )
    def func(n_clicks):
        return dcc.send_file(
            "./tinkoff_db.sqlite"
        )


    @app.callback(
        Output("download-logs", "data"),
        Input("btn_logs", "n_clicks"),
        prevent_initial_call=True,
    )
    def func(n_clicks):
        archive = zipfile.ZipFile('logs.zip', mode='w')

        def zipdir(path, ziph):
            # ziph is zipfile handle
            for root, dirs, files in os.walk(path):
                for file in files:
                    ziph.write(os.path.join(root, file),
                               os.path.relpath(os.path.join(root, file),
                                               os.path.join(path, '..')))
        zipdir('./analyze_report',archive)
        zipdir('./logs', archive)
        archive.close()
        return dcc.send_file(
            "./logs.zip"
        )

    app.layout = html.Div(
        [
            dbc.Row(
                [
                    dbc.Col(sidebar,width=2),
                    dbc.Col(dbc.Tabs([
                        dbc.Tab(html.Div(id="page-stock"),tab_id = "page-stock",label='Stock',tab_style ={"color": "#000000"}),
                        dbc.Tab(html.Div(id="page-currencies"),tab_id = "page-currencies",label='Currencies',tab_style ={"color": "#000000"}),
                        dbc.Tab([dbc.Button( "Refresh", id="portfolio_refresh", block=True),html.Div(id="page-portfolio")]
                                ,label='Portfolio',tab_style ={"color": "#000000"}),
                        dbc.Tab([dbc.Button("Refresh and apply", id="favorite_refresh", block=True),
                                 html.Div(id="page-favorite")]
                                , label='Favorite', tab_style={"color": "#000000"}),
                        dbc.Tab([dbc.Button("Refresh and apply", id="analyze_refresh", block=True),
                                 html.Div(id="page-analyze")]
                                , label='Analyze', tab_style={"color": "#000000"}),
                        dbc.Tab([dbc.Button("Refresh and apply", id="log_refresh", block=True),
                                 html.Div(id="page-log")]
                                , label='Log', tab_style={"color": "#000000"}),
                    ],id = 'contentTab'),
                        width=10,
                        style={    #"width": "16rem",
                        "padding": "1rem 2rem",
                        "background-color": "#14213d",
                        },
                        ),
                ],

            ),
        ]
    )



    @app.callback(Output("contentTab", "active_tab"), [Input("page-stock", "children"),Input("page-currencies", "children")]
                  ,prevent_initial_call=True)
    def set_changeTab(value0,value1):
        ctx = dash.callback_context
        if not ctx.triggered:
            input_id = 'No clicks yet'
        else:
            input_id = ctx.triggered[0]['prop_id'].split('.')[0]
            return input_id

        return "page-stock"

    @app.callback(Output("global_stock", "value"), [Input("global_input", "value")])
    def set_globalstock(value):
        if value is not None:
            if any(stock.figi in value for stock in  tradeController.tinko.stocks):
                return value

        return  tradeController.tinko.stocks[0].figi


    @app.callback(Output("page-stock", "children"), [
        Input("global_stock", "value"),
        Input("global_show","n_clicks"),
        State("global_interval", "value"),
        State("global_resolution", "value"),
        State("global_source", "value")]
    #,prevent_initial_call=True
                  )
    def render_page_stock(value,but,interval,resolution,source):
        if value is None:
            return html.P(f"wait")
        info =  tradeController.tinko.blockingCall( tradeController.tinko.get_stock_info,value)
        candles = []
        if source == 'tinko':
            candles =  tradeController.tinko.blockingCall( tradeController.tinko.get_candles,value, datetime.now() - timedelta(days=interval), datetime.now(), CandleResolution(resolution))

        if source == 'tinko_db':
            candles =  tradeController.tinkoDB.blockingCall( tradeController.tinkoDB.getActualCandles,value,interval,CandleResolution(resolution))

        buttonValue = 0
        buttonName = ''


        favorite =  tradeController.tinkoDB.blockingCall( tradeController.tinkoDB.getFavorite)
        if value in favorite:
            buttonName = 'Remove from Favorite'
            buttonValue = 'remove'
        else:
            buttonName = 'Add to Favorite'
            buttonValue = 'add'
        favoriteButton = dbc.Button(buttonName, id="favoriteButton", block=True, value=buttonValue)

        return html.Div([
            html.P(str(info)),
            html.P(str(tradeController.tinko.blockingCall( tradeController.tinko.get_orderbook,value))),
            favoriteButton,
            df.createGraph(candles,info.name),
            df.createPrecentCandleGraph(candles,info.name),
            df.createChancePlotFromCandle(tradeController.candleAnalyzer,candles,'chance'),
            df.createLinePlotFromCandle(candles,info.name),
            df.macadFromCandle(candles),
            df.kdjFromCandle(candles),
            df.tsiFromCandle(candles),
        ],style=df.BLOCK_STYLE)

    @app.callback(Output('favoriteButton', "value"),Output('favoriteButton', "children"),
                  Input('favoriteButton', "n_clicks"),
                   State('favoriteButton', "value"),
                   State('favoriteButton', "children"),
                   State("global_stock", "value"),
                  prevent_initial_call=True)
    def render_button(n_clicks,value,name,figi):
        if n_clicks == 0:
            return value,name
        favorite =  tradeController.tinkoDB.blockingCall( tradeController.tinkoDB.getFavorite)
        if value == 'add':
            name = 'Remove from Favorite'
            value = 'remove'
            if figi not in favorite:
                favorite.append(figi)
        else:
            name = 'Add to Favorite'
            value = 'add'
            if figi in favorite:
                favorite.remove(figi)
        tradeController.tinkoDB.blockingCall(tradeController.tinkoDB.setFavorite, favorite)
        return value, name
        #value = value[0]
        #favorite = tinkoDB.blockingCall(tinkoDB.getFavorite)
        #favorite.remove(value)
        #tinkoDB.blockingCall(tinkoDB.setFavorite,value)

    @app.callback(Output("page-currencies", "children"), [
        Input("currencies_stock", "value"),
        Input("currencies_show","n_clicks"),
        State("currencies_interval", "value"),
        State("currencies_resolution", "value")],
                    prevent_initial_call=True
                  )
    def render_page_currencies(value,but,interval,resolution):
        if value is None:
            return html.P(f"wait")
        info = tradeController.tinko.blockingCall(tradeController.tinko.get_stock_info,value)
        candles = tradeController.tinko.blockingCall(tradeController.tinko.get_candles,value, datetime.now() - timedelta(days=interval), datetime.now(), CandleResolution(resolution))
        return html.Div([html.P(str(info)),df.createGraph(candles),df.createLinePlotFromCandle(candles)],style=df.BLOCK_STYLE)

    @app.callback(Output("page-portfolio", "children"), [Input("portfolio_refresh","n_clicks")],prevent_initial_call=False)
    def render_page_portfolio(but):
        tradeController.tinko.blockingCall(tradeController.tinko.get_portfolio)
        tradeController.tinko.blockingCall(tradeController.tinko.get_portfolio_currencies)
        return html.Div([html.P(f'usd hold: {tradeController.tinkoTrader.configUSD.freeCount}'),
                         html.P(f'rub hold: {tradeController.tinkoTrader.configRUB.freeCount}'),
            df.requestCurrenciesTable(tradeController.tinko.portfolio_currencies),
            df.requestPortfolioTable(tradeController.tinko.portfolio,tradeController.tinkoDB)],style=df.BLOCK_STYLE)

    df.createCallbackGraph(app,'portfolio',x='Name')

    @app.callback(Output("page-analyze", "children"), [Input("analyze_refresh","n_clicks")],prevent_initial_call=True)
    def render_page_portfolio(but):

        return html.Div([df.requestAnalyzeTable(tradeController.candleAnalyzer.allData['all'])],style=df.BLOCK_STYLE)

    df.createCallbackGraph(app,'analyze',x='Name')

    @app.callback(Output("page-favorite", "children"), [Input("favorite_refresh","n_clicks")])
    def render_page_content(but):
        favorite = tradeController.tinkoDB.blockingCall(tradeController.tinkoDB.getFavorite)
        #tinko.blockingCall(tinko.get_portfolio)
        #tinko.blockingCall(tinko.get_portfolio_currencies)
        favoriteWidget = df.requestFavoriteWidget(app,tradeController.tinkoDB,'favorite',
                [{'description': tradeController.tinko.blockingCall(tradeController.tinko.get_stock_info,favor), 'figi': favor} for favor in favorite])


        return html.Div([favoriteWidget],style=df.BLOCK_STYLE)

    @app.callback(Output("page-log", "children"), [Input("log_refresh","n_clicks")])
    def render_page_content(but):

        return html.Div([df.requestLogTable()],style=df.BLOCK_STYLE)

    @app.callback([Input('favorite'+"_check", "value")],prevent_initial_call=True)
    def render_page_content(value):
        if value is None:
            return
        #value = value[0]
        #favorite = tinkoDB.blockingCall(tinkoDB.getFavorite)
        #favorite.remove(value)
        print(value)
        tradeController.tinkoDB.blockingCall(tradeController.tinkoDB.setFavorite,value)

    @app.callback([Input('freeCurrency'+"_apply", "n_clicks"),
                   State('freeCurrency'+'_value','value'),
                   State('freeCurrency'+'_currency','value')],prevent_initial_call=True)
    def render_page_currensy(n_clicks,value,currency):
        if value is None:
            return

        print(value)
        if currency == Currency.USD.value:
            tradeController.tinkoTrader.configUSD.freeCount = value
        if currency == Currency.RUB.value:
            tradeController.tinkoTrader.configRUB.freeCount = value

    if config.useDockerNetwork:
        app.run_server(port=8888,debug=True,use_reloader=False,host='0.0.0.0')
    else:
        app.run_server(port=8888, debug=True, use_reloader=False)


if __name__ == "__main__":
    main()