import numpy as np
import copy
import collections

scale = 1

class MyEMA:
  def __init__(self,N = 25*scale):
      self.k = 2 / (N + 1)
      self.N = N
      self.clear()

  def clear(self):
      self.EMA = None

  def update(self,value):
      if self.EMA is None:
          self.EMA = value
      self.EMA = self.k * value + (1 - self.k) * self.EMA
      return self.EMA

  def get(self):
      return self.EMA

  def getFromArray(self, values):
      self.clear()
      EMA = []
      for var in values:
          self.update(var)
          EMA.append(copy.deepcopy(self.EMA))
      return EMA


class MyMACD:

    def __init__(self,N = 26*scale, M = 12*scale, K = 9*scale):
        self.N = N
        self.M = M
        self.K = K
        self.clear()
    def clear(self):
        self.ema0 = MyEMA(self.N)
        self.ema1 = MyEMA(self.M)
        self.ema2 = MyEMA(self.K)
        self.MyMACAD = 0
        self.signal = 0
        self.gyst = 0

    def update(self,value):
        self.MyMACAD = self.ema1.update(value) - self.ema0.update(value)
        self.signal = self.ema2.update(self.MyMACAD)
        self.gyst = self.MyMACAD - self.signal
        return self.gyst

    def get(self):
        return self.gyst

    def getFromArray(self,values):
        self.clear()
        gyst = []
        MyMACAD = []
        signal = []

        for var in values:
            self.update(var)
            gyst.append(copy.deepcopy(self.gyst))
            MyMACAD.append(copy.deepcopy(self.MyMACAD))
            signal.append(copy.deepcopy(self.signal))
        return gyst,MyMACAD,signal

class MyKDJ():
    def __init__(self,N = 26*scale):
        self.N = N;
        self.RSV = 0
        self.K = 50
        self.D = 50
        self.J = 0

    def clear(self):
        self.RSV = 0
        self.K = 50
        self.D = 50
        self.J = 0
        self.candles = collections.deque(maxlen=self.N)

    def updateFromCandle(self,candle):
        self.candles.append(copy.deepcopy(candle))
        L = min(self.candles,key= lambda a: a.l).l
        H = max(self.candles,key= lambda a: a.h).h
        if L != H:
            self.RSV = 100 * (candle.c - L) / (H - L)
        else:
            self.RSV = 100 * (candle.c - L)
        self.K = 2 / 3 * self.K + 1 / 3 * self.RSV
        self.D = 2 / 3 * self.D + 1 / 3 * self.K
        self.J = 3 * self.K - 2 * self.D
        return self.J - self.D;

    def getFromArray(self, candles):
        self.clear()
        RSV = []
        K = []
        D = []
        J = []
        for candle in candles:
            self.updateFromCandle(candle)
            RSV.append(copy.deepcopy(self.RSV))
            K.append(copy.deepcopy(self.K))
            D.append(copy.deepcopy(self.D))
            J.append(copy.deepcopy(self.J))
        return RSV,K,D,J,np.array(J)-np.array(D)

class MyTSI:
    def __init__(self,N = 24*scale, M = 8*scale):
        self.N = N
        self.M = M
        self.clear()

    def clear(self):
        self.TSI = 0;
        self.ema0 = MyEMA(self.N)
        self.ema1 = MyEMA(self.N)
        self.ema2 = MyEMA(self.M)
        self.ema3 = MyEMA(self.M)

    def update(self,momentum):
        a = self.ema3.update(self.ema1.update(abs(momentum)))
        if a!=0:
            self.TSI = self.ema2.update(self.ema0.update(momentum)) / a
        else:
            self.TSI =  0*self.ema2.update(self.ema0.update(momentum))

        return self.TSI

    def getFromArray(self, candles):
        self.clear()
        TSI = []
        for candle in candles:
            self.update(candle.c-candle.o)
            TSI.append(copy.deepcopy(self.TSI))
        return TSI