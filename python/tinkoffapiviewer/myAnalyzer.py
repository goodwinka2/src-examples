import copy
import logging

import pandas as pd
import numpy as np
import myIndicators
from tinko_db import TinkoffDB
import simplethread
import threading
import logging as log
from threading import Timer
from dataclasses import dataclass
from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    Currency
)
import functools
import time
import codetiming
import concurrent.futures
from datetime import datetime, timedelta, timezone
import os
import scipy
import myMath
t = codetiming.Timer(name="class",logger=log.debug)

defaultColumns = {
    'data':[],
    'Name':None,
    'Name full':None,
    'figi': None,
    'type': None,
    'currency': None,
    'Price':None,
    'precent': None,
    'Count':None,
                  }
candleSeconds = {
    CandleResolution.MIN_1.value :60,
    CandleResolution.MIN_2.value :60*2,
    CandleResolution.MIN_3.value :60*3,
    CandleResolution.MIN_5.value :60*5,
    CandleResolution.MIN_10.value :60*10,
    CandleResolution.MIN_15.value :60*15,
    CandleResolution.MIN_30.value :60*30,
    CandleResolution.HOUR.value :60*60,
    CandleResolution.DAY.value :60*60*24,
    CandleResolution.WEEK.value :60*60*24*7,
    CandleResolution.MONTH.value :60*60*24*30,

}


class CandleAnalyzer(simplethread.SimpleThread):
#class CandleAnalyzer():
    def __init__(self,tinkoDb=None,tinko=None):
        simplethread.SimpleThread.__init__(self)
        self.tinkoDb = tinkoDb
        self.tinko = tinko
        self.allData = {'time':datetime.now(),
                        'all':None,
                        }
        self.buyData = {'time': datetime.now(),
                        'stock_usd': None,
                        'stock_rub': None,
                        'etf_rub': None,
                        'etf_usd': None,
                        }
        self.sellData = {'time': datetime.now(),
                        'stock_usd': None,
                        'stock_rub': None,
                        'etf_rub': None,
                        'etf_usd': None,
                        }

    def setTinkoUpdater(self,tinkoDbUpdater):
        self.tinkoDbUpdater = tinkoDbUpdater

    def startAnalyzeAll(self):
        log.debug('Start analyze')
        self.tinko.blockingCall(self.tinko.get_stocks)


        t.start()
        result = []
        threads = []
        loaded = self.tinkoDb.blockingCall(self.tinkoDb.getData,'allData')
        if loaded is None:
            loaded = self.allData
            loaded['time'] = loaded['time'].replace(year=2000)

        if True or datetime.now() - timedelta(days=0, hours=12, minutes=0) > loaded['time']:
            with concurrent.futures.ThreadPoolExecutor() as executor:
                for i,stock in enumerate(self.tinko.stocks[:]):
                    candles = self.tinkoDb.blockingCall(self.tinkoDb.getCandles,
                                              figi=stock.figi,
                                              interval = self.tinkoDbUpdater.configAll.resolution)

                    th = threading.Thread(target=self.createNumpyRow,args=(i,stock,candles,result))


                    th.start()
                    threads.append(th)
                for th in threads:
                    th.join()

            self.allData['all'] = pd.DataFrame(result, columns=defaultColumns.keys())\
                .query('').sort_values(by=['chance'],ascending=[False])
            self.allData['time'] = datetime.now()
            self.tinkoDb.blockingCall(self.tinkoDb.setData, 'allData', self.allData)
        else:
            self.allData = loaded
        


        t.stop()

        if not os.path.exists('analyze_report'):
            os.mkdir('analyze_report')
        with pd.ExcelWriter(f'analyze_report/{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.xlsx') as writer:
            self.allData['all'].to_excel(writer, sheet_name='all')



        return self.allData['all']


    def getPositiveCloseChance(self,closePrecent):
        return sum(True for x in closePrecent if x>0)/len(closePrecent)

    def getPositiveNextChance(self,closePrecent,globalChance):
        return 2*globalChance - self.getPositiveCloseChance(closePrecent)

    def getInfo(self,figi):
        return self.allData['all'].loc[self.allData['all']['figi']== figi].iloc[0]

    def createNumpyRow(self,index, stock, candles, result):
        row = defaultColumns.copy()  # {'Name': stock.ticker,'Name full': stock.name, 'Count': len(candles)}
        row['Name'] = f'{stock.ticker} {stock.figi}'
        row['Name full'] = stock.name
        #row['stock'] = stock
        row['figi'] = stock.figi
        row['currency'] = stock.currency.value
        row['type'] = stock.type.value
        row['Count'] = len(candles)




        result.append(row)
        log.debug(f'{threading.current_thread().name} ready analyze {index} from {len(self.tinko.stocks)}')
        return row

    def getRecomendation(self, candles):
        recomendation = {'rec': 'hold','info':{},'info_str':'None'}








        return recomendation

