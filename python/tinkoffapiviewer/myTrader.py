import copy
import logging
import typing

import pandas as pd
import numpy as np
import myIndicators
from tinko_db import TinkoffDB
import simplethread
import threading
import logging as log
from threading import Timer
from dataclasses import dataclass, field, asdict
from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    Currency,
    OperationType,
    Operation
)
import functools
import time
import codetiming
import concurrent.futures
from datetime import datetime, timedelta, timezone
import os
import scipy
import config

@dataclass
class Config:
    currency : Currency.USD
    maxPrice : 0.0
    freeCount : 0.0

@dataclass
class TradeInfo:
    name:str
    figi:str
    delta: 0.0
    type : str
    price : 0.0
    currentPrice : 0.0
    lots: 0
    iterations: 0
    info: str
    time: datetime = field(metadata={"deserialize": "ciso8601"})
    order: 'typing.Any' = object()
    info_sell: 'typing.Any' = object()
    info_buy: 'typing.Any' = object()

class TinkoffTrader(simplethread.SimpleThread):
    def __init__(self,tinkoDb=None,tinko=None):
        simplethread.SimpleThread.__init__(self)
        self.tinkoDb = tinkoDb
        self.tinko = tinko
        self.resolution = CandleResolution.HOUR
        self.candleCount = 100
        self.configUSD = Config(
            currency = Currency.USD,
            maxPrice = 200,
            freeCount = 0
        )
        self.configRUB = Config(
            currency = Currency.RUB,
            maxPrice = 15000,
            freeCount= 0
        )
        self.configRUBetf = Config(
            currency = Currency.RUB,
            maxPrice = 15000,
            freeCount= 0
        )
        self.usePrice= config.usePrice
        self.maxIterations = config.maxIterations
        self.myTrades = {'good':[],'bad':[]}

    def setAnalyzer(self,analyzer):
        self.analyzer = analyzer

    def saveTrades(self):
        good = []
        bad = []

        with pd.ExcelWriter(f'analyze_report/trades_{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.xlsx') as writer:
            pd.DataFrame(good).to_excel(writer, sheet_name='good')
            pd.DataFrame(bad).to_excel(writer, sheet_name='bad')
        self.myTrades = {'good': [], 'bad': []}

    def tryTrade(self):
        logging.info('start try trade')
        self.tinko.blockingCall(self.tinko.cancel_all_orders)
        self.trySell()
        self.tryBuy(self.configUSD, 'etf_usd')
        self.tryBuy(self.configUSD, 'stock_usd')
        self.tryBuy(self.configRUBetf, 'etf_rub')
        self.tryBuy(self.configRUB, 'stock_rub')
        logging.info('end try trade')

    def trySell(self,force=False):
        pass



    def getLotsCount(self,config,stock,price,currency):
        #balance = (currency.balance - currency.blocked - config.freeCount)
        balance = (currency.balance - config.freeCount)
        pricePerLot = price*1.01
        lots = int(np.floor(min(config.maxPrice,balance)/pricePerLot/stock.lot))
        #if stock.minQuantity is not None:
        #    if lots < stock.minQuantity:
        #        lots = 0
        return lots

    def tryProfit(self,info):
        return True


    def trySupport(self,row,price):
 
        return False

    def tryBuy(self, cfg, category):
        pass
 



