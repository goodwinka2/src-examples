
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objects as go

import pandas as pd
from datetime import datetime

import tinkoff.investments as tinko
from tinkoff.investments.utils.historical_data import HistoricalData
import asyncio

def get_or_create_eventloop():
    try:
        return asyncio.get_event_loop()
    except RuntimeError as ex:
        if "There is no current event loop in thread" in str(ex):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            return asyncio.get_event_loop()


df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/finance-charts-apple.csv')

app = dash.Dash(__name__)

from datetime import datetime

from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
)
from tinkoff.investments.utils.historical_data import HistoricalData
client = TinkoffInvestmentsRESTClient(
        token="t.JJnuDnMXv3cYKoi75aBvC3-cmHM1YIL4H0z3P1Rly64U_uK6E4qr5q38zFo0NbFHSUURHpT2iQKl-ehyoOJPPg", environment=Environment.SANDBOX
    )
candles = []
async def get_minute_candle():
    # show 1 minute candles for AAPL in 1 year period of time
    historical_data = HistoricalData(client)
    print('async')
    async for candle in historical_data.iter_candles(
        figi="BBG000B9XRY4",
        dt_from=datetime(2019, 1, 1),
        dt_to=datetime(2019, 1, 20),
        interval=CandleResolution.MIN_1,
    ):
        candles.append(candle)
    print('async end')

async def get_minute_candles(figi):
    # show 1 minute candles for AAPL in 1 year period of time
    historical_data = HistoricalData(client)
    candles.clear()
    print('async')
    async for candle in historical_data.iter_candles(
        figi=figi,
        dt_from=datetime(2019, 1, 1),
        dt_to=datetime(2019, 1, 20),
        interval=CandleResolution.MIN_1,
    ):
        candles.append(candle)
    print('async end')
    return candles

stocks =[]
async def get_currencies():
    # show 1 minute candles for AAPL in 1 year period of time
    print('async')
    stocks.extend(await client.market.instruments.get_stocks())

    print('async end')
    return stocks
bb = None
if 1:
    get_or_create_eventloop().run_until_complete(get_minute_candle())
    bb = get_or_create_eventloop().run_until_complete(get_currencies())
else:
    asyncio.run(get_minute_candle())
    bb = asyncio.run(get_currencies())

print('0')
print('1')
app.layout = html.Div([
    html.Label('Dropdown'),
    dcc.Checklist(
        id='toggle-rangeslider',
        options=[{'label': 'Include Rangeslider',
                  'value': 'slider'}],
        value=['slider']
    ),
    dcc.Dropdown(
        id='my-dropdown',
        options=[{'label': stock.name, 'value': stock.figi} for stock in stocks],
        #     {'label': 'Coke', 'value': 'COKE'},
        #     {'label': 'Tesla', 'value': 'TSLA'},
        #     {'label': 'Apple', 'value': 'AAPL'}
        # ],
        value='COKE'
    ),
    html.Label('plot'),
    dcc.Graph(id="graph"),
])

@app.callback(
    Output("graph", "figure"),
    [Input("toggle-rangeslider", "value")])
def display_candlestick(value):
    # fig = go.Figure(go.Candlestick(
    #     x=df['Date'],
    #     open=df['AAPL.Open'],
    #     high=df['AAPL.High'],
    #     low=df['AAPL.Low'],
    #     close=df['AAPL.Close']
    # ))
    c = get_or_create_eventloop().run_until_complete(get_minute_candles(value))
    fig = go.Figure(go.Candlestick(
        x=df['Date'],
        open=df['AAPL.Open'],
        high=df['AAPL.High'],
        low=df['AAPL.Low'],
        close=df['AAPL.Close']
    ))
    fig.update_layout(
        xaxis_rangeslider_visible='slider' in value
    )

    return fig

app.run_server(debug=True)

# import dash
# from dash.dependencies import Input, Output
# import dash_core_components as dcc
# import dash_html_components as html
# import plotly.graph_objects as go
# import pandas as pd
# from pandas_datareader import data as web
# from datetime import datetime as dt
#
# app = dash.Dash('Hello World')
#
#
# app.layout = html.Div([
#     dcc.Checklist(
#         id='toggle-rangeslider',
#         options=[{'label': 'Include Rangeslider',
#                   'value': 'slider'}],
#         value=['slider']
#     ),
#     dcc.Dropdown(
#         id='my-dropdown',
#         options=[
#             {'label': 'Coke', 'value': 'COKE'},
#             {'label': 'Tesla', 'value': 'TSLA'},
#             {'label': 'Apple', 'value': 'AAPL'}
#         ],
#         value='COKE'
#     ),
#     dcc.Graph(id='my-graph')
# ], style={'width': '500'})

# @app.callback([Output('my-graph', 'figure')], [Input('my-dropdown', 'value')])
# def update_graph(value):
#     # df = web.DataReader(
#     #     selected_dropdown_value,
#     #     'google',
#     #     dt(2017, 1, 1),
#     #     dt.now()
#     # )
#     #df = web.DataReader('USD000UTSTOM', 'moex', start='2017-07-01', end='2017-08-31')
#
#
#     # fig = go.Figure(go.Candlestick(
#     #     x=df['CLOSE'].index.array,
#     #     open=df['OPEN'],
#     #     high=df['HIGH'],
#     #     low=df['LOW'],
#     #     close=df['CLOSE']
#     # ))
#
#     # fig.update_layout(
#     #     xaxis_rangeslider_visible='slider' in value
#     # )
#
#     return fig


    # df = web.DataReader('USD000UTSTOM', 'moex', start='2017-07-01', end='2017-07-31')
    # df.head()
    # return {
    #     'data': [{
    #         'x': df['CLOSE'].index.array,
    #         'y': df['CLOSE'].array
    #     }],
    #     'layout': {'margin': {'l': 40, 'r': 0, 't': 20, 'b': 30}}
    # }

#app.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})


app.run_server(debug=False,
                    host = '127.0.0.1')







# import dash
# import dash_core_components as dcc
# import dash_html_components as html
# import pandas as pd
#
# data = pd.read_csv("avocado.csv")
# data = data.query("type == 'conventional' and region == 'Albany'")
# data["Date"] = pd.to_datetime(data["Date"], format="%Y-%m-%d")
# data.sort_values("Date", inplace=True)
#
# app = dash.Dash(__name__)
#
# app.layout = html.Div(
#     children=[
#         html.H1(children="Avocado Analytics",),
#         html.P(
#             children="Analyze the behavior of avocado prices"
#             " and the number of avocados sold in the US"
#             " between 2015 and 2018",
#         ),
#         dcc.Graph(
#             figure={
#                 "data": [
#                     {
#                         "x": data["Date"],
#                         "y": data["AveragePrice"],
#                         "type": "lines",
#                     },
#                 ],
#                 "layout": {"title": "Average Price of Avocados"},
#             },
#         ),
#         dcc.Graph(
#             figure={
#                 "data": [
#                     {
#                         "x": data["Date"],
#                         "y": data["Total Volume"],
#                         "type": "lines",
#                     },
#                 ],
#                 "layout": {"title": "Avocados Sold"},
#             },
#         ),
#     ]
# )
#
# if __name__ == "__main__":
#     app.run_server(debug=True,
#                    host = '127.0.0.1')