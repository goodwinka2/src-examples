import simplethread
import logging
from tinkoff.investments import (
    CandleResolution,
    Environment,
    TinkoffInvestmentsRESTClient,
    TinkoffInvestmentsStreamingClient,
    Currency
)
from tinkoff.investments.utils.historical_data import HistoricalData
from datetime import datetime, timedelta
import time
import config
class TinkoffThread(simplethread.SimpleThread):
    def __init__(self):
        simplethread.SimpleThread.__init__(self)
        self.stocks = []#self.blockingCall(self.get_stocks())
        self.accounts = None#self.blockingCall(self.get_accounts())
        self.currencies = []
        self.setExceptionLoop()
        pass

    def setExceptionLoop(self):
        self.exceptionLoopFunctions.append(self.get_portfolio_currencies)
        self.exceptionLoopFunctions.append(self.get_portfolio)
        self.exceptionLoopFunctions.append(self.get_currencies)
        self.exceptionLoopFunctions.append(self.get_currency)
        self.exceptionLoopFunctions.append(self.get_accounts)
        self.exceptionLoopFunctions.append(self.get_stocks)
        self.exceptionLoopFunctions.append(self.get_stock_info)
        self.exceptionLoopFunctions.append(self.get_orderbook)

        #self.exceptionLoopFunctions.append(self.get_candles)


    async def initSandbox(self,reinitialize = False):
        self.client = TinkoffInvestmentsRESTClient(
            token=config.tokenSand, #sand
            environment=Environment.SANDBOX, wait_on_rate_limit = config.wait_on_rate_limit)
        self.historical_data = HistoricalData(self.client)
        await self.get_stocks()
        await self.get_currencies()
        if reinitialize:
            #account = await self.client.sandbox.accounts.register()
            await self.client.sandbox.accounts.clear()
            #account = await self.client.sandbox.accounts.register()
            await self.client.sandbox.accounts.currencies.set_balance(
             Currency.RUB,
             40000
            )
            await self.client.sandbox.accounts.currencies.set_balance(
             Currency.USD,
             1500
            )
        # v = self.stocks[:3]
        # for stock in v:
        #     await self.client.sandbox.accounts.positions.set_balance(
        #         figi=stock.figi,
        #         balance=100,
        #         broker_account_id=account.brokerAccountId,
        #     )

        await self.get_accounts()
        await self.get_portfolio()
        await self.get_portfolio_currencies()
        pass

    async def initProduction(self):
        self.client = TinkoffInvestmentsRESTClient(
            token=config.tokenProd, #TINKOFF
            environment=Environment.PRODUCTION)
        self.historical_data = HistoricalData(self.client)
        await self.get_stocks()
        await self.get_currencies()
        await self.get_accounts()
        await self.get_portfolio()
        await self.get_portfolio_currencies()
        pass



    async def get_candles(self,figi,start=datetime.now() - timedelta(days=27),end=datetime.now(),interval=CandleResolution.HOUR):

        if 1:
            candles = []
            async for candle in self.historical_data.iter_candles(
                    figi=figi,
                    dt_from=start,#datetime.now() - timedelta(days=27),
                    dt_to=end,#datetime.now(),
                    interval=interval,#CandleResolution.HOUR,
            ):
                candles.append(candle)
        else:
            #candles = []
            candles = await self.client.market.candles.get(
                figi=figi,
                dt_from=start,  # datetime.now() - timedelta(days=27),
                dt_to=end,  # datetime.now(),
                interval=interval,  # CandleResolution.HOUR,
            )

        #print(f'return {figi} {len(candles)}')
        return candles

    async def get_stocks(self):
        self.stocks.clear()
        result = None
        while result is None:
            result = await self.client.market.instruments.get_stocks()
            if result is None:
                logging.error('no response')
                time.sleep(30)
        self.stocks.extend(result)
        result2 = None
        while result2 is None:
            result2 = await self.client.market.instruments.get_etfs()
            if result is None:
                logging.error('no response')
                time.sleep(30)
        self.stocks.extend(result2)

        self.stocks = sorted(self.stocks, key=lambda x: x.name)
        return self.stocks

    async def get_currencies(self):
        self.currencies.clear()
        result = None
        while result is None:
            result = await self.client.market.instruments.get_currencies()
            if result is None:
                logging.error('no response')
                time.sleep(30)
        self.currencies.extend(result)
        return self.currencies

    async def get_currency(self,currencyType):
        await self.get_portfolio_currencies()
        return next((x for x in self.portfolio_currencies if x.currency == currencyType), None)

    async def get_stock_info(self,figi):
        result = None
        while result is None:
            result = await self.client.market.instruments.get(figi)
            if result is None:
                logging.error('no response')
                time.sleep(30)
        return result

    async def get_orderbook(self,figi,depth=1):
        result = None
        while result is None:
            result = await self.client.market.orderbooks.get(figi,depth)
            if result is None:
                logging.error('no response')
                time.sleep(30)
        return result

    async def get_accounts(self):
        result = None
        while result is None:
            result = await self.client.user.get_accounts()
            if result is None:
                logging.error('no response')
                time.sleep(30)
        self.accounts = result
        print(str(self.accounts))
        return self.accounts

    async def get_portfolio(self):
        result = None
        while result is None:
            result = await self.client.portfolio.get_positions()
            if result is None:
                logging.error('no response')
                time.sleep(30)

        self.portfolio = result
        return self.portfolio

    async def get_portfolio_currencies(self):
        result = None
        while result is None:
            result = await self.client.portfolio.get_currencies()
            if result is None:
                logging.error('no response')
                time.sleep(30)
        self.portfolio_currencies = result
        return self.portfolio_currencies

    async def set_order(self,figi,lots,operationtype,price=0):
        result = None
        if price == 0:
            result = await self.client.orders.create_market_order(figi,lots,operationtype)
        else:
            result = await self.client.orders.create_limit_order(figi, lots, operationtype,price)
        #print(str(result))
        logging.info(f'set_order {figi} * {lots}: {result}')
        return result

    async def get_orders(self):
        result = None
        while result is None:
            result = await self.client.orders.get()
            if result is None:
                logging.error('no response')
                time.sleep(30)
        print(str(result))
        return result

    async def cancel_order(self,id):

        result = await self.client.orders.cancel(id)
        logging.warning(f'cancelled {id}: {str(result)}')
        return result

    async def cancel_orders(self, figi):
        for order in await self.get_orders():
            if order.figi == figi:
                await self.cancel_order(order.orderId)

    async def cancel_all_orders(self):
        for order in await self.get_orders():
            await self.cancel_order(order.orderId)

