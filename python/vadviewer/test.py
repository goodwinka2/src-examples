#pip install -q torchaudio soundfile
import torch
import codetiming
import copy
import hubconf
import matplotlib.pyplot as plt
import seaborn as sns
import torchaudio
from matplotlib.widgets import Button
import tkinter.filedialog as fd
import concurrent.futures
targetRate = 16000




class TestVAD:
    def __init__(self):
        torch.set_num_threads(1)

        self.drawWindow()
        self.model, self.utils = hubconf.silero_vad()
        # model, utils = silero_vad_mini_8k()
        return

    def one(self,event):
        filetypes = [("Аудио файл", "*.wav")]
        filename = fd.askopenfilename(title="Открыть файл", initialdir="./",
                                      filetypes=filetypes)
        if filename:
            print(filename)
            for ax in self.graph_axes:
                ax.clear()
            #self.loadFile(filename)
            with concurrent.futures.ThreadPoolExecutor() as executor:
                for i in range(60):
                    executor.submit(self.loadFileNotGiu, filename)


    def drawWindow(self):
        sns.set(font_scale=0.7)
        self.fig, self.graph_axes = plt.subplots(2,sharex=False,figsize=(14, 5))
        self.fig.subplots_adjust(left=0.05, right=0.95, top=0.85, bottom=0.2,wspace = 0, hspace = 0.5)
        axes_button_add = plt.axes([0.25, 0.05, 0.5, 0.05])
        self.button_add = Button(axes_button_add, 'Загрузить аудиофайл')
        on_clicked_id = self.button_add.on_clicked(self.one)

    def plot_signal(self,sr, signal, labels, axes,title):
        cutSignal = [float(signal[i]) for i in range(0,len(signal),int(sr/1000))]
        sns.lineplot(x=[i / sr for i in range(0,len(signal),int(sr/1000))], y=cutSignal,ax = axes)
        start, end = 0, 0
        for seg in labels:
            axes.axvspan(end, seg["start"] / sr, alpha=0.5, color="r")
            start, end = seg["start"] / sr, seg["end"] / sr
            axes.axvspan(start, end, alpha=0.5, color="g")
        axes.axvspan(end, (len(signal) - 1) / sr, alpha=0.5, color="r")
        # axes.set_title("Sampling rate = {} | Num. points = {} | Tot. duration = {:.2f} s".format(
        #     sr, len(signal), len(signal) / sr
        # ), size=10)
        axes.set_title(title)
        axes.set_xlabel("Время (с)", size=10)
        axes.set_ylabel("Амплитуда", size=10)
        return

    def getTimeStamps(self,timestamps,sr = 16000):
        speech_timestamps = copy.deepcopy(timestamps);
        for i, ts in enumerate(speech_timestamps):
            speech_timestamps[i]['start'] = speech_timestamps[i]['start'] / sr
            speech_timestamps[i]['end'] = speech_timestamps[i]['end'] / sr
        return speech_timestamps

    def getTotalSpeekTime(self,speech_timestamps,sr):
        res = 0
        for data in speech_timestamps:
            res+=data['end'] - data['start']
        return res/sr

    def analize(self,wav,rate):

        (get_speech_ts,
         get_speech_ts_adaptive,
         save_audio,
         read_audio,
         state_generator,
         single_audio_stream,
         collect_chunks) = self.utils

        transform = torchaudio.transforms.Resample(orig_freq=rate,
                                                   new_freq=targetRate)
        wav = transform(wav)

        t = codetiming.Timer(name="class")

        if 0:
            t.start()
            speech_timestamps = utils.get_speech_ts(wav, self.model, num_steps=2)
            t.stop()
            print(getTimeStamps(speech_timestamps,targetRate))
            plot_signal(16000, wav, speech_timestamps, 'fn')
            save_audio('only_speech.wav', collect_chunks(speech_timestamps, wav), targetRate)

        if 1:
            t.start()
            speech_timestamps = get_speech_ts_adaptive(wav, self.model, device='cpu', min_silence_samples=2000)
            t.stop()
            #save_audio('only_speech_adapt.wav', collect_chunks(speech_timestamps, wav), targetRate)
            #print(self.getTimeStamps(speech_timestamps,targetRate))

        return speech_timestamps,wav
    def loadFileNotGiu(self,fileName):

        self.wavStereo, rate = torchaudio.backend.soundfile_backend.load_wav(fileName)

        with concurrent.futures.ThreadPoolExecutor() as executor:

            futureLeft = executor.submit(self.analize,self.wavStereo[0], rate)
            futureRight = futureLeft
            if len(self.wavStereo) > 1:
                futureRight = executor.submit(self.analize, self.wavStereo[1], rate)

            speech_timestamps, wav = futureLeft.result()

            leftTime = self.getTotalSpeekTime(speech_timestamps, targetRate)

            speech_timestamps, wav = futureRight.result()
            rightTime = self.getTotalSpeekTime(speech_timestamps, targetRate)

    def loadFile(self,fileName):

        self.wavStereo, rate = torchaudio.backend.soundfile_backend.load_wav(fileName)

        with concurrent.futures.ThreadPoolExecutor() as executor:

            futureLeft = executor.submit(self.analize,self.wavStereo[0], rate)
            futureRight = futureLeft
            if len(self.wavStereo) > 1:
                futureRight = executor.submit(self.analize, self.wavStereo[1], rate)

            speech_timestamps, wav = futureLeft.result()

            leftTime = self.getTotalSpeekTime(speech_timestamps, targetRate)
            self.plot_signal(16000, wav, speech_timestamps, self.graph_axes[0],
                             'Левый канал. Общее время речи {} с'.format(leftTime))

            speech_timestamps, wav = futureRight.result()
            rightTime = self.getTotalSpeekTime(speech_timestamps, targetRate)
            self.plot_signal(16000, wav, speech_timestamps, self.graph_axes[1],
                             'Правый канал. Общее время речи {} с'.format(rightTime))

            self.fig.suptitle('Файл '+ fileName + '\nОтношение речи левого канала к правому {}'.format(leftTime/rightTime))

        plt.show()



testVAD = TestVAD()
testVAD.loadFile('./demo.wav')
