#pragma once

#include "qcustomplot.h"
#include "qlist.h"
//#include "dataconverter.h"
#include <QLayout>

#include "qwidget.h"
class TelemetryPlot :
	public QWidget
{
	Q_OBJECT
public:
	TelemetryPlot(QString xName = QString("x"), QString yName = QString("y"));
	~TelemetryPlot();

	//void setDataConverterPointer(DataConverter*p);
	enum RangeType
	{
		DynamicRangeType, //������������ ������������ �������
		StaticRangeType,// ����������� �������
		IncrementalRangeType// ������������� �������
	};

	void changeRangeType(RangeType);
	void changeStaticValueRange(QCPRange);
	void setShownValueCount(int); // ���������� ��������� ���������� ������

	void updateData(double data, double time, int graphIndex = 0);
	void setName(QString xName = QString("x"), QString yName = QString("y"));
	void replot();
	void addGraph(QString name = QString());
	void clear();

	void clearAdditionalData(); // ������� ��� �������� ����� ��������
	void addLevelLine(double dataValue, QColor color = Qt::green, QString name = QString());

protected:
	void contextMenuEvent(QContextMenuEvent *event) Q_DECL_OVERRIDE;

private:
	bool isReplotable;
	void createGui();
	RangeType currentRangeType;
	QList<QCPAbstractPlottable*> plotsList;
	QList<QCPAbstractItem*> itemsList;
	int shownValueCount;
	QCPRange staticRangeValue;
	QCPRange dynamicRangeValue;
	QCPRange incrementalRangeValue;
	//QCPRange dynamicRangeValue;
	QCPRange globalRangeValue;
	QCPRange globalRangeKey;

	QCustomPlot *qCustomPlot;
	QString xName;
	QString yName;
	QList<QColor> colorList;
	//QLabel currentModeLabel;

};

