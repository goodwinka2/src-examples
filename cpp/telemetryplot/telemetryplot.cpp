#include "telemetryplot.h"


TelemetryPlot::TelemetryPlot(QString x, QString y)
{
	isReplotable = true;
	currentRangeType = RangeType::IncrementalRangeType;
	staticRangeValue = QCPRange(0, 0);
	xName = x;
	yName = y;
	shownValueCount = 1000;
	colorList.append(Qt::red);
	colorList.append(Qt::blue);
	colorList.append(Qt::green);
	colorList.append(Qt::black);
	colorList.append(Qt::yellow);
	colorList.append(Qt::cyan);
	colorList.append(Qt::magenta);
	colorList.append(Qt::darkRed);
	colorList.append(Qt::darkBlue);
	colorList.append(Qt::darkCyan);
	colorList.append(Qt::darkGreen);
	colorList.append(Qt::darkMagenta);
	colorList.append(Qt::gray);
	colorList.append(Qt::darkYellow);


	createGui();

	setMinimumHeight(300);
	setMinimumWidth(1000);
}


TelemetryPlot::~TelemetryPlot()
{
}

void TelemetryPlot::changeRangeType(RangeType type)
{
	currentRangeType = type;
}

void TelemetryPlot::changeStaticValueRange(QCPRange range)
{
	staticRangeValue = range;
}

void TelemetryPlot::setShownValueCount(int count)
{
	shownValueCount = count;
}

void TelemetryPlot::updateData(double y, double x, int graphIndex)
{
	if (graphIndex < 0)
		return;


	if (graphIndex >= qCustomPlot->graphCount())
	{
		return;
	}
	auto graph = qCustomPlot->graph(graphIndex);
	auto data = graph->data();

	if (data.data()->isEmpty())
	{
		incrementalRangeValue.lower = y;
		dynamicRangeValue.lower = y;
		globalRangeKey.lower = x;

		incrementalRangeValue.upper = y;
		dynamicRangeValue.upper = y;
		globalRangeKey.upper = x;
	}

	incrementalRangeValue.lower = std::min(incrementalRangeValue.lower, y);
	incrementalRangeValue.upper = std::max(incrementalRangeValue.upper, y);


	//globalRangeKey.lower = std::min(globalRangeKey.lower, x);
	globalRangeKey.upper = std::max(globalRangeKey.upper, x);

#if 0
	if (data.data()->size() > 1000)
	{
		//data.data()->clear();
		data.data()->removeBefore(data.data()->at(20)->key);
		globalRangeKey.lower = data.data()->at(0)->key;

	}
#else
	if (data.data()->size() > shownValueCount)
	{
		globalRangeKey.lower = data.data()->at(data.data()->size() - shownValueCount)->key;
		//qCustomPlot->xAxis->setRange(globalRangeKey.upper, globalRangeKey.lower, Qt::AlignRight);
	}
#endif

	if (!data.data()->isEmpty())
	if (data.data()->at(data.data()->size() - 1)->key > x)
	{
		data.data()->clear();

		incrementalRangeValue.lower = y;
		dynamicRangeValue.lower = y;
		globalRangeKey.lower = x;

		incrementalRangeValue.upper = y;
		dynamicRangeValue.upper = y;
		globalRangeKey.upper = x;
	}
	graph->addData(x, y);



	//qCustomPlot->replot();
}

void TelemetryPlot::setName(QString x, QString y)
{
	xName = x;
	yName = y;
	qCustomPlot->xAxis->setLabel(xName);
	qCustomPlot->yAxis->setLabel(yName);
}

void TelemetryPlot::clear()
{
	clearAdditionalData();
}

void TelemetryPlot::clearAdditionalData()
{
	for (auto var : plotsList)
	{
		qCustomPlot->removePlottable(var);
		//delete var;
	}
	plotsList.clear();
	for (auto var : itemsList)
	{
		qCustomPlot->removeItem(var);
		//delete var;
	}
	itemsList.clear();
}

void TelemetryPlot::contextMenuEvent(QContextMenuEvent *event)
{

}

void TelemetryPlot::addLevelLine(double dataValue, QColor color, QString name)
{

	QCPCurve *newCurve = new QCPCurve(qCustomPlot->xAxis, qCustomPlot->yAxis);
	newCurve->setPen(QPen(color));
	newCurve->setName(name);

	if (name.isEmpty())
		newCurve->removeFromLegend();
	bool ok;
	//auto rangeValue = qCustomPlot->graph()->getValueRange(ok);
	auto rangeKey = globalRangeKey;// qCustomPlot->graph()->getKeyRange(ok);

	QVector<double> x;
	QVector<double> y;

	x << rangeKey.lower;
	y << dataValue;

	x << rangeKey.upper;
	y << dataValue;

	newCurve->setData(x, y);

	//qCustomPlot->xAxis->setRange(rangeKey.lower, rangeKey.upper);//��� ��� Ox
	//qCustomPlot->yAxis->setRange(rangeValue.lower, rangeValue.upper);//��� ��� Oy

	//QCPItemText *dispersionText = new QCPItemText(qCustomPlot);
	//dispersionText->position->setCoords(data->at(dataIndex)->mainKey(), data->at(dataIndex)->mainValue() + (rangeValue.upper - rangeValue.lower) * 2 / 100.0);
	//dispersionText->setRotation(0);
	////qCustomPlot->graph(0)->get
	//dispersionText->setText(QString::fromLocal8Bit("%1").arg(data->at(dataIndex)->mainValue()));
	////dispersionText->setFont(QFont(font().family(), 10));
	//dispersionText->setFont(QFont("Arial", 10));

	QCPItemText *dispersionText = new QCPItemText(qCustomPlot);
	dispersionText->position->setCoords(rangeKey.lower, dataValue);
	dispersionText->setRotation(0);
	dispersionText->setPositionAlignment(Qt::AlignLeft);
	//qCustomPlot->graph(0)->get
	dispersionText->setText(QString::fromLocal8Bit("%1").arg(dataValue));
	//dispersionText->setFont(QFont(font().family(), 10));
	dispersionText->setFont(QFont("Arial", 10));
	dispersionText->setPen(QPen(color));
	dispersionText->setBrush(Qt::white);
	//qCustomPlot->replot();

	plotsList << newCurve;
	itemsList << dispersionText;
}


void TelemetryPlot::replot()
{
	if (!isReplotable)
		return;

	qCustomPlot->xAxis->setRange(globalRangeKey.lower, globalRangeKey.upper);//��� ��� Ox

	switch (currentRangeType)
	{
	case TelemetryPlot::DynamicRangeType:
	{
											bool ok = false;
											QCPRange range;
											
											for (size_t graphIndex = 0; graphIndex < qCustomPlot->graphCount(); graphIndex++)
											{
												auto graph = qCustomPlot->graph(graphIndex);

												if (graph->visible())
												{
													if (!ok)
														range = graph->getValueRange(ok, QCP::sdBoth, globalRangeKey);
													auto tempRange = graph->getValueRange(ok, QCP::sdBoth, globalRangeKey);
													range.lower = std::min(range.lower, tempRange.lower);
													range.upper = std::max(range.upper, tempRange.upper);
												}
											}
											dynamicRangeValue = range;
											//dynamicRangeValue = qCustomPlot->graph()->getValueRange(ok, QCP::sdBoth, globalRangeKey);
											//dynamicRangeValue.lower = std::min(dynamicRangeValue.lower, range.lower);
											//dynamicRangeValue.upper = std::max(dynamicRangeValue.upper, range.upper);
											if (ok)
											qCustomPlot->yAxis->setRange(dynamicRangeValue.lower, dynamicRangeValue.upper);//��� ��� Oy
											break;
	}
	case TelemetryPlot::StaticRangeType:
		qCustomPlot->yAxis->setRange(staticRangeValue.lower, staticRangeValue.upper);//��� ��� Oy
		break;
	case TelemetryPlot::IncrementalRangeType:
		qCustomPlot->yAxis->setRange(incrementalRangeValue.lower, incrementalRangeValue.upper);//��� ��� Oy
		break;
	default:
		qCustomPlot->yAxis->setRange(incrementalRangeValue.lower, incrementalRangeValue.upper);//��� ��� Oy
		break;
	}


	qCustomPlot->replot();
}

void TelemetryPlot::addGraph(QString name)
{


	qCustomPlot->addGraph();
	//qCustomPlot->graph(0)->setData(x, y);
	qCustomPlot->graph()->setLineStyle(QCPGraph::lsStepLeft);
	if (name.isEmpty())
		name = tr("Graph %1").arg(qCustomPlot->graphCount());

	qCustomPlot->graph()->setName(name);

	QPen pen = QPen(colorList.at(qCustomPlot->graphCount() % colorList.size()));
	pen.setWidth(3);
	qCustomPlot->graph()->setPen(pen);
	//qCustomPlot->legend
}

void TelemetryPlot::createGui()
{



	qCustomPlot = new QCustomPlot();
	qCustomPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom /*| QCP::iSelectPlottables*/);
	qCustomPlot->clearGraphs();

	qCustomPlot->setPlottingHint(QCP::phFastPolylines, true);

	qCustomPlot->legend->setVisible(true);
	//qCustomPlot->legend->setFont(QFont("Helvetica", 9));


	qCustomPlot->xAxis->setLabel(xName);
	qCustomPlot->yAxis->setLabel(yName);

	//setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	//qCustomPlot->setMinimumHeight(100);
	//qCustomPlot->setMinimumWidth(1000);

	qCustomPlot->replot();

	connect(qCustomPlot, &QCustomPlot::plottableClick, [this](QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *mouseEvent)
	{
		if (mouseEvent->button() != Qt::MouseButton::LeftButton)
			return;
		for (size_t graphIndex = 0; graphIndex < qCustomPlot->graphCount(); graphIndex++)
		{
			auto graph = qCustomPlot->graph(graphIndex);
			QCPItemTracer *groupTracer = new QCPItemTracer(qCustomPlot);
			auto data = graph->data();
			data->at(dataIndex)->mainKey();

			groupTracer->setGraph(graph);
			groupTracer->setGraphKey(data->at(dataIndex)->mainKey());
			groupTracer->setInterpolating(true);
			groupTracer->setStyle(QCPItemTracer::tsCircle);
			groupTracer->setPen(QPen(Qt::green));
			groupTracer->setBrush(Qt::green);
			groupTracer->setSize(7);

			bool ok;
			auto rangeValue = graph->getValueRange(ok);
			auto rangeKey = graph->getKeyRange(ok);
			//qCustomPlot->xAxis->setRange(rangeKey.lower, rangeKey.upper);//��� ��� Ox
			//qCustomPlot->yAxis->setRange(rangeValue.lower, rangeValue.upper);//��� ��� Oy

			QCPItemText *dispersionText = new QCPItemText(qCustomPlot);
			dispersionText->position->setCoords(data->at(dataIndex)->mainKey(), data->at(dataIndex)->mainValue() + (rangeValue.upper - rangeValue.lower) * 2 / 100.0);
			dispersionText->setRotation(0);
			//qCustomPlot->graph(0)->get
			dispersionText->setText(QString::fromLocal8Bit("%1").arg(data->at(dataIndex)->mainValue()));
			//dispersionText->setFont(QFont(font().family(), 10));
			dispersionText->setFont(QFont("Arial", 10));
			itemsList << groupTracer;
			itemsList << dispersionText;
		}

		replot();
	});
	//qCustomPlot->xAxis->setRange(x.first(), x.last());//��� ��� Ox
	//double minY = *std::min_element(y.begin(), y.end());
	//double maxY = *std::max_element(y.begin(), y.end());
	//qCustomPlot->yAxis->setRange(minY, maxY);//��� ��� Oy

	connect(qCustomPlot, &QCustomPlot::mousePress, [this](QMouseEvent *mouseEvent)
	{
		if (mouseEvent->button() != Qt::MouseButton::RightButton)
			return;

		QMenu menu(this);
		auto action = new QAction(tr("Pause"), this);
		action->setCheckable(true);
		action->setChecked(isReplotable);
		//action->setShortcuts(QKeySequence::Cut);
		//action->setStatusTip(tr("Cut the current selection's contents to the clipboard"));

		connect(action, &QAction::triggered, [this]()
		{
			isReplotable = !isReplotable;

		});

		menu.addAction(action);

		auto rangeTypeMenu = menu.addMenu(tr("RangeType"));

		action = new QAction(tr("DynamicRangeType"), this);
		action->setCheckable(true);
		action->setChecked((currentRangeType == RangeType::DynamicRangeType));
		connect(action, &QAction::triggered, [this]()
		{
			currentRangeType = RangeType::DynamicRangeType;
		});
		rangeTypeMenu->addAction(action);

		action = new QAction(tr("IncrementalRangeType"), this);
		action->setCheckable(true);
		action->setChecked((currentRangeType == RangeType::IncrementalRangeType));
		connect(action, &QAction::triggered, [this]()
		{
			currentRangeType = RangeType::IncrementalRangeType;
		});
		rangeTypeMenu->addAction(action);

		action = new QAction(tr("StaticRangeType"), this);
		action->setCheckable(true);
		action->setChecked((currentRangeType == RangeType::StaticRangeType));
		connect(action, &QAction::triggered, [this]()
		{
			currentRangeType = RangeType::StaticRangeType;
		});
		rangeTypeMenu->addAction(action);

		for (size_t graphIndex = 0; graphIndex < qCustomPlot->graphCount(); graphIndex++)
		{
			auto graph = qCustomPlot->graph(graphIndex);

			auto action = new QAction(graph->name(), this);
			action->setCheckable(true);
			action->setChecked(graph->visible());
			//action->setShortcuts(QKeySequence::Cut);
			//action->setStatusTip(tr("Cut the current selection's contents to the clipboard"));

			connect(action, &QAction::triggered, [this, graph]()
			{
				graph->setVisible(!graph->visible());
				if (graph->visible())
					graph->addToLegend();
				else
					graph->removeFromLegend();
			});

			menu.addAction(action);
		}


		//auto cutAct = new QAction(tr("Cu&t"), this);
		//cutAct->setShortcuts(QKeySequence::Cut);
		//cutAct->setStatusTip(tr("Cut the current selection's contents to the "
		//	"clipboard"));

		//auto copyAct = new QAction(tr("&Copy"), this);
		//copyAct->setShortcuts(QKeySequence::Copy);
		//copyAct->setStatusTip(tr("Copy the current selection's contents to the "
		//	"clipboard"));

		//auto pasteAct = new QAction(tr("&Paste"), this);
		//pasteAct->setShortcuts(QKeySequence::Paste);
		//pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
		//	"selection"));


		//menu.addAction(cutAct);
		//menu.addAction(copyAct);
		//menu.addAction(pasteAct);

		menu.exec(mouseEvent->globalPos());
		replot();
	});


	setLayout(new QVBoxLayout);

	//setFixedHeight(140);

	//auto tempWidget = new QWidget;
	//tempWidget->setLayout(new QHBoxLayout);
	//tempWidget->layout()->setContentsMargins(0, 0, 0, 0);
	//layout()->addWidget(&stateInfoLabel);
	layout()->addWidget(qCustomPlot);

	//stateInfoLabel.setTextFormat(Qt::RichText);
	//tempWidget->layout()->addWidget(qCustomPlot);
	//tempWidget->layout()->addWidget(&dangerInfoLabel);
	//tempWidget->layout()->addWidget(&identificationInformationLabel);
	//tempWidget->layout()->addWidget(&rlPacketStructureLabel);




	//updateCurrentModeData();
	//updateSetIdentificationInformationData();
	//updateData();

#if 0
	connect(&dataConverter->currentMode, &BaseDataStorage::readyData, this, &InfoWidgetRelease::updateCurrentModeData, Qt::QueuedConnection);
	//connect(&dataConverter->rlStruct, &BaseDataStorage::readyData, this, &InfoWidgetRelease::updateRlPacketStructureData, Qt::QueuedConnection);
	connect(&dataConverter->identificationInfo, &BaseDataStorage::readyData, this, &InfoWidgetRelease::updateSetIdentificationInformationData, Qt::QueuedConnection);
	connect(&dataConverter->analyzeData, &BaseDataStorage::readyData, this, &InfoWidgetRelease::updateDangerInfoData, Qt::QueuedConnection);
#else

	//connect(&dataConverter->identificationInfo, &BaseDataStorage::readMyData, this, &InfoWidgetRelease::updateData, Qt::QueuedConnection);
#endif
}