#include "qtlogger.h"

#include "qdebug.h"


//class QtLogger;

//static QtLogger* qtLoggerPointer = nullptr;
QtLogger* qtLogger = nullptr;


static const char *priNameShort[] =
{
	"NAN ",
	"EMRG",
	"ALRT",
	"CRIT",
	"ERR ",
	"WARN",
	"NOTE",
	"INFO",
	"DBUG",
	0
};

Q_DECLARE_METATYPE(va_list);


QtLogger::QtLogger(QString filename)
{
	this->moveToThread(new QThread);
	
	qRegisterMetaType<va_list>("va_list");


	QFile file1(filename + ".backup");
	if (file1.exists())
		file1.remove();
	file1.close();

	file1.setFileName(filename);
	file1.open(QIODevice::ReadWrite);
	if (file1.exists())
		file1.rename(filename + ".backup");
	file1.close();

	logFile.setFileName(filename);
	logFile.open(QIODevice::WriteOnly | QIODevice::Text);
	stream.setDevice(&logFile);

	logRecordCntr = 0;

	connect(this, &QtLogger::vlog_impl_, this, &QtLogger::append, Qt::QueuedConnection);
	this->thread()->start();
	this->thread()->setPriority(QThread::LowestPriority);
}


QtLogger::~QtLogger()
{
	
	this->thread()->quit();
	this->thread()->wait();
	logFile.flush();
}

void QtLogger::append(QDateTime time, int pri, const char *file, int line, const char *func, QString text)
{
	//static QString prefix, timestring, text;
	//text.clear();
	//text.vsprintf(fmt, ap);
	//qDebug() << daterTime.toString() << text;




	//QDateTime time = QDateTime::currentDateTime();
	static QString prefix, timestring, text1;
	prefix.clear();
	timestring.clear();
	//text.clear();
	timestring = time.toString(QString("dd.MM.yyyy-hh:mm:ss.zzz"));

	prefix.sprintf(
		"%10.10d %s %s: ",
		(logRecordCntr++),//% 100000000,
		timestring.toLatin1().constData(),
		priNameShort[pri]);

	static QString at;
	at.clear();
	if ((file && line) && func)
	{
		//QString at;
		at.sprintf("%s:%d %s: ", file, line, func);
		prefix.append(at);
	}
	else
	if (func)
	{
		//QString at;
		at.sprintf("%s: ", func);
		prefix.append(at);
	}


#if 0
	QTextCodec *c = QTextCodec::codecForName(/*"KOI8-U"*/ /*"IBM 866"*/ /*"KOI8-R"*/ "Windows-1251");
	QTextCodec::setCodecForLocale(c);

	QByteArray encodedString = c->fromUnicode(fmt);
	QString sss = c->toUnicode(fmt);
#endif /* 0 */

	//QString ss = QString::fromLocal8Bit(fmt);

	//text.vsprintf(fmt, ap);

#if 0
	text.prepend(prefix);
	text.append("\n");
	//stream << text << "\n";
	logFile.write(qPrintable(text));
#else
	prefix.append(text);
	emit readyString(prefix);
	prefix.append("\n");
	//stream << text << "\n";
	logFile.write(qPrintable(prefix));	
	//logFile.flush();
#endif
	//qDebug() << text;

	//if (logFile)
	//{
	//	//fprintf(logFile, prefix.toLatin1().constData());
	//	fprintf(logFile, qPrintable(text));
	//	fprintf(logFile, "\n");
	//	fflush(logFile);
	//}

}

void log_impl_(int pri, const char *file, int line, const char *func, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	//qtLogger->append(QDateTime::currentDateTime(), pri, file, line, func, fmt, ap);
	QString text;
	text.vsprintf(fmt, ap);
	if (qtLogger!=nullptr)
	emit qtLogger->vlog_impl_(QDateTime::currentDateTime(), pri, file, line, func, text);
	va_end(ap);
}


int logOpen(QString filename, bool keep_stderr)
{
	if (qtLogger == nullptr)
		qtLogger = new QtLogger(filename);

	//logFileName = filename;
	//keepStderr = keep_stderr;
	//return openLogFile();
	return 0;
}

int logClose()
{
	log_debug("logClose");
	//fclose(logFile);
	
	if (qtLogger != nullptr)
	{
		//qtLogger->thread()->wait();
		qtLogger->deleteLater();
		//delete qtLogger;
	}
	qtLogger = nullptr;
	return 0;
}
