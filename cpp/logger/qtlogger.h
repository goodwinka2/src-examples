#pragma once


#include <stdarg.h>

#define SIZE_PER_LOGFILE                (1024) /* in bytes */
#define NUMBER_OF_BACKUP_FILES_TO_KEEP  (10)

#include <QDateTime>
#include <QObject>
#include <QThread>
#include <QString>
#include <QFile>
#include <QTextStream>


class QtLogger :public QObject
{
	Q_OBJECT
public:
	QtLogger(QString filename);
	~QtLogger();
	void append(QDateTime, int pri, const char *file, int line, const char *func, QString text);

signals:
	void vlog_impl_(QDateTime, int pri, const char *file, int line, const char *func, QString text);
	void readyString(QString);
private:
	unsigned int logRecordCntr;
	QFile logFile;
	QTextStream stream;
};

extern QtLogger* qtLogger;



enum LogPriorityLevel
{
LOG_NAN,
LOG_EMERG,  
LOG_ALERT,
LOG_CRIT,
LOG_ERROR,
LOG_WARNING,
LOG_NOTICE,
LOG_INFO,
LOG_DEBUG,
};



//#ifdef __cplusplus
//extern "C" {
//#endif

	/*
	* Generate a log message using a printf-style format string and option arguments.
	* This function should not be called directly; it exists only as a helper function
	* for the cpLog macro, which in turn is defined only if we are using the GNU
	* C compiler (i.e. if the preprocessor macro __GNUC__ is defined).
	*
	*   pri  - the priority to assign to this message
	*   file - the file that contains the code that is calling cpLog
	*   line - the specific line in the file where cpLog is being called
	*   fmt  - a printf-style format string
	*   ...  - any number of additional values to substitute into the string,
	*          according to printf rules
	*/
void log_impl_(int pri, const char *file, int line, const char *func, const char *fmt, ...);
	

//#define log_prio(priority__, fmt__,...) \
//	log_impl_(priority__, __FILE__, __LINE__, __FUNCTION__, fmt__, __VA_ARGS__); 

#define log_info(fmt__,...) \
	log_impl_(LogPriorityLevel::LOG_INFO, 0, 0, __FUNCTION__, fmt__, __VA_ARGS__);


#define log_debug(fmt__,...) \
	log_impl_(LogPriorityLevel::LOG_DEBUG, 0, 0, __FUNCTION__, fmt__, __VA_ARGS__);


#define log_warn(fmt__,...) \
	log_impl_(LogPriorityLevel::LOG_WARNING, 0, 0, __FUNCTION__, fmt__, __VA_ARGS__);


#define log_error(fmt__,...) \
	log_impl_(LogPriorityLevel::LOG_ERROR, 0, 0, __FUNCTION__, fmt__, __VA_ARGS__);
	

extern int logClose();
extern int logOpen(QString filename,bool keep_stderr = false);




//#ifdef __cplusplus
//}
//#endif




