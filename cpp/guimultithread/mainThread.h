
#pragma once

#include "qapplication.h"

#include "qlist.h"
#include "qmutex.h"

#include <QMutexLocker>

#include "qmessagebox.h"
#include "qthread.h"
#include "qwaitcondition.h"
#include "qdebug.h"
#include "qfiledialog.h"
#include "qwindow.h"
//#include "qwebview.h"

class GuiFabricCaller;
class GuiFabric;

extern GuiFabricCaller *guiFabricCaller;



#define GuiCallerClasses	GuiFabricCaller *guiFabricCaller = nullptr;//	MainThreadCalc *mainThreadCalc = nullptr;\


class GuiFabric :public QObject
{
	Q_OBJECT

public:
	GuiFabric(GuiFabricCaller *caller)
	{
		guiFabricCaller = caller;
	};

	void createMessageBox(QString *title, QString *text, QString *detailedText, QWaitCondition *waitThread,int *result, bool wait)
	{
		
		QMessageBox messageBox;
		messageBox.setWindowTitle(*title);
		messageBox.setText(*text);
		messageBox.setDetailedText(*detailedText);
		messageBox.show();

		if (wait == true)
		{
			*result = messageBox.exec();
			waitThread->wakeAll();
		}
		else
		{
			waitThread->wakeAll();
			messageBox.exec();			
		}
	
	}

	void createFileDialog(QString *title, QString *filter, QWaitCondition *waitThread, QString *result, bool wait)
	{

		QFileDialog fileDialog;
		

		if (wait == true)
		{
			*result = fileDialog.getOpenFileName(0, *title, QDir::currentPath(), *filter);
			waitThread->wakeAll();
		}
		else
		{
			QString dialogTitle = *title;
			QString dialogFilter = *filter;

			waitThread->wakeAll();
			fileDialog.getOpenFileName(0, dialogTitle, QDir::currentPath(), dialogFilter);
		}


	}

	void createHtmlViewer(QString *html, QWaitCondition *waitThread, QString *result, bool wait)
	{

		


		if (wait == true)
		{
			*result = "nope";//fileDialog.getOpenFileName(0, *title, QDir::currentPath(), *filter);
			waitThread->wakeAll();
		}
		else
		{
			//QString dialogTitle = *title;

#if 0
			QString *htmlText = new QString(*html);
			QWebView *view = new QWebView();
			//view->load(QUrl("http://portal/Pages/main.aspx/"));
			view->setHtml(*htmlText);
			view->show();
#endif
			waitThread->wakeAll();
			*result = "yep";
			//fileDialog.getOpenFileName(0, dialogTitle, QDir::currentPath(), dialogFilter);
		}


	}
	
signals:
	void ready();

private:
	GuiFabricCaller *guiFabricCaller;
	//template<class T>
	//T *ui;
};

class GuiFabricCaller:public QObject
{
	Q_OBJECT

public:

	GuiFabricCaller()	
	{	
		guiFabric = new GuiFabric(this);
		guiFabric->moveToThread(qApp->thread());
		this->moveToThread(&callerThread);
		callerThread.start();
		callerThread.setPriority(QThread::LowestPriority);

		connect(this, &GuiFabricCaller::messageBox, guiFabric, &GuiFabric::createMessageBox);// , Qt::BlockingQueuedConnection);
		connect(this, &GuiFabricCaller::fileDialog, guiFabric, &GuiFabric::createFileDialog);
		connect(this, &GuiFabricCaller::htmlView, guiFabric, &GuiFabric::createHtmlViewer);
	};
	~GuiFabricCaller()
	{
		//guiFabric->deleteLater();
		//guiFabric->moveToThread(this->thread());
		//delete guiFabric;
	};


	QMessageBox::StandardButton showMessageBox(QString title = QString("Message"), QString text = QString(), QString detailedText = QString(), bool waitForResult = true)
	{
		QMutex mutex;
		mutex.lock();
		
		int result = 0;
		QWaitCondition waitThread;

		emit messageBox(&title, &text, &detailedText, &waitThread, &result, waitForResult);
					
		waitThread.wait(&mutex);
		mutex.unlock();

		qDebug() << "messageBox return: "<<result;
		return (QMessageBox::StandardButton)result;
	}

	QString showFileDialog(QString title, QString filter, bool waitForResult = true)
	{
		QMutex mutex;
		mutex.lock();
		
		QString result;
		QWaitCondition waitThread;

		emit fileDialog(&title, &filter, &waitThread, &result, waitForResult);

		if (qApp->thread() != QThread::currentThread())
		waitThread.wait(&mutex);		
		mutex.unlock();

		qDebug() << "fileDialog return: " << result;
		return result;
	}

	QString showHtml(QString html, bool waitForResult = false)
	{
		QMutex mutex;
		mutex.lock();

		QString result;
		QWaitCondition waitThread;

		emit htmlView(&html, &waitThread, &result, waitForResult);

		waitThread.wait(&mutex);
		mutex.unlock();

		qDebug() << "htmlView return: " << result;
		return result;
	}

signals:

	void messageBox(QString *title, QString *text, QString *detailedText, QWaitCondition *waitThread, int *result, bool wait);
	void fileDialog(QString *title, QString *filter, QWaitCondition *waitThread, QString *result, bool wait);
	void htmlView(QString *, QWaitCondition *waitThread, QString *result, bool wait);

private:
	QThread callerThread;
	GuiFabric *guiFabric;
};


inline void initializeGuiCallers()
{
	//mainThreadCalc = new MainThreadCalc();
	//mainThreadCalc->moveToThread(qApp->thread());
	guiFabricCaller = new GuiFabricCaller();
}

inline void clearGuiCallers()
{
	//mainThreadCalc = new MainThreadCalc();
	//mainThreadCalc->moveToThread(qApp->thread());
	//delete guiFabricCaller;
	guiFabricCaller->deleteLater();
}




#if 0
extern MainThreadCalc *mainThreadCalc;
inline void callFromMainThread(void (*func)()){
	mainThreadCalc->callFunction(func);
}
class MainThreadCalc : public QObject
{
	Q_OBJECT


signals :
	void calcEnd();
	void callFunctionFromList(int);

public:

	MainThreadCalc()
	{
		connect(this, &MainThreadCalc::callFunctionFromList, this, &MainThreadCalc::doFunction);
	}

	void callFunction(void(*function)())
	{
		QMutexLocker locker(&listMutex);

		int index = functionList.indexOf(function);

		if (index == -1)
		{
			functionList << function;
			index = functionList.indexOf(function);
		}


		emit callFunctionFromList(index);
	}

private:

	void doFunction(int i)
	{
		if (i > functionList.size() - 1)
			return;

		functionList[i]();
	}

	int append(void(*function)())
	{
		//QMutexLocker locker(&listMutex);
		functionList << function;
		return functionList.size() - 1;
	}
private:

	QList <void(*)()> functionList;
	QMutex	listMutex;

};
#endif