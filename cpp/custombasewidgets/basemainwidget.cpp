#include "basemainwidget.h"
#include <QLabel>
#include <QCheckBox>
#include "qdebug.h"

BaseMainWidget::BaseMainWidget()
{
	setLayout(new QHBoxLayout);
	layout()->addWidget(&mainWidget);
	layout()->addWidget(&subWidget);
	mainWidget.setLayout(new QVBoxLayout);
	subWidget.setLayout(new QVBoxLayout);
	subWidget.setFixedWidth(400);

	layout()->setContentsMargins(0, 0, 0, 0);
	mainWidget.layout()->setContentsMargins(0, 0, 0, 0);
	subWidget.layout()->setContentsMargins(0, 0, 0, 0);
}

BaseMainWidget::~BaseMainWidget()
{
}

void BaseMainWidget::appendNewWidget(QWidget* widget, QString key)
{
	//widgetList << widget;


	auto button = new QPushButton;
	connect(button, &QPushButton::clicked, [this, button]()
	{
		if (button->parent() == &mainWidget)
		{
			qDebug() << "button->parent()";
			return;
		}
		auto tempWidget = mainWidget.layout()->takeAt(0)->widget();
		//button->setEnabled(false);
		//button->layout()->itemAt(0)->widget()->setEnabled(true);
		//button->setDown(true);
		//button->setFlat(true);
		//((QPushButton*)tempWidget)->setFlat(false);
		//((QPushButton*)tempWidget)->setDown(false);
		//tempWidget->setEnabled(true);
		tempWidget->layout()->itemAt(0)->widget()->setAttribute(Qt::WA_TransparentForMouseEvents, true);
		subWidget.layout()->addWidget(tempWidget);
		mainWidget.layout()->addWidget(button);
		mainWidget.layout()->itemAt(0)->widget()->layout()->itemAt(0)->widget()->setAttribute(Qt::WA_TransparentForMouseEvents, false);
	});
	
	button->setLayout(new QVBoxLayout);
	button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	button->layout()->setContentsMargins(0, 0, 0, 0);
	//button->setCheckable(true);
	//button->setEnabled(true);
	//button->setDown(true);
	auto contentWidget = new QWidget;
	contentWidget->setLayout(new QVBoxLayout);

	if (!key.isNull())
	contentWidget->layout()->addWidget(new QLabel(key));
	
	widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	//auto glass = new QLabel("GLASS");
	//glass->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	//glass->setParent(widget);	
	//glass->setStyleSheet("background-color: rgba(255,0,0,140);");

	//contentWidget->layout()->addWidget(glass);
	contentWidget->layout()->addWidget(widget);
	contentWidget->layout()->setContentsMargins(0, 0, 0, 0);
	contentWidget->setContentsMargins(5, 5, 5, 5);
	//button->layout()->addWidget(widget);
	button->layout()->addWidget(contentWidget);
	//widget->stackUnder(button);
	button->layout()->setAlignment(Qt::AlignCenter);
	buttonWidgetList << button;
	contentWidget->setAttribute(Qt::WA_TransparentForMouseEvents, true);

	subWidget.layout()->addWidget(button);
	mainWidget.layout()->addWidget(buttonWidgetList.first());
	mainWidget.layout()->itemAt(0)->widget()->layout()->itemAt(0)->widget()->setAttribute(Qt::WA_TransparentForMouseEvents, false);



	if (key.isNull())
		key = QString("widget_%1").arg(widgetsMap.size());
	widgetsMap.insert(key, button);

	showWidgetList << key;
}

void BaseMainWidget::repaintWidgets()
{

}

void BaseMainWidget::changeShownWidget()
{
	//

	auto controlWidget = new QWidget();
	controlWidget->show();
	controlWidget->setWindowTitle(tr("Widget control view"));
	auto mainLayout = new QVBoxLayout();
	controlWidget->setLayout(mainLayout);
	controlWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	//controlWidget->setFixedSize(300, 140);
	controlWidget->setFixedWidth(300);
	
	//totlaLabel->setText(tr("total:%1").arg(totalSize));

	//auto currentLabel = new QLabel();

	for (auto &var : widgetsMap.keys())
	{
		auto checkBox = new QCheckBox(var);
		checkBox->setChecked(showWidgetList.contains(var));
		//checkBox->setFixedHeight(80);
		mainLayout->addWidget(checkBox);
		connect(checkBox, &QCheckBox::stateChanged, [this,var](int state)
		{
			if (state == Qt::Checked)
			{
				if (!showWidgetList.contains(var))
					showWidgetList << var;
			}
			else
			{
				if (showWidgetList.indexOf(var) != -1)
					showWidgetList.removeAt(showWidgetList.indexOf(var));
			}
		});
	}



	connect(controlWidget, &QWidget::destroyed, [this, controlWidget]()
	{

		for (auto &var : widgetsMap)
		{
			var->setVisible(false);	
			subWidget.layout()->addWidget(var);
		}

		for (auto &var : showWidgetList)
		{
			qDebug() << var;

			subWidget.layout()->addWidget(widgetsMap.value(var));
			//widgetsMap.value(var)->show();
			widgetsMap.value(var)->setVisible(true);
			//auto checkBox = new QCheckBox(var);
			//checkBox->setChecked(showWidgetList.contains(var));
			//mainLayout->addWidget(checkBox);
		}
		if (!showWidgetList.isEmpty())
		mainWidget.layout()->addWidget(widgetsMap.value(showWidgetList.first()));
	});

	//connect(controlWidget, &QWidget::destroyed, [this](){controlWidget = nullptr; });
	//connect(controlWidget, &QWidget::destroyed, &dataLogger, &DataLogger::stopLog);


	controlWidget->setAttribute(Qt::WA_DeleteOnClose, true);
	controlWidget->setWindowFlags(Qt::WindowStaysOnTopHint);
	controlWidget->show();
	controlWidget->activateWindow();

}