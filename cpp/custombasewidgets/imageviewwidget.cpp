#include "imageviewwidget.h"


ImageViewWidget::ImageViewWidget()
{
	//setMinimumSize(1, 1);
	//setPixmap(QPixmap::fromImage(image));
	setScaledContents(true);
	//QSizePolicy policy = QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
	//policy.setHeightForWidth(true);
	//setSizePolicy(policy);
	
	pixmapWidth = 0;
	pixmapHeight = 0;
	//setImage(image);
}


ImageViewWidget::~ImageViewWidget()
{
}


void ImageViewWidget::setImage(QImage &image)
{
	//pixmap = QPixmap::fromImage(image);
	//setPixmap(scaledPixmap());
	pixmapWidth = image.width();
	pixmapHeight = image.height();
	updateMargins();
	setPixmap(QPixmap::fromImage(image));
}

QPixmap ImageViewWidget::scaledPixmap()
{
	return pixmap()->scaled(size(), Qt::KeepAspectRatio,Qt::SmoothTransformation);
}

void ImageViewWidget::resizeEvent(QResizeEvent *e)
{
	updateMargins();
	QLabel::resizeEvent(e);
	//if (!pixmap()->isNull())
	//setPixmap(scaledPixmap());
}

void ImageViewWidget::updateMargins()
{
	if (pixmapHeight == 0 || pixmapWidth == 0)
		return;
	
	int w = width();
	int h = height();
	int d = 5;
	if (w == 0 || h == 0)
		return;
	if (w * pixmapHeight > h *pixmapWidth)
	{
		int m = (w - (h *pixmapWidth / pixmapHeight)) / 2;
		setContentsMargins(m+d, d, m+d, d);
	}
	else
	{
		int m = (h - (w * pixmapHeight / pixmapWidth)) / 2;
		setContentsMargins( d, m+d, d,m+d);
	}
}

//int ImageViewWidget::heightForWidth(int i)
//{
//	//pixmap()
//	if (width() > pixmap()->width())
//	{
//		return pixmap()->height();
//	}
//	else
//	{
//		return ((qreal)pixmap()->height()*width()) / pixmap()->width();
//	}
//}