#pragma once
#include <QWidget>

#include <QList>
#include <QLayout>
#include <QPushButton>
#include <QString>
#include <QMap>

class BaseMainWidget:public QWidget
{
	Q_OBJECT
public:
	BaseMainWidget();
	~BaseMainWidget();

	void appendNewWidget(QWidget*,QString key = QString());
	void changeShownWidget();
	void repaintWidgets();

private:

	QList<QString> showWidgetList;
	QMap<QString, QWidget*> widgetsMap;
	QList<QPushButton *> buttonWidgetList;
	QWidget mainWidget;
	QWidget subWidget;
};

