#pragma once

#include <QLabel>
#include <QImage>
#include <QPixMap>

class ImageViewWidget:public QLabel
{
	Q_OBJECT
public:
	ImageViewWidget();
	~ImageViewWidget();

	void setImage(QImage &image);
	QPixmap scaledPixmap();
	void resizeEvent(QResizeEvent *);

	//bool hasHeightForWidth(){ return true; }
	//QSize sizeHint(){ return pixmap()->size(); };
	//QSize minimumSizeHint(){ return QSize(0,0); };
	int heightForWidth(int i);
private:
	void updateMargins();
	//QPixmap pixmap;
	int pixmapWidth;
	int pixmapHeight;
};

