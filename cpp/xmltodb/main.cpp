#include "xmldataviewer.h"
#include <QtWidgets/QApplication>
#include "xmldataviewerexception.h"
#include <QDebug>
#include <QMessageBox>
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	int result = 0;
	try
	{
		XmlDataViewer w;
		w.show();
		result = a.exec();
	}
	catch (XmlDataViewerException &e)
	{
		QMessageBox::critical(0, QObject::tr("Unhandled exception"), QObject::tr("Unhandled XmlDataViewerException %1\n%2")
			.arg(e.exeption())
			.arg(e.what()) );		
	}
	
	return result;
}
