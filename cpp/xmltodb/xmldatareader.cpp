#include "xmldatareader.h"
#include <QAbstractTableModel>
#include <QDebug>
#include <QApplication>
#include <QFile>
#include <QDir>
#include <QXmlStreamReader>
#include "xmldataviewerexception.h"

#include <QDomDocument>
#include <QTextStream>


XmlDataReader::XmlDataReader()
{
	moveToThread(new QThread);
	thread()->start();
}

XmlDataReader::~XmlDataReader()
{
}

void XmlDataReader::setXmlDataTablePointer(QSharedPointer<XmlDataTable> pointer)
{
	xmlData = pointer;
}

void XmlDataReader::saveXml(QString pathName)
{
	for (size_t i = 0; i < xmlData.data()->rowCount(QModelIndex()); i++)
	{


		QDomDocument doc("Xml");
		QDomElement root = doc.createElement("root");
		doc.appendChild(root);

		for (size_t j = 1; j < xmlData.data()->columnCount(QModelIndex()); j++)
		{
			QDomElement tag = doc.createElement(xmlData.data()->headerData(j,Qt::Horizontal,0).toString());
			root.appendChild(tag);

			QDomText t = doc.createTextNode(xmlData.data()->data(xmlData.data()->index(i,j),0).toString());
			tag.appendChild(t);
		}


		QString xml = doc.toString();

		QFile file(pathName +"\\" + QString("data_%1.xml").arg(i));
		if (!file.open(QFile::WriteOnly | QFile::Text))
		{
			//xmlFilereadErrors << tr("Can't open file %1").arg(fileInfo.fileName());
			continue;
			//return;
		}
		QTextStream stream(&file);
		doc.save(stream,0);
	}

	qDebug() << "";
}

void XmlDataReader::loadXml(QString pathName)
{
	if (qApp->thread() == QThread::currentThread())
		throw XmlDataViewerException(XmlDataViewerException::CALC_IN_GUI_THREAD, "XmlDataReader::loadXml");
		
	emit readyPercent(0);

	//thread()->msleep(100);
	//emit readyPercent(50);
	//thread()->msleep(100);
	
	QStringList xmlFilereadErrors;
	//xmlFilereadErrors << "test error";
	QDir dir(pathName);
	
	QFileInfoList dirContent = dir.entryInfoList(QStringList()	<< "*.xml",	QDir::Files );
	for (int i = 0; i < dirContent.size(); ++i) 
	{
		QFileInfo fileInfo = dirContent.at(i);
		//qDebug() << qPrintable(QString("%1 %2").arg(fileInfo.size(), 10).arg(fileInfo.fileName())); 
	}
	
	QStringList text;

	for (int i = 0; i < dirContent.size(); ++i) 
	{
		QFileInfo fileInfo = dirContent.at(i);
		QFile file(pathName +"\\"+ fileInfo.fileName());
		if (!file.open(QFile::ReadOnly | QFile::Text))
		{
			xmlFilereadErrors << tr("Can't open file %1").arg(fileInfo.fileName());
			continue;
		}

		QDomDocument doc("mydocument");
		if (!doc.setContent(&file)) 
		{
			xmlFilereadErrors << tr("Can't setContent %1").arg(fileInfo.fileName());
			file.close();
			continue;
		}
		file.close();

		QDomElement docElem = doc.documentElement();
		QDomNode n = docElem.firstChild();
		QList<QPair<QString, QString>> pairList;
		while(!n.isNull()) 
		{
			QDomElement e = n.toElement();
			if(!e.isNull()) 
			{
				pairList << QPair<QString, QString>(e.tagName(), e.text());
				//qDebug() << e.tagName() << "\t"<< e.text(); 
			}
			n = n.nextSibling();
		}
		if (!pairList.isEmpty())
		{
			xmlData.data()->insertRow(0, QModelIndex());
			int newId = xmlData.data()->rowCount(QModelIndex()) - 1;
			auto head = xmlData.data()->headerData(0, Qt::Horizontal, 0);
			xmlData.data()->setData(newId, head, QVariant(QString("%1").arg(newId)), 0);
			for (auto var : pairList)
			{				
				xmlData.data()->setHeaderData(0, Qt::Horizontal, var.first, 0);
				//auto head = xmlData.data()->headerData(i, Qt::Horizontal, 0);
				xmlData.data()->setData(newId, QVariant(var.first), QVariant(var.second), 0);
				
			}
		}
		else
			xmlFilereadErrors << tr("File %1 is empty").arg(fileInfo.fileName());

		emit readyPercent(dirContent.size()*100/(i+1));
	}

	emit errors(xmlFilereadErrors);
	
	emit readyXmlData(xmlData);
	
	emit readyPercent(100);
}

