#pragma once
#include <QAbstractTableModel>
#include <QList>
#include <QMap>
class XmlDataTable :public QAbstractTableModel
{
	
public:
	XmlDataTable();
	~XmlDataTable();

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	bool insertRows(int position, int rows, const QModelIndex &index);
	bool setData(const QModelIndex &index, const QVariant &value, int role);
	bool setData(int,const QVariant&, const QVariant &value, int role);
	bool setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role = Qt::EditRole);
	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent) const;
	int columnCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role) const;

private:
	QList<QVariant> header;
	QList<QMap<QVariant, QVariant>> rowData;
	
};

