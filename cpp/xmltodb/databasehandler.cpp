#include "databasehandler.h"


#include <QThread>
#include "xmldataviewerexception.h"



DatabaseHandler::DatabaseHandler()
{
	xmlData = QSharedPointer<XmlDataTable>(new XmlDataTable());
	
	tableName = "testDatabase";


	moveToThread(new QThread);
	thread()->start();
}


DatabaseHandler::~DatabaseHandler()
{
	if (dbase.isOpen())
	dbase.close();
}

void DatabaseHandler::loadData()
{
	if (qApp->thread() == QThread::currentThread())
		throw XmlDataViewerException(XmlDataViewerException::CALC_IN_GUI_THREAD, "DatabaseHandler::loadData()");
	
	dbase = QSqlDatabase::addDatabase("QSQLITE");
	dbase.setDatabaseName("testApp.sqlite");
	if (!dbase.open())
	{
		throw XmlDataViewerException(XmlDataViewerException::DB_FILE_OPEN_FAIL, dbase.lastError().text());
	}
	
	
	QSqlQuery query;
	QString   str = QString("CREATE TABLE %1 ( "
		"id"
		");").arg(tableName);
	query.exec(str);

	

	if (!query.exec(QString("SELECT * FROM %1;").arg(tableName))) 
		qDebug() << "Unable to execute query";

	
	QSqlRecord rec = query.record();

	for (size_t j = 0; j < rec.count(); j++)
	{
		xmlData.data()->setHeaderData(0, Qt::Horizontal, QVariant(rec.fieldName(j)));
	}

	int i = 0;
	while (query.next()) 
	{		
		xmlData.data()->insertRow(0, QModelIndex());
		for (size_t j = 0;j < rec.count(); j++)
		{
			xmlData.data()->setData(i, QVariant(rec.fieldName(j)), QVariant(query.value(j)), 0);
		}
		i++;		
	}
	

	connect(xmlData.data(), &XmlDataTable::dataChanged, this, &DatabaseHandler::dataChanged,Qt::QueuedConnection);
	emit readyXmlData(xmlData);
	
	
}

void DatabaseHandler::dataChanged(QModelIndex index)
{	
	qDebug() << "DatabaseHandler::dataChanged";
	QSqlQuery query(dbase);
	auto head = xmlData.data()->headerData(index.column(), Qt::Horizontal, 0);
	auto value = xmlData.data()->data(index, 0);

	bool dataInDb = false;
	bool headInDb = false;
	query.exec(QString("PRAGMA table_info(%1)").arg(tableName));
	while (query.next()) 
	{
		//qDebug() << query.value(1);
		if (head == query.value(1))
		{
			headInDb = true;
			break;
		}
	}
	if (!headInDb)
	{
		query.exec(QString("ALTER TABLE %1 ADD COLUMN %2")
			.arg(tableName)
			.arg(head.toString())
			);
	}


	if (query.exec(QString(
		"SELECT %2 FROM %1")		//"SELECT * FROM %1 WHERE %2 = %3")
		.arg(tableName)
		.arg("id")
		))		
	{
		while (query.next())
		{
			auto id = query.value(0);
			if (id == index.row())
			{
				dataInDb = true;
				break;
			}
		}
		qDebug() << "true" << index.row();
	}
	else
	{
		qDebug() << "false";
	}

	if (!dataInDb)
		query.exec(
			QString("INSERT INTO %1 (%2) VALUES('%3')")
			.arg(tableName)
			.arg("id")
			//.arg(head.toString())
			.arg(index.row())
			//.arg(value.toString())
			);
	

	qDebug() << "dbupdate" << query.exec(
		QString("UPDATE %1 SET %2 = '%3' WHERE %4 = '%5'")
		.arg(tableName)
		.arg(head.toString())
		.arg(value.toString())
		.arg("id")
		.arg(index.row())
		);

	qDebug() << index;
}