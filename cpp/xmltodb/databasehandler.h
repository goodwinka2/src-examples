#pragma once


#include <QObject>
#include <QSharedPointer>
#include <QtSql>

#include "xmldatatable.h"

class DatabaseHandler:public QObject
{
	Q_OBJECT
	
public:
	DatabaseHandler();
	~DatabaseHandler();

	void loadData();
	void dataChanged(QModelIndex);
	
signals:
	void readyXmlData(QSharedPointer<XmlDataTable>);

private:
	QSharedPointer<XmlDataTable> xmlData;
	QSqlDatabase dbase;
	QString tableName;
};

