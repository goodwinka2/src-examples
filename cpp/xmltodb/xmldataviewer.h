#ifndef XMLDATAVIEWER_H
#define XMLDATAVIEWER_H

#include <QtWidgets/QWidget>
#include <QMainWindow>
#include  <QLabel>
#include <QTableView>
#include <QProgressDialog>


#include "databasehandler.h"

#include "xmldatatable.h"
#include "xmldatareader.h"

class XmlDataViewer : public QMainWindow//QWidget
{
	Q_OBJECT

public:
	XmlDataViewer(QWidget *parent = 0);
	~XmlDataViewer();

	void lodaTableData(QSharedPointer<XmlDataTable>);
	void workProgress(int);
signals:
	void loadXml(QString);
	void saveXml(QString);
	void loadDbData();
private:
	void errors(QStringList);
	QLabel *statusLabel;
	DatabaseHandler databaseHandler;
	XmlDataReader xmlDataReader;
	QTableView dataTable;
	
	QProgressBar *progressBar;
	QSharedPointer<XmlDataTable> xmlData;
};

#endif // XMLDATAVIEWER_H
