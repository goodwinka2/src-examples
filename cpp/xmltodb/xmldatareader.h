#pragma once
#include <QObject>
#include <QThread>
#include <QSharedPointer>
#include "xmldatatable.h"
class XmlDataReader :
	public QObject
{
	Q_OBJECT
public:
	XmlDataReader();
	~XmlDataReader();

	void setXmlDataTablePointer(QSharedPointer<XmlDataTable>);
	void loadXml(QString);
	void saveXml(QString);
signals:
	void readyXmlData(QSharedPointer<XmlDataTable>);
	void readyPercent(int);
	void errors(QStringList);
private:
	QSharedPointer<XmlDataTable> xmlData;
};

