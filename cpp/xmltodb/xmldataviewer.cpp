﻿#include "xmldataviewer.h"
#include <QMenuBar>
#include  <QStatusBar>
#include  <QLabel>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QProgressBar>
#include <QSpacerItem>
#include <QStyleFactory>
#include <QApplication>

#include <QItemDelegate>
#include <QErrorMessage>

#include "xmldataviewerexception.h"

XmlDataViewer::XmlDataViewer(QWidget *parent)
	: QMainWindow(parent)
{
	qApp->setStyle(QStyleFactory::create("Fusion"));	
	
	progressBar = nullptr;

	resize(800, 600);

	connect(&databaseHandler, &DatabaseHandler::readyXmlData, this, &XmlDataViewer::lodaTableData, Qt::QueuedConnection);	
	connect(&databaseHandler, &DatabaseHandler::readyXmlData, &xmlDataReader, &XmlDataReader::setXmlDataTablePointer, Qt::QueuedConnection);
	connect(this, &XmlDataViewer::loadXml, &xmlDataReader, &XmlDataReader::loadXml, Qt::QueuedConnection);
	connect(this, &XmlDataViewer::saveXml, &xmlDataReader, &XmlDataReader::saveXml, Qt::QueuedConnection);
	connect(this, &XmlDataViewer::loadDbData, &databaseHandler, &DatabaseHandler::loadData, Qt::QueuedConnection);	
	connect(&xmlDataReader, &XmlDataReader::errors, this, &XmlDataViewer::errors, Qt::QueuedConnection);
	//connect(&xmlDataReader, &XmlDataReader::readyXmlData, this, &XmlDataViewer::lodaTableData, Qt::QueuedConnection);

	//databaseHandler.loadData();

	auto fileMenu = menuBar()->addMenu(tr("File"));
	connect(fileMenu->addAction(tr("Import files (*.xml)")), &QAction::triggered, [this]()
	{	
		statusLabel->setText(tr("Load data..."));
		QString dir = QFileDialog::getExistingDirectory(this,
			tr("Choose dir with XML"),
			QDir::currentPath(),
			QFileDialog::ShowDirsOnly
			| QFileDialog::DontResolveSymlinks);

		progressBar = new QProgressBar(this);
		progressBar->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
		progressBar->setFormat(tr("Progress %p%"));
		progressBar->show();
		workProgress(0);
		setEnabled(false);
		progressBar->setEnabled(true);

		connect(&xmlDataReader, &XmlDataReader::readyPercent, this, &XmlDataViewer::workProgress, Qt::QueuedConnection);

		emit loadXml(dir);
		 
	});

	connect(fileMenu->addAction(tr("Export files (*.xml)")), &QAction::triggered, [this]()
	{
		//statusLabel->setText(tr("Save data..."));
		QString dir = QFileDialog::getExistingDirectory(this,
			tr("Choose dir"),
			QDir::currentPath(),
			QFileDialog::ShowDirsOnly
			| QFileDialog::DontResolveSymlinks);

		emit saveXml(dir);

	});


	fileMenu->addSeparator();
	connect(fileMenu->addAction(tr("Exit")), &QAction::triggered, [this]()
	{
		statusLabel->setText("Exit...");// can you see this message? i can't
		close();		
	});

	statusLabel = new QLabel(tr("Program started"));
	
	
	statusBar()->addWidget(statusLabel);		 


	setCentralWidget(new QWidget);
	centralWidget()->setLayout(new QVBoxLayout);
	centralWidget()->layout()->addWidget(&dataTable);

	emit loadDbData();
}


XmlDataViewer::~XmlDataViewer()
{
	disconnect();
	databaseHandler.thread()->quit();
	xmlDataReader.thread()->quit();
	databaseHandler.thread()->wait();
	xmlDataReader.thread()->wait();


	if (progressBar != nullptr)
		delete progressBar;

}

void XmlDataViewer::workProgress(int percent)
{
	if (progressBar == nullptr)
		return;
		//throw XmlDataViewerException(XmlDataViewerException::GUI_FAIL, "progressBar == nullptr");

	progressBar->setValue(percent);

	if (percent == 100)
	{
		disconnect(&xmlDataReader, &XmlDataReader::readyPercent, this, &XmlDataViewer::workProgress);
		setEnabled(true);
		delete progressBar;
		progressBar = nullptr;
		dataTable.update();
		statusLabel->setText(tr("Data is ready"));
	}
}

void XmlDataViewer::lodaTableData(QSharedPointer<XmlDataTable> newXmlData)
{
	xmlData = newXmlData;	
	dataTable.clearSpans();//reset();
	dataTable.setModel(xmlData.data());
	dataTable.setEditTriggers(QAbstractItemView::AllEditTriggers);
	dataTable.update();
	dataTable.repaint();
	//auto itemDelegate = new QItemDelegate(&dataTable);
	//dataTable.setItemDelegate(itemDelegate);
	dataTable.resizeRowsToContents();
	statusLabel->setText(tr("Data is ready"));
}

void XmlDataViewer::errors(QStringList errorList)
{
	if (errorList.isEmpty())
		return;
	QString result;
	//result.append(tr("Errors count:%1 \n").arg(errorList.count()));
	result.append(errorList.join("\n\n"));

	QMessageBox msgBox;
	msgBox.setWindowTitle(tr("Error"));
	msgBox.setText(tr("Errors count:%1").arg(errorList.count()));
	msgBox.setDetailedText(result);
	msgBox.setStandardButtons(QMessageBox::Ok);
	int ret = msgBox.exec();


	//auto errorWidget = new QErrorMessage(this);
	//errorWidget->showMessage(result);
}