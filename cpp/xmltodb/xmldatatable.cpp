#include "xmldatatable.h"
#include "xmldataviewerexception.h"

XmlDataTable::XmlDataTable()
{
	setHeaderData(0, Qt::Horizontal, QVariant("id"));
#if 0
	//header.append(QVariant("h 1"));
	//header.append(QVariant("h 2"));
	//header.append(QVariant("h 3"));
	//header.append(QVariant("h 4"));
	setHeaderData(0, Qt::Horizontal, QVariant("h 0"), 0);
	setHeaderData(0, Qt::Horizontal, QVariant("h 1"), 0);
	setHeaderData(0, Qt::Horizontal, QVariant("h 2"), 0);
	setHeaderData(0, Qt::Horizontal, QVariant("h 3"), 0);
	setHeaderData(0, Qt::Horizontal, QVariant("h 4"), 0);
	setHeaderData(0, Qt::Horizontal, QVariant("h 5"), 0);

	//rawData.append(QMap<QVariant, QVariant>());
	//rawData.append(QMap<QVariant, QVariant>());
	insertRow(0, QModelIndex());
	insertRows(0, 6, QModelIndex());

	//for (size_t h = 0; h < header.size(); h++)
	for (auto head:header)
	{
		for (size_t i = 0; i < rowData.size(); i++)
		{
			//rowData[i].insert(head, QVariant(QString("head(%1) :: raw(%2)").arg(head.toString()).arg(i)));
			QVariant value(QString("head(%1) :: raw(%2)").arg(head.toString()).arg(i));
			//QModelIndex index;
			//index.
			setData(index(i, header.indexOf(head), QModelIndex()), value, Qt::EditRole);
		}
	}

#endif


}


XmlDataTable::~XmlDataTable()
{
}

Qt::ItemFlags XmlDataTable::flags(const QModelIndex &index) const 
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (index.isValid()) flags |= Qt::ItemIsEditable;
	return flags;
}

int XmlDataTable::rowCount(const QModelIndex &parent) const
{
	return rowData.size();
}
int XmlDataTable::columnCount(const QModelIndex &parent) const
{
	return header.size(); 
}

QVariant XmlDataTable::data(const QModelIndex &index, int role) const
{
	//Q_UNUSED(role);
	if (role != Qt::DisplayRole && role != Qt::EditRole)
		return QVariant();

	auto result = rowData.at(index.row()).value(header.at(index.column()));
	return QVariant(result);
	
	
}

bool XmlDataTable::setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role)
{
	Q_UNUSED(section);
	Q_UNUSED(role);

	if (orientation == Qt::Horizontal)
	{
		if (!header.contains(value))
		{
			header.append(value);
			beginInsertColumns(QModelIndex(), 0, 0); //notify that  rows will be appended	
			endInsertColumns();
		}
	}
	return true;
}


QVariant XmlDataTable::headerData(int section, Qt::Orientation orientation, int role) const
{
	
	if (role != Qt::DisplayRole)
		return QVariant();
	QVariant result;
	switch (orientation)
	{
	case Qt::Horizontal:
		result = header.at(section);
		break;
	case Qt::Vertical:
		result = QVariant(tr("%1").arg(section));
		break;
	default:
		break;
	}
	return result;
}


bool XmlDataTable::insertRows(int position, int rows, const QModelIndex &index_)
{
	Q_UNUSED(index_);
	Q_UNUSED(position);
	
	for (int row = 0; row < rows; row++) 
	{
		rowData.append(QMap<QVariant, QVariant>());

		beginInsertRows(QModelIndex(), 0, 0); //notify that  rows will be appended	
		endInsertRows();		
		
	}

	return true;
}


bool XmlDataTable::setData(const QModelIndex &index, const QVariant &value, int role)
{
	//Q_UNUSED(role);
	if (index.isValid() )//&& role == Qt::EditRole)	
	{
		int row = index.row();
		int column = index.column();		
		
		rowData[row][header.at(column)] = value;

		emit dataChanged(index, index);
		
		return true;
	}

	return false;
}

bool XmlDataTable::setData(int row, const QVariant& head, const QVariant &value, int role)
{	
	rowData[row][head] = value;
	QModelIndex dataIndex(index(row, header.indexOf(head)));
	emit dataChanged(dataIndex, dataIndex);
	
	return true;
}