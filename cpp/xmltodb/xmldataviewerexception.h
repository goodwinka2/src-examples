#pragma once
#include <QString>

class XmlDataViewerException
{
public:
	enum Exception
	{
		DB_FILE_OPEN_FAIL,
		GUI_FAIL,
		CALC_IN_GUI_THREAD
	};

	XmlDataViewerException(XmlDataViewerException::Exception e, QString text = QString(""))
	{
		id = e;
		description = text;
	}

	QString what(){ return description; }
	Exception exeption(){ return id; }
private:
	Exception id;
	QString description;
};