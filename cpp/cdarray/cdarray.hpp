#pragma once
#include <exception>
#include <iterator>

template <typename T>
class _iterator;

template <class T>
class CDArray
{
public:
    CDArray();
    CDArray(unsigned size);
    virtual ~CDArray();
    unsigned add(const T &t);
    bool remove(unsigned number);
    bool modify(unsigned n, const T &t);
    unsigned resize(unsigned size);
    void clear();
    T &operator[](unsigned i) const;
    unsigned getNumber();
    unsigned getSize();
    T *getData();

    _iterator<T> begin();
    _iterator<T> end();

private:
    T *data;
    int number;
    int size;
};

template <typename T>
class _iterator
{
    T *ptr;

public:
    using iterator_category = std::random_access_iterator_tag;
    using value_type = T;
    using reference = T &;
    using pointer = T *;
    using difference_type = unsigned long long;

    _iterator(T *ptr) : ptr(ptr) {}

    reference operator*() const { return *ptr; }
    pointer operator->() const { return ptr; }

    _iterator &operator++()
    {
        ptr++;
        return *this;
    }
    _iterator &operator--()
    {
        ptr--;
        return *this;
    }

    difference_type operator-(const _iterator &it) const { return this->ptr - it.ptr; }

    _iterator operator+(const difference_type &diff) const { return _iterator(ptr + diff); }
    _iterator operator-(const difference_type &diff) const { return _iterator(ptr - diff); }

    reference operator[](const difference_type &offset) const { return *(*this + offset); }

    bool operator==(const _iterator &it) const { return this->ptr == it.ptr; }
    bool operator!=(const _iterator &it) const { return this->ptr != it.ptr; }
    bool operator<(const _iterator &it) const { return this->ptr < it.ptr; }
    bool operator>(const _iterator &it) const { return this->ptr > it.ptr; }
    bool operator>=(const _iterator &it) const { return !(this->ptr < it.ptr); }
    bool operator<=(const _iterator &it) const { return !(this->ptr > it.ptr); }

    operator _iterator<const T>() const { return _iterator<const T>(ptr); }
};

template <class T>
CDArray<T>::CDArray() : number(0), size(1)
{
    data = 0;
    resize(size);
}
template <class T>
CDArray<T>::CDArray(unsigned size) : number(0), size(size)
{
    data = 0;
    resize(size);
}

template <class T>
CDArray<T>::~CDArray()
{
    free(data);
}

template <class T>
unsigned CDArray<T>::add(const T &t)
{
    *(data + number) = t;
    number++;
    if (number == size)
    {
        size *= 2;
        data = (T *)realloc(data, sizeof(T) * size);
    }
    return number;
}

template <class T>
bool CDArray<T>::remove(unsigned number)
{
    if (this->number < number || number == 0)
        return false;
    this->number -= number;
    return true;
}

template <class T>
bool CDArray<T>::modify(unsigned n, const T &t)
{
    if (n >= size)
        return false;
    *(data + n) = t;
    return true;
}

template <class T>
unsigned CDArray<T>::resize(unsigned size)
{
    if (size == 0)
        throw std::runtime_error("bad size");

    this->size = size;
    data = (T *)realloc(data, sizeof(T) * size);
    if (number >= size)
        number = size - 1;
    return this->size;
}

template <class T>
void CDArray<T>::clear()
{
    number = 0;
    resize(1);
}

template <class T>
T &CDArray<T>::operator[](unsigned i) const
{
    if (i >= number)
        throw std::runtime_error("bad index");

    return *(data + i);
};

template <class T>
unsigned CDArray<T>::getNumber() { return number; };

template <class T>
unsigned CDArray<T>::getSize() { return size; };

template <class T>
T *CDArray<T>::getData() { return data; };

template <class T>
_iterator<T> CDArray<T>::begin()
{
    return _iterator<T>(data);
}

template <class T>
_iterator<T> CDArray<T>::end()
{
    return _iterator<T>(data + number);
}
