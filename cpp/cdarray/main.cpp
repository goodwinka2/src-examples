#include <iostream>
#include "cdarray.hpp"

#include <algorithm>

const bool useVisualisation = false;
const unsigned count = 700;

int main() {   
    
    float max = 0,min = 0;
    std::cout << "Enter min value" << std::endl;
    std::cin >> min;
    std::cout << "Enter max value" << std::endl;
    std::cin >> max;
    float k = (max-min)/RAND_MAX;
    
    CDArray<float> arr(count);

    if(useVisualisation)
        std::cout << "Array:" << std::endl;
    for(unsigned i = 0;i<count;i++)
    {
        arr.add(min +  std::rand()*k );
        if(useVisualisation)        
        std::cout << arr[i] << std::endl;
    }

    std::sort(arr.begin(), arr.end(),[](auto a, auto b){return a < b;});
    

    if(useVisualisation)
    {
        std::cout << "Sorted Array:" << std::endl;
        for(unsigned i = 0;i<count;i++)
        {
            std::cout << arr[i] << std::endl;
        }
    }


    auto mean = 0*arr[0];
    for(auto& var: arr)
    {
        mean+=var;
    }
    mean/=arr.getNumber();
    auto dispersion = 0*arr[0];
    for(auto& var: arr)
    {
        dispersion+=pow(var-mean,2);
    }
    dispersion/=arr.getNumber();
    dispersion= sqrt(dispersion);

    std::cout << "mean\t\t" << mean<< std::endl;
    std::cout << "dispersion\t" << dispersion<< std::endl;
}