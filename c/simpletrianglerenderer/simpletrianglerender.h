#ifndef SIMPLETRIANGLERENDER_H
#define SIMPLETRIANGLERENDER_H


typedef  unsigned int TriangleRendererColorType;
//typedef TriangleRendererColorType unsigned char;

typedef struct 
{
	int width;
	int height;
	int widthDec1;
	int heightDec1;
	TriangleRendererColorType *image;
}TriangleRenderer;

//struct TriangleRendererPoint2d
//{
//	int x;
//	int y;
//	TriangleRendererColorType color;
//};

#ifdef __cplusplus
extern "C" {
#endif

void triangleRenderer_init(TriangleRenderer *renderer, void *image, int width, int height);

void triangleRenderer_clear(TriangleRenderer *renderer);

void triangleRenderer_drawTriangle(TriangleRenderer *renderer,int v0_x, int v0_y, TriangleRendererColorType v0_c,
	int v1_x, int v1_y, TriangleRendererColorType v1_c, 
	int v2_x, int v2_y, TriangleRendererColorType v2_c);



#ifdef __cplusplus
}
#endif



#endif