#include "simpletrianglerender.h"

//#include <math.h>

void triangleRenderer_init(TriangleRenderer *renderer, void *image, int width, int height)
{
	renderer->image = image;
	renderer->width = width;
	renderer->height = height;
	renderer->widthDec1 = width-1;
	renderer->heightDec1 = height-1;

	triangleRenderer_clear(renderer);
}

void triangleRenderer_clear(TriangleRenderer *renderer)
{
	//renderer->image;
	memset(renderer->image, 0, renderer->width*renderer->height*sizeof(TriangleRendererColorType));

}

#define bias(a_x, a_y,b_x, b_y) ( (b_x >= a_x || a_y >= b_y) ? (0):(-1) )

#define orient2d(a_x, a_y, b_x, b_y, c_x, c_y)	( -( ((b_x - a_x)*(c_y - a_y) - (b_y - a_y)*(c_x - a_x)) + bias(a_x, a_y,b_x, b_y)) )

#define min3(a,b,c)	((a<b) ? (((a<c) ? (a) : (c))) : (((b<c) ? (b) : (c))))

#define max3(a,b,c)	((a>b) ? (((a>c) ? (a) : (c))) : (((b>c) ? (b) : (c))))

#define min2(a,b)	((a<b) ? (a) : (b))

#define max2(a,b)	((a>b) ? (a) : (b))



void triangleRenderer_drawTriangle(TriangleRenderer *renderer,int v0_x, int v0_y, TriangleRendererColorType v0_c,
	int v1_x, int v1_y, TriangleRendererColorType v1_c,
	int v2_x, int v2_y, TriangleRendererColorType v2_c)
{


	int minX = min3(v0_x, v1_x, v2_x);
	int minY = min3(v0_y, v1_y, v2_y);

	int maxX = max3(v0_x, v1_x, v2_x);
	int maxY = max3(v0_y, v1_y, v2_y);

	//minX = max2(minX, 0);
	//minY = max2(minY, 0);

	if (maxX >= renderer->width || maxY >= renderer->height || minX < 0 || minY < 0)return;

	int p_x;
	int p_y;
	TriangleRendererColorType p_c;

	register int w0, w1, w2;
	//register int sign;
	//sign = -1;
	//sign = (v0_x <= v1_x && v0_x <= v2_x && v0_y <= v1_y && v0_y <= v2_y 
	//	&& v1_x >= v2_x && v1_y <= v2_y) ? (1) : (-1);
	//sign = (v0_x <= v1_x && v1_x <= v2_x || v0_y <= v1_y && v1_y <= v2_y) ? (1) : (-1);

	p_c = max3(v0_c, v1_c, v2_c);

	for (p_y = minY; p_y <= maxY; p_y++)
	{
		for (p_x = minX; p_x <= maxX; p_x++)
		{
			w0 = orient2d(v1_x, v1_y, v2_x, v2_y, p_x, p_y);
			w1 = orient2d(v2_x, v2_y, v0_x, v0_y, p_x, p_y);
			w2 = orient2d(v0_x, v0_y, v1_x, v1_y, p_x, p_y);

			if (w0 >= 0 && w1 >= 0 && w2 >= 0)
			{
				//if (p_x + p_y * renderer->width < renderer->width*renderer->height)

				//renderer->image[p_x + p_y * renderer->width] = p_c;
				renderer->image[p_x + p_y * renderer->width] = (w0 > w1) ? ((w0 > w2) ? (v0_c) : (v2_c)) : ((w1 > w2) ? (v1_c) : (v2_c));
			}

		}
	}

}